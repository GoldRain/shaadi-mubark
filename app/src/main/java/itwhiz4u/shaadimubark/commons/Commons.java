package itwhiz4u.shaadimubark.commons;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import itwhiz4u.shaadimubark.activity.ChatActivity;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.Message;
import itwhiz4u.shaadimubark.model.NotificationModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.DateUtils;
import itwhiz4u.shaadimubark.utils.Utils;

public class Commons {

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;
    public static int notiCount = 0;

    public static ChatActivity g_chatActivity = null;

    public static int SCREEN_WIDTH = 0;

    public static int SCREEN_HEIGHT = 0;

    public static int STATUSBAR_HEIGHT = 0;

    public static UserModel g_user ;

    public static ArrayList<UserModel> all_users = new ArrayList<>();
    public static ArrayList<UserModel> match_users = new ArrayList<>();
    public static ArrayList<UserModel> favorite_users = new ArrayList<>();
    public static UserModel filter_user = new UserModel();
    public static CountryModel filter_country = new CountryModel();
    public static HashMap<Integer, ChatModel> chatRooms = new HashMap<>();
    public static HashMap<Integer, ArrayList<Message>> messages = new HashMap<>();
    public static ArrayList<NotificationModel> myNotifications = new ArrayList<>();
    public static int price = 0;


    public static long loginTime;

    public static String getCurrentLanCode(){
        return Locale.getDefault().getLanguage();//.getDisplayLanguage();
    }


    public static int GetPixelValueFromDp(Context context, float dp_value) {

        int pxValue = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp_value, context.getResources()
                        .getDisplayMetrics());

        return pxValue;
    }


    public static int addrToIdx(String addr) {
        int pos = addr.indexOf("@");
        return Integer.valueOf(addr.substring(0, pos)).intValue();
    }

    public static String fileExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf(".") == -1) {
            return url;
        } else {
            String ext = url.substring(url.lastIndexOf(".") );
            if (ext.indexOf("%") > -1) {
                ext = ext.substring(0, ext.indexOf("%"));
            }
            if (ext.indexOf("/") > -1) {
                ext = ext.substring(0, ext.indexOf("/"));
            }
            return ext.toLowerCase();
        }
    }

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {
            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String  mCurrentActivityName = "";

}
