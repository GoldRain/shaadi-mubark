package itwhiz4u.shaadimubark.commons;

public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_SESSION = "session";
    public static final String PREFKEY_MAPID = "mapID";
    public static final String PREFKEY_USER_MODEL = "userModel";
    public static final String PREFKEY_IS_ADMIN = "is_admin";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_USERNAME = "username";
    public static final String PREFKEY_USER_GENDER = "user_gender";

}

