package itwhiz4u.shaadimubark.commons;

import java.util.logging.Handler;

public class ReqConst {

    public static final String SERVER_ADDR = "http://sm.squareone.host";

    public static final String PRIVACY_URL = "http://sm.squareone.host/privacy.html";
    public static final String TERMS_URL = "http://sm.squareone.host/terms.html";

    //public static final String SERVER_ADDR = "http://192.168.1.81/Mubarak";
    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static Handler g_handler = null;
    public static String g_appVersion = "1.0";
    public static int g_badgCount = 0;

    public static final String SERVER_URL = SERVER_ADDR + "/index.php/Api/callBackend";

    //session
    public static final String SESSION = "session";
    //Request value
    public static final String MODULE = "module";
    public static final String FUNCTION = "function";

    //functions.
    public static final String FUNC_SIGNUP = "signUp";
    public static final String FUNC_SIGNIN = "signIn";
    public static final String FUNC_VERIFY = "verifyUser";
    public static final String FUNC_RESEND_CODE = "resendVerifyCode";
    public static final String FUNC_CHECK_USERNAME = "checkUserNameValid";
    public static final String FUNC_UPDATE_PROFILE = "updateProfile";
    public static final String FUNC_UPDATE_PROFILE_IMAGE = "updateProfileImage";
    public static final String FUNC_SET_FAVORITE = "setUserFavorite";
    public static final String FUNC_SET_USER_SEEN = "setUserAsSeen";
    public static final String FUNC_SEARCH_USER = "searchUsers";
    public static final String FUNC_DELETE_USER = "deleteProfile";
    public static final String FUNC_ADD_IMAGES = "addImages";
    public static final String FUNC_SEND_CHAT_REQUEST = "sendChatRequest";
    public static final String FUNC_REPORT_USER = "reportUser";
    public static final String FUNC_UPLOAD_IMAGE = "uploadImage";
    public static final String FUNC_GET_MY_NOTIFICATIONS = "getMyNotifications";
    public static final String FUNC_DELET_NOTIFICATION = "deleteNotification";
    public static final String FUNC_ACCEPT_CHART_REQUEST = "acceptChatRequest";
    public static final String FUNC_BLOCK_CHART_REQUEST = "blockChatRequest";
    public static final String FUNC_SEND_CHAT_NOTI = "sendChatNotification";
    public static final String FUNC_SET_NOTIFICATION_READ = "setNotificationRead";
    public static final String FUNC_CLEAR_ALL = "clearAll";
    public static final String FUNC_LOGOUT = "logout";
    public static final String FUNC_PAYMENT = "payfor6months";

    //modules
    public static final String MODULE_USER = "User";
    public static final String MODULE_CHATS = "Chats";
    public static final String MODULE_REPORT = "Reports";
    public static final String MODULE_IMAGE_UPLOADER = "ImageUploader";
    public static final String MODULE_NOTIFICATIONS = "Notifications";
    public static final String MODULE_PAYMENT = "Payments";

    //params
    /*signin*/
    public static final String PARAM_LANGUAGE = "language";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_USER_EMAIL = "user_email";
    public static final String PARAM_USER_PASSWORD = "user_password";
    public static final String PARAM_FCMTOKEN = "fcmtoken";

    //sign up
    public static final String PARAM_FIRST_NAME = "user_firstname";
    public static final String PARAM_LAST_NAME = "user_lastname";
    public static final String PARAM_USER_NAME = "user_username";
    public static final String PARAM_USER_BIRTHDAY = "user_birthday";
    public static final String PARAM_USER_PHONE_NUMBER = "user_phonenumber";
    public static final String PARAM_FILE = "file[0]";
    public static final String PARAM_FILE_KEY = "file";
    public static final String PARAM_USER_IMAGES = "user_images";

    //verify
    public static final String PARAM_PIN = "pin";

    //personal
    public static final String PARAM_USER_GENDER = "user_gender";
    public static final String PARAM_USER_HEIGHT = "user_height";
    public static final String PARAM_USER_TOTALSIBLING = "user_totalsibling";
    public static final String PARAM_USER_COUNTRY = "user_country";
    public static final String PARAM_USER_CITY = "user_city";
    public static final String PARAM_USER_MARITAL_STATUS = "user_maritalstatus";
    public static final String PARAM_USER_CHILDREN = "user_children";
    public static final String PARAM_USER_COMPLEXION = "user_complexion";
    public static final String PARAM_USER_BODYTYPE = "user_bodytype";
    public static final String PARAM_USER_APPEARANCE = "user_appearance";
    public static final String PARAM_USER_HAIR = "user_hair";
    public static final String PARAM_USER_FACIAL = "user_facialhair";

    //religion
    public static final String PARAM_USER_RELIGION = "user_religion";
    public static final String PARAM_USER_CASTE = "user_caste";
    public static final String PARAM_USER_CASTESTRING = "user_castestring";
    public static final String PARAM_USER_SUBCASTE = "user_subcaste";

    //professional
    public static final String PARAM_USER_PROFESSIONAL = "user_profession";
    public static final String PARAM_USER_JOBSTATUS = "user_jobstatus";
    public static final String PARAM_USER_SALARY = "user_salary";
    public static final String PARAM_USER_RELOCATABLE = "user_relocatable";
    public static final String PARAM_USER_OWNAPARTMENT = "user_ownapartment";
    public static final String PARAM_USER_EDUCATIONLEVEL = "user_educationlevel";

    //match req
    public static final String PARAM_FILTER_AGEFROM = "filter_agefrom";
    public static final String PARAM_FILTER_AGETO = "filter_ageto";
    public static final String PARAM_FILTER_HEIGHTFROM = "filter_heightfrom";
    public static final String PARAM_USER_FILTER_HEIGHTTO = "filter_heightto";
    public static final String PARAM_FILTER_COUNTRY = "filter_country";
    public static final String PARAM_FILTER_CITY = "filter_city";
    public static final String PARAM_USER_FILTER_MARITAL = "filter_maritalstatus";
    public static final String PARAM_FILTER_RELIGION = "filter_religion";
    public static final String PARAM_FILTER_MINEDUCATION = "filter_minimumedu";
    public static final String PARAM_USER_FILTER_PROFESSIONS = "filter_professions";
    public static final String PARAM_FILTER_CASTE = "filter_caste";
    public static final String PARAM_FILTER_HAVECHILDREN = "filter_havechildren";
    public static final String PARAM_FILTER_OWNAPARTMENT = "filter_ownapartment";
    public static final String PARAM_FILTER_RELOCATABLE = "filter_relocatable";
    public static final String PARAM_USER_PAID = "user_paid";

    //match page
    public static final String PARAM_FAVORITE_USER = "favorite_fav_user";
    public static final String PARAM_FAVORITE_STATUS = "favorite_status";
    public static final String PARAM_USER_SEEN = "user_seen";

    //search user page
    public static final String PARAM_INDEX = "index";
    public static final String PARAM_USER_FAVORITE = "favorite_fav_user";
    public static final String PARAM_USER_CREATEDAT = "user_createdat";
    public static final String PARAM_SEARCH_STRING = "search_string";

    //notification
    public static final String PARAM_NOTICATION_TIMESTAMP = "notification_timestamp";
    public static final String PARAM_NOTIFICATION_ID = "notification_id";
    public static final String PARAM_TOKEN = "token";
    public static final String PARAM_REQUEST_ID = "request_id";
    public static final String RES_NOTIFICATIONS = "notifications";
    public static final String PARAM_NOTIFICATION_TYPE = "notification_type";
    public static final String PARAM_NOTIFICATION_SENDER = "notification_sender";
    public static final String PARAM_NOTIFICATION_RECEIVER = "notification_receiver";
    public static final String PARAM_NOTIFICATION_READ = "notification_read";
    public static final String PARAM_NOTIFICATION_ITEM = "notification_item";
    public static final String PARAM_NOTIFICATION_MESSAGE = "notification_message";
    public static final String PARAM_NOTIFICATION_TITLE = "notification_title";

    //chat
    public static final String RES_CHATS = "chats";

    //payment
    public static final String PARAM_AMOUNT = "amount";
    public static final String PARAM_STRIPE_TOKEN = "stripeToken";
    public static final String PARAM_CURRENCY = "currency";

    //report user
    public static final String PARAM_REPORTED_USER = "reported_user";
    public static final String PARAM_REPORT_CONTENT = "report_content";


    public static final String REQ_FACEBOOK = "loginwithfacebook";
    public static final String REQ_UPLOAD_THUMB_NAIL = "uploadThumbnail";
    public static final String REQ_UPLOAD_VIDEO = "uploadVideo";
    public static final String REQ_SAVE_MULTI_FILES = "saveMulitiFiles";
    public static final String REQ_ALL_REQUEST = "getAllRequests";
    public static final String REQ_UPLOAD_AVATAR = "uploadAvatar";
    public static final String REQ_UPDATE_PROFILE = "updateProfile";
    public static final String RES_ALLNOTIFICATION = "getAllNotifications";
    public static final String REQ_ACCEPT_REQUEST = "acceptRequest";
    public static final String REQ_DELETE_REQUEST = "deleteRequest";
    public static final String REQ_REJECT_REQUEST = "rejectRequest";
    public static final String REQ_REMOVE_NOTIFICATION = "removeNotification";
    public static final String REQ_RESUBMIT_REQUEST = "reSubmitRequest";
    public static final String REQ_GETS_SUBSCRIBERS = "getSubscribers";
    public static final String REQ_UPDATE_TOKEN = "updateToken";

    public static final String REQ_SUBMIT_REQUEST = "submitRequest";


    public static final String PARAM_ID = "id";
    public static final String RES_NOTI_ID = "noti_id";


    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_TITLE ="title";



    public static final String PARAM_SUBSCRIBER = "subscriber";
    public static final String PARAM_SUBJECTS = "subject";

    //response value

    //sign in

    public static final String RES_CASTE = "caste";
    public static final String RES_PRICE = "price";
    public static final String RES_COUNTRIES = "countries";
    public static final String RES_USERS = "users";
    public static final String RES_MATCH_USERS = "match_users";
    public static final String RES_FAVORITE_USERS = "favorite_users";
    /*sign up*/
    public static final String RES_USER = "user";
    public static final String RES_USER_PHOTO = "user_photo";

    //profile
    public static final String RES_USER_IMAGES = "user_images";
    public static final String PARAM_REQUEST_USER = "request_request_user";
    public static final String PARAM_RECEIVE_USER = "request_receive_user";
    //chat
    public static final String RES_IMAGE_URL = "image_url";

    public static final String RESULT = "result";
    public static final String MESSAGE = "message";
    public static final String RES_SUCCESS = "success";
    public static final String USER_STATUS = "user_status";
    public static final String RES_FAIL = "fail";


}
