package itwhiz4u.shaadimubark.commons;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class Constants {

    public static final String APP_NAME = "ShaadiMubark";
    public static final int VOLLEY_TIME_OUT = 60000;

    public static String TOKEN = "token";

    public static final int LIMIT_FILE = 25 * 1024 * 1024;

    public static final int PROFILE_IMAGE_SIZE = 512;

    public static final int RECENT_MESSAGE_COUNT = 20;
    public static final int MAX_IMAGE_COUNT = 6;
    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";

    public static final String KEY_COUNT = "count";
    public static final int SPLASH_TIME = 1000;
    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int PICK_FROM_IMAGES = 116;

    public static final int FROM_SIGNUP = 500;
    public static final int FROM_FORGOT = 1000;
    public static final String FROM_PAGE = "from_page";

    public static final int PICK_FROM_VIDEO_GALLERY = 106;
    public static final String KEY_IMAGES = "images";

    public static final String KEY_ROOM = "room";
    public static final String KEY_LOGOUT = "logout";
    public static final String KEY_USER = "user";
    public static final String REPORTED_USER_ID = "reportedUserId";
    public static final String KEY_FAVORITE_USERS = "favorite_users";
    public static final String KEY_MATCH_USERS = "match_users";
    public static final String KEY_FROM_FORGOT = "from_forgot";

    public static final String KEY_FROM_PROFILE = "from_profile";
    public static final String PROFILE = "Profile";
    public static final String SIGN_UP = "SignUp";
    public static final String PERSONAL = "Personal";
    public static final String RELIGION = "Religion";
    public static final String PROFESSION = "Profession";
    public static final String MATCHREQ = "MatchReq";
    public static final String USER_ARRAY = "UserArray";
    public static String PAGE_TO = "";
    public static final String SETTING_PAGE = "SettingPage";
    public static final String CHATTING_PAGE = "ChattingPage";
    public static final String PROFILE_PAGE = "ProfilePage";
    public static final String CHAT_ROOM_DELETED = "***delete***chat***";

    public static String EMAIL = "";
    public static int WIDTH = 0;
    public static final int ANDROID = 0;

    public static String qrstring = " ";

    public static int FILTER_STATUS = 0; //0: all: 1: request: 2:accepted:3:rejected

    //public static boolean SUBSCRIBER = false;/* false:student, true:subscriber */

    public static int NOTI_COUNTER = 0;

    public static int SIGNUP_STATUS = 0; //0: sign up user , 5 login user, 10: update user
    public static int PROFILE_STATUS = 0; // 0: pending, 1: verified, 2: completed personal, 3: completed religion, 4: completed prof, 10: done

    public static int USER_STATUS_PENDING = 0;
    public static int USER_STATUS_VERIFIED = 1;
    public static int USER_STATUS_COMP_PERSON = 2;
    public static int USER_STATUS_COMP_RELIGION = 3;
    public static int USER_STATUS_COMP_PROFESSION = 4;
    public static int USER_STATUS_PROFILE_COMPLETED = 10;
    public static int USER_STATUS_SUSPENDED = 100;
    public static int USER_STATUS_DELETED = 101;

    public static int pageIDX = 1;

    public static final String KEY_BROADCAST = "com.itwhiz4u.shaadimubark";

    public static final String CHAT_FRAGMENT = "chatFragment";
    public static final String FAVORITE_FRAGMENT = "favoriteFragment";
    public static final String NOTIFICATION_FRAGMENT = "notificationFragment";
    public static final String SEARCH_FRAGMENT = "searchFragment";
    public static final String SETTING_FRAGMENT = "settingFragment";

    public static final String MAIN_ACTIVITY = "mainActivity";
}
