package itwhiz4u.shaadimubark.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

import itwhiz4u.shaadimubark.activity.ChatActivity;
import itwhiz4u.shaadimubark.base.BaseActivity;

public class StatePendingActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_state_pending);

        //  if(AppConstants.aroundActivity==null){
        Intent i = new Intent(this, ChatActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        //  }
        finish();
    }

}
