package itwhiz4u.shaadimubark.utils.Callback;

import org.json.JSONObject;

import java.util.ArrayList;

public interface Callback {
    public void callback(Object object);
    public void callback(String result, String message, JSONObject response);
}
