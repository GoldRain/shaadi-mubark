package itwhiz4u.shaadimubark.utils;

import android.annotation.SuppressLint;

import org.json.JSONException;
import org.json.JSONObject;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

/**
 * Created by ITWhiz4U on 3/5/2018.
 */

public class MyJSONObject extends JSONObject {

    public String getStringValue(String name) {
        try {
            if (this.has(name)) {
                if( isNull(this.getString(name))) {
                    return "";
                }
                else {
                    return this.getString(name);
                }
            }
            else {
                return "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public int getIntValue(String name) {
        try {
            if (this.has(name)) {
                if( isNull(this.getString(name))) {
                    return 0;
                }
                else {
                    return this.getInt(name);
                }
            }
            else {
                return 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public float getFloatValue(String name) {
        try {
            if (this.has(name)) {
                if( isNull(this.getString(name))) {
                    return 0.f;
                }
                else {
                    return this.getInt(name);
                }
            }
            else {
                return 0.f;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return 0.f;
        }
    }

    public long getLongValue(String name) {
        try {
            if (this.has(name)) {
                if( isNull(this.getString(name))) {
                    return 0L;
                }
                else {
                    return this.getInt(name);
                }
            }
            else {
                return 0L;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return 0L;
        }
    }

}
