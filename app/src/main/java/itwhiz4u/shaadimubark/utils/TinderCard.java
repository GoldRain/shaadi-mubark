package itwhiz4u.shaadimubark.utils;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.NonReusable;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeHead;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.mindorks.placeholderview.annotations.swipe.SwipeView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MatchActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.User;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;
import itwhiz4u.shaadimubark.utils.Callback.Callback;

@NonReusable
@Layout(R.layout.tinder_card_view)
public class TinderCard {

    Context context;
    private static int count;

    @View(R.id.card_profile)
    CardView card_profile;
    @View(R.id.profileImageView)
    RoundedImageView profileImageView;

    @View(R.id.txv_name)
    TextView txv_name;

    @View(R.id.txv_year)
    TextView txv_year;

    @View(R.id.txv_height)
    TextView txv_height;
    @SwipeView
    android.view.View view;

    public UserModel matchUser;
    MatchActivity activity;

    public TinderCard(MatchActivity matchActivity, UserModel userModel) {

        matchUser = userModel;
        this.activity = matchActivity;
    }

    @Click(R.id.profileImageView)
    public void onClick() {
        Log.d("DEBUG", "profileImageView");
    }

    @Resolve
    public void onResolve() {


        card_profile.setPreventCornerOverlap(false);

        txv_name.setText(matchUser.getName());
        txv_year.setText(String.valueOf(matchUser.getAge()));
        txv_height.setText(String.valueOf(matchUser.user_height));


        if (matchUser.user_photo.length() > 0)
            Picasso.with(activity).load(matchUser.user_photo).placeholder(R.drawable.img_user).into(profileImageView);

    }

    @SwipeOut
    public void onSwipedOut() {
        setUserasSeen();
    }

    @SwipeCancelState
    public void onSwipeCancelState() {
    }

    @SwipeIn
    public void onSwipeIn() {
        setFavoriteUser();
    }

    @SwipeInState
    public void onSwipeInState() {
    }

    @SwipeOutState
    public void onSwipeOutState() {
    }

    @SwipeHead
    public void onSwipeHead() {
    }

    private void setFavoriteUser(){
        int userId = matchUser.user_id;
        final Callback callback = new Callback() {
            @Override
            public void callback(Object object) {

            }

            @Override
            public void callback(String result, String message, JSONObject response) {
            }
        };

        ApiHelper helper = new ApiHelper();
        helper.setUserFavorite(matchUser, 1, callback);
    }

    private void parseFavorite(String json){

        Log.d("Match", json);


    }

    private void setUserasSeen(){

        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.showAlertDialog(activity.getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_SET_USER_SEEN);
                    params.put(ReqConst.SESSION, EasyPreference.with(activity).getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_USER_SEEN, String.valueOf(matchUser.user_id));
                    params.put(ReqConst.SESSION, EasyPreference.with(activity).getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

}
