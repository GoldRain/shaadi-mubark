package itwhiz4u.shaadimubark.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.telecom.Call;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.Message;
import itwhiz4u.shaadimubark.model.MessagesenderModel;
import itwhiz4u.shaadimubark.model.NotificationModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Callback.Callback;

public class ChatManager {

    public static DatabaseReference firebaseReferencechat = FirebaseDatabase.getInstance().getReference("Chats");
    public static LocalBroadcastManager lbm;

    public ChatManager() { }

    public ChildEventListener setChatListener(final ChatModel chatRoom, final Callback callback) {
        return firebaseReferencechat.child(chatRoom.chat_room).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                final Message chatEntity = new Message(
                        dataSnapshot.getKey(),
                        dataSnapshot.child("text").getValue().toString(),
                        dataSnapshot.child("date").getValue().toString(),
                        dataSnapshot.child("senderId").getValue().toString(),
                        dataSnapshot.child("type").getValue().toString(),
                        Integer.parseInt(dataSnapshot.child("read").getValue().toString()),
                        chatRoom.chat_user
                );
                callback.callback(chatEntity);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void sendMessage(MessagesenderModel message, String chatRoom) {
        firebaseReferencechat.child(chatRoom).push().setValue(
                message);
    }

    public static void sendDeleteMessage() {

        MessagesenderModel message = new MessagesenderModel(
                DateUtils.getcurrentgmttime(),
                0,
                String.valueOf(Commons.g_user.user_id),
                "***delete***chat***",
                "text" );

        for (int i = 0; i < Commons.chatRooms.size(); i++){
            sendMessage(message,Commons.chatRooms.get(i).chat_room);
        }

    }

    public static void setConnectedRef(){

        // since I can connect from multiple devices, we store each connection instance separately
        // any time that connectionsRef's value is null (i.e. has no children) I am offline
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myConnectionsRef = database.getReference("User/" + String.valueOf(Commons.g_user.user_id));

        // stores the timestamp of my last disconnect (the last time I was seen online)

        final DatabaseReference connectedRef = database.getReference(".info/connected");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    setUserOnline();
                    myConnectionsRef.onDisconnect().setValue(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled at .info/connected");
            }
        });
    }

    public static void setUserOnline() {
        if(Commons.g_user != null) {
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myConnectionsRef = database.getReference("User/" + String.valueOf(Commons.g_user.user_id));
            myConnectionsRef.setValue(true);
        }
    }

    public static void setUserOffline() {
        if(Commons.g_user != null) {
            final FirebaseDatabase database = FirebaseDatabase.getInstance();
            final DatabaseReference myConnectionsRef = database.getReference("User/" + String.valueOf(Commons.g_user.user_id));
            myConnectionsRef.setValue(false);
        }
    }

    public void setUserOnlineListener(int userId, final Callback callback){
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference userRef = database.getReference("User/" + String.valueOf(userId));
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {
                    Boolean status = (Boolean) dataSnapshot.getValue();
                    callback.callback(status);
                }
                else {
                    callback.callback(false);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.callback(false);
            }
        });

    }

    public static void removeUserOnlineListener(int userId){

        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myConnectionsRef = database.getReference("User/" + String.valueOf(userId));
        myConnectionsRef.removeValue();
    }

    public static void removeAllListener() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        for(ChatModel chat: Commons.chatRooms.values()) {

            if(chat.chatHandler != null) {
                database.getReference().child("Chats").child(chat.chat_room).removeEventListener(chat.chatHandler);
            }
        }
    }

    public static void setDeleteUserListener() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference deleteRef = database.getReference("Delete");

        deleteRef.child("user_id").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null) {

                    Log.d("getValue==>", String.valueOf(dataSnapshot.getValue()));
                    int userId = Integer.parseInt(dataSnapshot.getValue().toString());
                    Commons.chatRooms.remove(userId);
                    int index = 0;
                    for (UserModel user : Commons.all_users) {
                        if (user.user_id == userId) {
                            Commons.all_users.remove(index);
                            break;
                        }
                        index += 1;
                    }

                    index = 0;
                    for (UserModel user : Commons.match_users) {
                        if (user.user_id == userId) {
                            Commons.match_users.remove(index);
                            break;
                        }
                        index += 1;
                    }

                    index = 0;
                    for (UserModel user : Commons.favorite_users) {
                        if (user.user_id == userId) {
                            Commons.favorite_users.remove(index);
                            break;
                        }
                        index += 1;
                    }

                    if (Commons.mCurrentActivityName.equals(Constants.MAIN_ACTIVITY)){
                        lbm = LocalBroadcastManager.getInstance(ShaadiMubarkApplication.getContext());
                        lbm.sendBroadcast(new Intent(Constants.KEY_BROADCAST));
                    }
                    Log.d("aaaa===", "deleted");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void notifyDeleteProfile() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference deleteRef = database.getReference("Delete");
        deleteRef.child("user_id").setValue(Commons.g_user.user_id);
    }

}
