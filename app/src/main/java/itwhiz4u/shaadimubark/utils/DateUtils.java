package itwhiz4u.shaadimubark.utils;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

    public static long getTimeStamp(){
        Long tsLong = System.currentTimeMillis()/1000;
        return tsLong;
    }

    public static String getcurrentgmttime(){
        Long tsLong = System.currentTimeMillis();
        String ts = tsLong.toString();
        return ts;
    }

    public static String getDate(int time, String format) {

        SimpleDateFormat date_format = new SimpleDateFormat(format);
        date_format.setTimeZone(TimeZone.getDefault());
        long sTime = (long)time * 1000;
        return date_format.format(new Date(sTime));
    }

    public static String getTimeString(int time) {
        int date = (int)(new Date().getTime() / 1000);
        int timeStamp = date - time;
        if (timeStamp < 60){
            return "less than a minute";
        }
        else if(timeStamp < 3600)
        {
            int value = (int)(timeStamp/60);
            if(value > 1) {
                return String.valueOf(value) + " minutes ago";
            }
            else {
                return "a minute ago";
            }
        }
        else if(timeStamp < 86400)
        {
            int value = (int)(timeStamp/3600);

            if(value > 1) {
                return String.valueOf(value) + " hours ago";
            }
            else {
                return "an hour ago";
            }
        }
        else{
            SimpleDateFormat format = new SimpleDateFormat("EEE MMM d");
            format.setTimeZone(TimeZone.getDefault());
            long sTime = (long)time * 1000;
            return format.format(new Date(sTime));
        }
    }

}
