package itwhiz4u.shaadimubark.utils.Backend;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.telecom.Call;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.activity.SignInActivity;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.PriceModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Callback.Callback;
import itwhiz4u.shaadimubark.utils.ChatManager;

public class ApiHelper {

    String url = ReqConst.SERVER_URL;

    public void processPostRequest(String url, final HashMap<String, String> params, HashMap<String, String> headers, final Callback callback) {

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject json = new JSONObject(response);
                    String result = json.optString(ReqConst.RESULT);
                    String message = json.optString(ReqConst.MESSAGE);
                    if(message.equals("Session token has already expired") || message.equals("Session token has been tampered with")) {
                        //logout
                    }
                    CasteModel.allCastes = getCastes(json);
                    //PriceModel.allPrices = getPrices(json);
                    callback.callback(result, message, json);
                }
                catch (Exception e){
                    callback.callback("fail", e.getMessage(), new JSONObject());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.callback("fail", error.getMessage(), new JSONObject());
                //activity.showAlertDialog(activity.getString(R.string.network_error));
            }
        })
        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return params;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);

    }

    private ArrayList<CasteModel> getCastes(JSONObject json) {

        ArrayList<CasteModel> castes = new ArrayList<>();
        try {
            JSONArray objects = json.getJSONArray(ReqConst.RES_CASTE);
            for (int i = 0; i < objects.length(); i++) {
                castes.add(new CasteModel(objects.getJSONObject(i)));
            }
        }
        catch (Exception e) {

        }
        return castes;
    }

    /*private ArrayList<PriceModel> getPrices(JSONObject json) {

        ArrayList<PriceModel> prices = new ArrayList<>();
        try {
            JSONArray objects = json.getJSONArray(ReqConst.RES_PRICE);
            for (int i = 0; i < objects.length(); i++) {
                prices.add(new PriceModel(objects.getJSONObject(i)));
            }
        }
        catch (Exception e) {

        }
        return prices;
    }*/

    HashMap<String, String> setModule(HashMap<String, String> params, String module, String function) {

        HashMap<String, String> newParams = params;

        String session = Prefs.getString(PrefConst.PREFKEY_SESSION, "");

        if(session.length() > 0) {
            newParams.put(PrefConst.PREFKEY_SESSION, session);
        }

        if(Commons.g_user.user_id > 0){
            newParams.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
        }
        newParams.put(ReqConst.MODULE, module);
        newParams.put(ReqConst.FUNCTION, function);
        newParams.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
        return newParams;
    }

    public void setUserFavorite(final UserModel user, final int status, final Callback callback){

        HashMap<String, String> params = new HashMap<>();

        params.put(ReqConst.PARAM_FAVORITE_USER, String.valueOf(user.user_id));
        params.put(ReqConst.PARAM_FAVORITE_STATUS, String.valueOf(status));
        HashMap<String, String> reqParams = setModule(params, ReqConst.MODULE_USER, ReqConst.FUNC_SET_FAVORITE);
        final Callback callback1 = new Callback() {
            @Override
            public void callback(Object object) {

            }

            @Override
            public void callback(String result, String message, JSONObject response) {
                if(result.equals(ReqConst.RES_SUCCESS)){
                    Boolean exists = false;
                    user.isFavorite = status;
                    for(UserModel ruser: Commons.all_users){

                        if(user.user_id == ruser.user_id){
                            ruser.isFavorite = status;
                            if(status == 1) {
                                Commons.favorite_users.add(ruser);
                            }
                            exists = true;
                            break;
                        }
                    }

                    if(!exists){
                        Commons.favorite_users.add(user);
                    }
                    if(status == 0) {
                        int index = 0;
                        for (UserModel ruser : Commons.favorite_users) {
                            if(ruser.user_id == user.user_id) {
                                Commons.favorite_users.remove(index);
                                break;
                            }
                            index ++;
                        }
                    }
                }
                callback.callback(result, message, response);
            }
        };
        processPostRequest(url, reqParams, new HashMap<String, String>(), callback1);

    }

    public void logOut(final Activity activity){

        HashMap<String, String> params = new HashMap<>();
        HashMap<String, String> reqParams = setModule(params, ReqConst.MODULE_USER, ReqConst.FUNC_LOGOUT);
        final Callback callback1 = new Callback() {
            @Override
            public void callback(Object object) {

            }

            @Override
            public void callback(String result, String message, JSONObject response) {
                if(result.equals(ReqConst.RES_SUCCESS)){

                    Constants.SIGNUP_STATUS = 0;
                    Constants.PROFILE_STATUS = 0;

                    ChatManager.setUserOffline();
                    ChatManager.removeAllListener();
                    Commons.g_user = new UserModel();
                    Commons.chatRooms = new HashMap<>();
                    Commons.all_users = new ArrayList<>();
                    Commons.favorite_users = new ArrayList<>();
                    Commons.myNotifications = new ArrayList<>();
                    Commons.messages = new HashMap<>();
                    Commons.g_isAppRunning = false;

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            activity.startActivity(new Intent(activity, SignInActivity.class));
                            EasyPreference.with(activity).clearAll().save();
                            activity.finish();
                        }
                    });

                }
            }
        };
        processPostRequest(url, reqParams, new HashMap<String, String>(), callback1);
    }

    public void payment6Months(final int amount,final String currency, final String stripeToken, final Callback callback){

        HashMap<String, String> params = new HashMap<>();
        params.put(ReqConst.PARAM_AMOUNT, String.valueOf(amount));
        params.put(ReqConst.PARAM_STRIPE_TOKEN, stripeToken);
        params.put(ReqConst.PARAM_CURRENCY, currency);

        HashMap<String, String> reqParams = setModule(params, ReqConst.MODULE_PAYMENT, ReqConst.FUNC_PAYMENT);
        final Callback callback1 = new Callback() {
            @Override
            public void callback(Object object) {

            }

            @Override
            public void callback(String result, String message, JSONObject response) {
                if(result.equals(ReqConst.RES_SUCCESS)){
                    Commons.g_user.user_paid = response.optInt("user_paid", 0);
                }
                callback.callback(result, message, response);
            }
        };
        processPostRequest(url, reqParams, new HashMap<String, String>(), callback1);
    }

}
