package itwhiz4u.shaadimubark.model;

import org.json.JSONObject;

import java.io.Serializable;

import itwhiz4u.shaadimubark.commons.Commons;

/**
 * Created by ITWhiz4U on 2/6/2018.
 */

public class NotificationModel implements Serializable {

    public static int NOTIFICATION_TYPE_CHAT_REQUESTED     = 1;
    public static int NOTIFICATION_TYPE_CHAT_BLOCKED       = 2;
    public static int NOTIFICATION_TYPE_CHAT_ACCEPTED      = 3;
    public static int NOTIFICATION_TYPE_CHAT_MESSAGE       = 4;

    public int notification_id = 0;
    public int notification_type = 0;
    public UserModel notification_sender = new UserModel();
    public UserModel notification_receiver = new UserModel();
    public int notification_timestamp = 0;
    public int notification_read = 0; // 0: unread ; 1: read
    public String notification_message = "";
    public String  notification_title = "";
    public int notification_item = 0;

    public NotificationModel(){}

    public NotificationModel(JSONObject jsonObject){

        notification_id = jsonObject.optInt("notification_id",0);
        notification_type = jsonObject.optInt("notification_type",0);
        notification_sender = new UserModel(jsonObject);
        notification_receiver = Commons.g_user;
        notification_read = jsonObject.optInt("notification_read",0);
        notification_title = jsonObject.optString("notification_title", "");
        notification_message = jsonObject.optString("notification_message", "");
        notification_timestamp = jsonObject.optInt("notification_timestamp", 0);
        notification_item = jsonObject.optInt("notification_item", 0);

    }
}
