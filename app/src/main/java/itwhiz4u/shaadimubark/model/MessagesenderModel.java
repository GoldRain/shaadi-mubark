package itwhiz4u.shaadimubark.model;

public  class MessagesenderModel {
   public    String date;
    public  int read;
    public  String senderId;
    public  String text;
    public  String type;

    public  MessagesenderModel(String date, int read, String senderId, String text, String type) {
        this.date = date;
        this.read = read;
        this.senderId = senderId;
        this.text = text;
        this.type = type;
    }
}
