package itwhiz4u.shaadimubark.model;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.RestartActivity;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.BaseActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.utils.Callback.Callback;
import itwhiz4u.shaadimubark.utils.ChatManager;
import itwhiz4u.shaadimubark.utils.DateUtils;
import itwhiz4u.shaadimubark.utils.StatePendingActivity;

/**
 * Created by ITWhiz4U on 2/6/2018.
 */

public class ChatModel implements Serializable {


    public int chat_id = 0;
    public UserModel chat_user = new UserModel();
    public String chat_room = "";
    public int chat_createdat = 0;

    public Callback delegate;

    public ChildEventListener chatHandler;


    public int getLastMessageTime(){
        if(Commons.messages.containsKey(chat_user.user_id)) {
            ArrayList<Message> messages = Commons.messages.get(chat_user.user_id);
            if(messages.size() > 0) {
                return (int) (messages.get(messages.size() - 1).getCreatedAt().getTime() / 1000);
            }
        }
        return chat_createdat;
    }

    public Message getLastMessage(){

        if(Commons.messages.containsKey(chat_user.user_id)) {
            ArrayList<Message> messages = Commons.messages.get(chat_user.user_id);
            if(messages.size() > 0) {
                return messages.get(messages.size() - 1);
            }
        }
        return null;
    }
    public ChatModel(){}

    public ChatModel(JSONObject jsonObject){

        chat_id = jsonObject.optInt("chat_id",0);
        chat_user = new UserModel(jsonObject);
        chat_room = jsonObject.optString("chat_room", "");
        chat_createdat = jsonObject.optInt("chat_createdat",0);
        setChatListener();

    }

    public void setChatListener() {
        final ChatManager manager = new ChatManager();

        Callback callback = new Callback() {
            @Override
            public void callback(Object object) {
                if(object != null) {
                    Message message = (Message) object;
                    if(Commons.messages.containsKey(chat_user.user_id)) {
                        Commons.messages.get(chat_user.user_id).add(message);
                    }
                    else {
                        ArrayList<Message> myMessages = new ArrayList<>();
                        myMessages.add(message);
                        Commons.messages.put(chat_user.user_id, myMessages);
                    }
                    if(delegate != null) {
                        delegate.callback(message);
                    }
                    else {

                        if (message.getCreatedAt().getTime() > Commons.loginTime){

                            if (!message.getUser().getId().equals(String.valueOf(Commons.g_user.user_id))){

                                Context context = ShaadiMubarkApplication.getContext();
                                Intent intent = new Intent(context, StatePendingActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 , intent,
                                        PendingIntent.FLAG_ONE_SHOT);

                                String messageContent = "";

                                if (message.getMessagetype().equals("text")){
                                    messageContent = message.getText();
                                } else if (message.getMessagetype().equals("photo")){
                                    messageContent = message.getUser().getName() + context.getString(R.string.shared_photo);
                                }
                                Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                                        .setSmallIcon(R.mipmap.ic_launcher)
                                        .setAutoCancel(true)
                                        .setContentTitle(message.getUser().getName())
                                        .setContentText(messageContent)
                                        .setFullScreenIntent(pendingIntent, true)
                                        //.setAutoCancel(false)
                                        .setSound(defaultSoundUri)
                                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                                        .setContentIntent(pendingIntent);


                                NotificationManager notificationManager =
                                        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                                notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

                            }

                        }

                    }


                }
            }

            @Override
            public void callback(String result, String message, JSONObject response) {

            }
        };
        chatHandler = manager.setChatListener(this, callback);

    }

}

