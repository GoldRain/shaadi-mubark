package itwhiz4u.shaadimubark.model;

import android.text.format.DateFormat;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Message implements IMessage,
        MessageContentType.Image, /*this is for default image messages implementation*/
        MessageContentType /*and this one is for custom content type (in this case - voice message)*/ {

    private String id = "";
    private String text = "";
    private Date createdAt;
    private User user;
    private Image image;
    private Voice voice;

    private String messagetype = "";
    private int readstatus = 0;
    private String chattime = "";
    private String senderid = "";


    public String getMessagetype() {
        return messagetype;
    }

    public Message(String id, User user, String text) {
        this(id, user, text, new Date());
    }

    public Message(String id, User user, String text, Date createdAt) {
        this.id = id;
        this.text = text;
        this.user = user;
        this.createdAt = createdAt;

    }

    public Message(String text, String messagetype, int readstatus, String chattime, String senderid) {
        this.text = text;
        this.messagetype = messagetype;
        this.readstatus = readstatus;
        this.chattime = chattime;
        this.senderid = senderid;
    }

    public Message(String id, String text, String chattime, String senderid, String messagetype, int readstatus, UserModel user) {
        this.id = id;
        this.chattime = chattime;
        this.senderid = senderid;
        this.messagetype = messagetype;
        this.readstatus = readstatus;
        this.createdAt = timestamptodate(this.chattime);
        this.user = new User(this.senderid, user.getName(), user.user_photo,true);

        if(this.messagetype.equals("text")){
            this.text = text;
        }else {
            this.image = new Image(text);
        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public User getUser() {
        return this.user;
    }

    @Override
    public String getImageUrl() {
        return image == null ? null : image.url;
    }

    public Voice getVoice() {
        return voice;
    }

    public String getStatus() {
        return "Sent";
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public static class Image {

        private String url;

        public Image(String url) {
            this.url = url;
        }
    }

    public static class Voice {

        private String url;
        private int duration;

        public Voice(String url, int duration) {
            this.url = url;
            this.duration = duration;
        }

        public String getUrl() {
            return url;
        }

        public int getDuration() {
            return duration;
        }
    }


    public Date timestamptodate(String timestamp1) {

        long timestamp = Long.parseLong(timestamp1);

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();//get your local time zone.
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(tz);//set time zone.
        String localTime = sdf.format(new Date(timestamp));
        Date date = new Date();
        try {
            date = sdf.parse(localTime);//get local date
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;

    }
}
