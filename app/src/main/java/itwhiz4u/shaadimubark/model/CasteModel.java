package itwhiz4u.shaadimubark.model;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

import itwhiz4u.shaadimubark.commons.Commons;

/**
 * Created by ITWhiz4U on 3/22/2018.
 */

public class CasteModel implements Serializable {

    public int caste_id = 0;
    String caste_string_en = "";
    String caste_string_no = "";

    //English
    //norsk bokmål
    public String caste_string() {
        if (Commons.getCurrentLanCode().equals("nb_NO"))
            return caste_string_no;
        return caste_string_en;
    }

    public CasteModel(JSONObject caste){

        caste_id = caste.optInt("caste_id");
        caste_string_en = caste.optString("caste_string_en");
        caste_string_no = caste.optString("caste_string_no");

    }

    public CasteModel(){};

    public static ArrayList<CasteModel> allCastes;
}
