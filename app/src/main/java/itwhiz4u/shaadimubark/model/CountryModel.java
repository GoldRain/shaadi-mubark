package itwhiz4u.shaadimubark.model;

import android.content.Context;
import android.text.TextUtils;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

import itwhiz4u.shaadimubark.commons.Commons;

public class CountryModel implements Serializable {

    public String dialCode = "";
    public String name_en = "";
    public String name_no = "";
    public String code = "";
    public String flag = "";
    public int price = 0;
    public String currency = "";

    //English
    //norsk bokmål
    public String country_string() {
        if (Commons.getCurrentLanCode().equals("nb_NO"))
            return name_no;
        return name_en;
    }

    public CountryModel(JSONObject country){

        dialCode = country.optString("dialCode", "");
        name_en = country.optString("name_en");
        name_no = country.optString("name_no");
        code = country.optString("code", "");
        flag = country.optString("flag", "");
        price = country.optInt("price", 0);
        currency = country.optString("currency","");
    }


    // endregion

    // region Getter/Setter


    public String getDialCode() {
        return dialCode;
    }

    public void setDialCode(String dialCode) {
        this.dialCode = dialCode;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

/*    public void loadFlagByCode(Context context) {
        if (this.flag != -1) {
            return;
        }

        try {
            this.flag = context.getResources()
                    .getIdentifier("flag_" + this.code.toLowerCase(Locale.ENGLISH), "drawable",
                            context.getPackageName());
        } catch (Exception e) {
            e.printStackTrace();
            this.flag = -1;
        }
    }*/
    // endregion

    public CountryModel(){};

    public static ArrayList<CountryModel> allCountry;


}
