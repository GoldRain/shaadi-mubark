package itwhiz4u.shaadimubark.model;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class PriceModel implements Serializable {

    public int price = 0;
    public String currency = "";
    public Boolean checked=false;

    public PriceModel(JSONObject priceJson){

        price = priceJson.optInt("price", 0);
        currency = priceJson.optString("currency", "");
    }

    public PriceModel(){}

    public static ArrayList<PriceModel> allPrices;

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
