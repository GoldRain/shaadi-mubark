package itwhiz4u.shaadimubark.model;

import java.util.Comparator;

public class ChatModelComprator implements Comparator<ChatModel> {
    public int compare(ChatModel chat1, ChatModel chat2) {
        if (chat1.getLastMessageTime() == chat2.getLastMessageTime()) {
            return 0;
        }
        else if(chat1.getLastMessageTime()>chat2.getLastMessageTime()) {
            return -1;
        }
        else {
            return 1;
        }
    }
}
