package itwhiz4u.shaadimubark.model;

import android.annotation.SuppressLint;
import android.util.Log;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;

/**
 * Created by ITWhiz4U on 2/5/2018.
 */

public class UserModel implements Serializable {

    //first sign up
    public int user_id = 0;
    public String user_firstname = "";
    public String user_lastname = "";
    public String user_username = "";
    public String user_facebookid = "";
    public String user_photo = ""; //optional
    public String user_images = "";
    public String user_phonenumber = "";
    public String user_birthday = "";
    public String user_password = "";
    public String user_email = "";

    //terms check

    //2nd professional info
    public Gender user_gender = Gender.possibleValues[0];
    public double user_height = 0;
    public int user_totalsibling = 0;
    public String user_country = "";
    public String user_city = "";
    public MaritalStatus user_maritalstatus = MaritalStatus.possibleValues[0];
    public int user_children = 0;
    public Complexion user_complexion = Complexion.possibleValues[0];
    public BodyType user_bodytype = BodyType.possibleValues[0];
    public Appearance user_appearance = Appearance.possibleValues[0];
    public Hair user_hair = Hair.possibleValues[0]; //men only
    public FacialHair user_facialhair = FacialHair.possibleValues[0]; //men only

    //third religion and caste
    public Religion user_religion = Religion.possibleValues[0];
    public CasteModel user_caste;
    public String user_castestring = "";
    public String user_subcaste = "";

    //fourth Profession information
    public long user_createdat = 0L;
    public EducationLevel user_educationlevel = EducationLevel.possibleValues[0];
    public String user_profession = "";
    public JobStatus user_jobstatus = JobStatus.possibleValues[0];
    public int user_salary = 0;
    public int user_relocatable = 0; //0:false, 1: true
    public int user_ownapartment = 0; //men only ,0:false, 1: true


    public int isFavorite = 0; //0:false, 1: true

    //match requirement

    public int filter_agefrom = 0;
    public int filter_ageto = 0;
    public int filter_heightfrom = 0;
    public int filter_heightto = 0;
    public String filter_country = "";
    public String filter_city = "";
    public Religion filter_religion = Religion.none;
    public EducationLevel filter_minimumedu = EducationLevel.none;
    public int filter_maritalstatus = 0;
    public String filter_professions = "";
    public String filter_caste = "";
    public int filter_havechildren = 0; //0: false , 1 : true
    public int filter_ownapartment = 0; //in case female : 0: false , 1 : true
    public int filter_relocatable = 0; //in case female : 0: false , 1 : true
    public int user_status = 0;
    public int user_seen = 0;
    public int user_paid = 0;
    public Boolean user_online = false;

    public String[] user_imageArray;

    //filter caste
    public ArrayList<CasteModel> filter_casteArray() {
        ArrayList<CasteModel> resultArray = new ArrayList<>();
        for( CasteModel caste: CasteModel.allCastes) {
            if (casteIsInFilter(caste.caste_id)) {
                resultArray.add(caste);
            }
        }
        return resultArray;
    }
    public String filter_casteString() {
        String result = "";
        for (CasteModel caste: filter_casteArray()) {
            if (result.length() > 0) {
                result += ", " + caste.caste_string();
            }
            else {
                result = caste.caste_string();
            }
        }
        return result;
    }

    public Boolean casteIsInFilter(int caste){
        if (filter_caste.startsWith(String.valueOf(caste) + ",") || filter_caste.endsWith("," + String.valueOf(caste)) || filter_caste.contains("," + String.valueOf(caste) +",")
                || filter_caste == String.valueOf(caste)) {
            return true;
        }
        return false;
    }

    //filter country
    public ArrayList<CountryModel> filter_countryArray(){
        ArrayList<CountryModel> resultArray = new ArrayList<>();
        for (CountryModel country : CountryModel.allCountry){
            if (countryIsInFilter(country.code)){
                resultArray.add(country);
            }
        }
        return resultArray;
    }

    public String filter_countryString() {
        String result = "";
        for (CountryModel country: filter_countryArray()) {
            if (result.length() > 0) {
                result += ", " + country.country_string();
            }
            else {
                result = country.country_string();
            }
        }
        return result;
    }

    public Boolean countryIsInFilter(String country){
        if (filter_country.startsWith(country + ",") || filter_country.endsWith("," + country) || filter_country.contains("," + country +",")
                || filter_country.equals(country)) {
            return true;
        }
        return false;
    }

    public String filter_maritalstatusString() {
        String result = "";
        for (MaritalStatus status: MaritalStatus.possibleValues) {
            if((status.rawValue & filter_maritalstatus) > 0) {
                if(result.length() > 0) {
                    result += ", " + status.getString();
                }
                else {
                    result = status.getString();
                }
            }
        }
        return result;
    }

    public UserModel(){};

    public UserModel(JSONObject jsonObject){

        //1st sign up
        user_id = jsonObject.optInt("user_id", 0);
        user_firstname = jsonObject.optString("user_firstname", "");
        user_lastname = jsonObject.optString("user_lastname", "");
        user_username = jsonObject.optString("user_username", "");
        user_facebookid = jsonObject.optString("user_facebookid", "");
        user_photo = jsonObject.optString("user_photo", "");
        user_images = jsonObject.optString("user_images", "");
        user_phonenumber = jsonObject.optString("user_phonenumber", "");
        user_birthday = jsonObject.optString("user_birthday", "");
        user_password = jsonObject.optString("user_password", "");
        user_email = jsonObject.optString("user_email", "");

        //2nd personal info
        user_gender = Gender.getGender(jsonObject.optInt("user_gender"));                  //gender enum
        user_height = jsonObject.optDouble("user_height", 0);
        user_totalsibling = jsonObject.optInt("user_totalsibling", 0);
        user_country = jsonObject.optString("user_country", "");
        user_city = jsonObject.optString("user_city", "");
        user_maritalstatus = MaritalStatus.getMaritalStatus(jsonObject.optInt("user_maritalstatus",0));
        user_children = jsonObject.optInt("user_children", 0);
        user_complexion = Complexion.getComplexion(jsonObject.optInt("user_complexion",0));  //complexion enum
        user_bodytype = BodyType.getBodyType(jsonObject.optInt("user_bodytype",0));          //body type enum
        user_appearance = Appearance.getAppearance(jsonObject.optInt("user_appearance",0)); //Appearance enum
        user_hair = Hair.getHair(jsonObject.optInt("user_hair")); //Hair enum
        user_facialhair = FacialHair.getFacialHair(jsonObject.optInt("user_facialhair",0)); //Facial Hair enum

        //3rd religion and caste
        user_religion = Religion.getReligion(jsonObject.optInt("user_religion"));          //Religion enum
        int caste_id =  jsonObject.optInt("user_caste", 0);
        if(caste_id > 0) {
            for(CasteModel caste: CasteModel.allCastes) {
                if(caste.caste_id == caste_id) {
                    user_caste = caste;
                }
            }
        }
        user_castestring = jsonObject.optString("user_castestring", "");
        user_subcaste = jsonObject.optString("user_subcaste", "");

        //4th professional info
        user_createdat = jsonObject.optLong("user_createdat");
        user_educationlevel = EducationLevel.getEducationLevel(jsonObject.optInt("user_educationlevel"));
        user_profession = jsonObject.optString("user_profession", "");
        user_jobstatus = JobStatus.getJobStatus(jsonObject.optInt("user_jobstatus"));
        user_salary = jsonObject.optInt("user_salary", 0);
        user_relocatable = jsonObject.optInt("user_relocatable", 0);                          //0: false, 1:true
        user_ownapartment = jsonObject.optInt("user_ownapartment", 0);                        //0: false, 1:true
        isFavorite = jsonObject.optInt("isFavorite", 0);                                      //0: false, 1:true

        //match requirement
        filter_agefrom = jsonObject.optInt("filter_agefrom", 0);
        filter_ageto = jsonObject.optInt("filter_ageto", 0);
        filter_heightfrom = jsonObject.optInt("filter_heightfrom", 0);
        filter_heightto = jsonObject.optInt("filter_heightto", 0);
        filter_country = jsonObject.optString("filter_country", "");
        filter_city = jsonObject.optString("filter_city", "");
        filter_religion = Religion.getReligion(jsonObject.optInt("filter_religion"));
        filter_minimumedu = EducationLevel.getEducationLevel(jsonObject.optInt("filter_minimumedu"));
        filter_maritalstatus = jsonObject.optInt("filter_maritalstatus", 0);
        filter_professions = jsonObject.optString("filter_professions", "");
        filter_caste = jsonObject.optString("filter_caste");
        filter_havechildren = jsonObject.optInt("filter_havechildren",0);
        filter_ownapartment = jsonObject.optInt("filter_ownapartment", 0);
        filter_relocatable = jsonObject.optInt("filter_relocatable" , 0);

        user_status = jsonObject.optInt("user_status", 0);
        user_seen = jsonObject.optInt("user_seen", 0); //
        user_paid = jsonObject.optInt("user_paid", 0); //0: not paid , 1: paid

    }

    public UserModel copyUser() {

        UserModel user = new UserModel();

        user.user_id = user_id;
        user.user_firstname = user_firstname;
        user.user_lastname = user_lastname;
        user.user_username  = user_username ;
        user.user_facebookid  = user_facebookid ;
        user.user_photo  = user_photo ;
        user.user_images  = user_images ;
        user.user_phonenumber  = user_phonenumber ;
        user.user_birthday  = user_birthday ;
        user.user_password  = user_password ;
        user.user_email  = user_email ;


        user.user_gender  = user_gender ;
        user.user_height  = user_height ;
        user.user_totalsibling  = user_totalsibling ;
        user.user_country  = user_country ;
        user.user_city  = user_city ;
        user.user_maritalstatus = user_maritalstatus;
        user.user_children = user_children;
        user.user_complexion  = user_complexion ;
        user.user_bodytype  = user_bodytype ;
        user.user_appearance  = user_appearance ;
        user.user_hair  = user_hair ;
        user.user_facialhair  = user_facialhair ;


        user.user_religion  = user_religion ;
        user.user_subcaste  = user_subcaste ;
        user.user_castestring = user_castestring;
        user.user_caste = user_caste;

        user.user_createdat  = user_createdat ;
        user.user_educationlevel = user_educationlevel;
        user.user_profession  = user_profession ;
        user.user_jobstatus  = user_jobstatus ;
        user.user_salary  = user_salary ;
        user.user_relocatable  = user_relocatable ;
        user.user_ownapartment  = user_ownapartment ;

        user.isFavorite  = isFavorite ;

        user.filter_agefrom  = filter_agefrom ;
        user.filter_ageto  = filter_ageto ;
        user.filter_heightfrom  = filter_heightfrom ;
        user.filter_heightto  = filter_heightto ;
        user.filter_country = filter_country;
        user.filter_city = filter_city;
        user.filter_religion  = filter_religion ;
        user.filter_minimumedu  = filter_minimumedu ;
        user.filter_maritalstatus = filter_maritalstatus;
        user.filter_professions  = filter_professions ;
        user.filter_caste  = filter_caste ;
        user.filter_havechildren = filter_havechildren;
        user.filter_ownapartment  = filter_ownapartment ;
        user.filter_relocatable  = filter_relocatable ;
        user.user_status  = user_status ;
        user.user_seen  = user_seen ;
        user.user_paid  = user_paid ;

        return user;
    }

    public CountryModel getCurrency(){

        CountryModel currency =  new CountryModel();

        for (int i = 0; i < CountryModel.allCountry.size(); i++){
            if (CountryModel.allCountry.get(i).code.equals(user_country)){
                currency = CountryModel.allCountry.get(i);
                break;
            }
        }

        return currency;
    }

    public String getName(){
        return user_firstname + " " + user_lastname;
    }

    public int getAge(){

        if (user_birthday.length() > 0) {


            String[] dates = user_birthday.split("-");
            int compareValue = Integer.parseInt(dates[0]) * 10000 + Integer.parseInt(dates[1]) * 100 + Integer.parseInt(dates[2]);
            Date date = new Date();
            @SuppressLint({"NewApi", "LocalSuppress"}) Calendar calendar = Calendar.getInstance();

            @SuppressLint({"NewApi", "LocalSuppress"}) int value = calendar.get(Calendar.YEAR) * 10000 + calendar.get(Calendar.MONTH + 1) * 100 + calendar.get(Calendar.DATE);

            Log.d("month==>", String.valueOf(calendar.get(Calendar.MONTH)));
            Log.d("data==>", String.valueOf(value));
            return (value - compareValue) / 10000;
        }

        return 0;
    }


    public enum MaritalStatus {

        none(0),
        single(1),
        divorced(2),
        widowed(4);

        public String getString() {
            switch (rawValue){
                case 0:
                    return   ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.single);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.divorced);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.widowed);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;

        private MaritalStatus(int _value) {
            rawValue = _value;
        }

        public static MaritalStatus[] possibleValues = MaritalStatus.none.getDeclaringClass().getEnumConstants();

        public static MaritalStatus getMaritalStatus(int value) {

            switch (value) {
                case 0:
                    return MaritalStatus.none;
                case 1:
                    return MaritalStatus.single;
                case 2:
                    return MaritalStatus.divorced;
                case 4:
                    return MaritalStatus.widowed;

                default:
                    return MaritalStatus.none;
            }
        }
    }

    public enum Complexion {

        none(0),
        fair(1),
        medium(2),
        dark(4);

        public String getString() {
            switch (rawValue){
                case 0:
                    return   ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.fair);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.medium);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.dark);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;

        private Complexion(int _value) {
            rawValue = _value;
        }

        public static Complexion[] possibleValues = Complexion.none.getDeclaringClass().getEnumConstants();

        public static Complexion getComplexion(int value) {

            switch (value) {
                case 0:
                    return Complexion.none;
                case 1:
                    return Complexion.fair;
                case 2:
                    return Complexion.medium;
                case 4:
                    return Complexion.dark;

                default:
                    return Complexion.none;
            }
        }
    }


    public enum Gender {
        none(0),
        male(1),
        female(2);

        public String getString() {
            switch (rawValue){
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.male);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.female);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;

        private Gender(int value) {
            rawValue = value;
        }

        public static Gender[] possibleValues = Gender.none.getDeclaringClass().getEnumConstants();

        public static Gender getGender(int rawValue) {
            switch (rawValue){
                case 0:
                    return Gender.none;
                case 1:
                    return Gender.male;
                case 2:
                    return Gender.female;
                default:
                    return Gender.none;
            }
        }
    }

    public enum BodyType {

        none(0),
        slim(1),
        normal(2),
        abovenormal(4);

        public String getString(){
            switch (rawValue){

                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.slim);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.normal);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.above_normal);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;

        private BodyType(int value){
            rawValue = value;
        }

        public static BodyType[] possibleValues = BodyType.none.getDeclaringClass().getEnumConstants();

        public static BodyType getBodyType(int rawValue){
            switch (rawValue){
                case 0:
                    return BodyType.none;
                case 1:
                    return BodyType.slim;
                case 2:
                    return BodyType.normal;
                case 4:
                    return BodyType.abovenormal;

                default:
                    return BodyType.none;
            }
        }
    }


    public enum Appearance{

        none(0),
        normal(1),
        attractive(2),
        veryattractive(4);

        public String getString(){

            switch (rawValue){
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.normal);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.attractive);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.very_attractive);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;
        private Appearance(int value){
            rawValue = value;
        }

        public static Appearance[] possibleValues = Appearance.none.getDeclaringClass().getEnumConstants();

        public static Appearance getAppearance(int rawValue){
            switch (rawValue){
                case 0:
                    return Appearance.none;

                case 1:
                    return Appearance.normal;

                case 2:
                    return Appearance.attractive;
                case 4:
                    return Appearance.veryattractive;
                default:
                    return Appearance.none;
            }
        }
    }

    public enum Hair{

        none(0),
        normal(1),
        thin(2),
        bald(4);

        public String getString(){

            switch (rawValue){
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.normal);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.thin);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.bald);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;

        private Hair(int value){
            rawValue = value;
        }

        public static Hair[] possibleValues = Hair.none.getDeclaringClass().getEnumConstants();

        public static Hair getHair(int rawValue){
            switch (rawValue){
                case 0:
                    return Hair.none;
                case 1:
                    return Hair.normal;
                case 2:
                    return Hair.thin;
                case 4:
                    return Hair.bald;
                default:
                    return Hair.none;
            }
        }
    }

    public enum FacialHair{

        none(0),
        cleanshaved(1),
        mustache(2),
        shortbeard(4),
        fullbeard(8);

        public String getString(){
            switch (rawValue){
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.clean_shaved);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.mustache);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.short_beard);
                case 8:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.full_beard);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;

        private FacialHair(int value){
            rawValue = value;
        }

        public static FacialHair[] possibleValues = FacialHair.none.getDeclaringClass().getEnumConstants();

        public static FacialHair getFacialHair(int rawValue) {
            switch (rawValue){

                case 0:
                    return FacialHair.none;
                case 1:
                    return FacialHair.cleanshaved;
                case 2:
                    return FacialHair.mustache;
                case 4:
                    return FacialHair.shortbeard;
                case 8:
                    return FacialHair.fullbeard;
                default:
                    return FacialHair.none;
            }
        }
    }

    public enum Religion{

        none(0),
        sunni(1),
        shia(2);

        public String getString(){
            switch (rawValue) {
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.sunni);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.shia);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;
        private Religion(int value){
            rawValue = value;
        }

        public static Religion[] possibleValues = Religion.none.getDeclaringClass().getEnumConstants();

        public static Religion getReligion(int rawValue){

            switch (rawValue){
                case 0:
                    return Religion.none;
                case 1:
                    return Religion.sunni;
                case 2:
                    return Religion.shia;
                default:
                    return Religion.none;
            }
        }
    }

    public enum EducationLevel {

        none(0),
        bachelor(1),
        masters(2),
        phd(4),
        other(8);

        public String getString() {
            switch (rawValue) {
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.bachelor);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.master);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.phd);
                case 8:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.other);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;
        private EducationLevel (int value){
            rawValue = value;
        }

        public static EducationLevel[] possibleValues = EducationLevel.none.getDeclaringClass().getEnumConstants();

        public static EducationLevel getEducationLevel(int rawValue){
            switch (rawValue){
                case 0:
                    return EducationLevel.none;
                case 1:
                    return EducationLevel.bachelor;
                case 2:
                    return EducationLevel.masters;
                case 4:
                    return EducationLevel.phd;
                case 8:
                    return EducationLevel.other;
                default:
                    return EducationLevel.none;
            }
        }
    }

    public enum JobStatus {

        none(0),
        fulltime(1),
        parttime(2),
        jobless(4);

        public String getString() {
            switch (rawValue) {
                case 0:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
                case 1:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.full_time);
                case 2:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.part_time);
                case 4:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.jobless);
                default:
                    return ShaadiMubarkApplication.getInstance().getString(R.string.not_selected);
            }
        }

        public int rawValue;
        private JobStatus (int value){
            rawValue = value;
        }

        public static JobStatus[] possibleValues = JobStatus.none.getDeclaringClass().getEnumConstants();

        public static JobStatus getJobStatus(int rawValue){
            switch (rawValue){
                case 0:
                    return JobStatus.none;
                case 1:
                    return JobStatus.fulltime;
                case 2:
                    return JobStatus.parttime;
                case 4:
                    return JobStatus.jobless;
                default:
                    return JobStatus.none;
            }
        }

    }

}



