package itwhiz4u.shaadimubark.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.iamhabib.easy_preference.EasyPreference;
import com.nex3z.flowlayout.FlowLayout;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.CookieManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.SignInActivity;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.activity.TermsPolicyActivity;
import itwhiz4u.shaadimubark.activity.UserProfileActivity;
import itwhiz4u.shaadimubark.activity.VerifyActivity;
import itwhiz4u.shaadimubark.adapter.CurrencyListViewAdapter;
import itwhiz4u.shaadimubark.adapter.SpinnerAdapter;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.PriceModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;
import itwhiz4u.shaadimubark.utils.Callback.Callback;
import itwhiz4u.shaadimubark.utils.ChatManager;
import itwhiz4u.shaadimubark.utils.DateUtils;


public class SettingsFragment extends Fragment {

    @BindView(R.id.ryt_profile) RelativeLayout ryt_profile;
    @BindView(R.id.ryt_delete) RelativeLayout ryt_delete;
    @BindView(R.id.ryt_pay) RelativeLayout ryt_pay;
    @BindView(R.id.txv_pay) TextView txv_pay;
    @BindView(R.id.ryt_about_us) RelativeLayout rlt_about_us;
    @BindView(R.id.ryt_share) RelativeLayout ryt_share;
    @BindView(R.id.ryt_logout) RelativeLayout ryt_logout;

    MainActivity activity;
    View view;

    //currency
    ArrayList<PriceModel> currencies = new ArrayList<>();
    int amount = 0;
    String currency = "";

    public SettingsFragment() {
        // Required empty public constructor
    }

    Dialog dialog1;
    CurrencyListViewAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Constants.PAGE_TO = "";

        view =  inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        //display pay date
        if (Commons.g_user.user_paid > 0){
            String paid = DateUtils.getDate(Commons.g_user.user_paid, "dd.MM.yyy");
            txv_pay.setText(activity.getString(R.string.available_until) + " " + paid);
        }
        return  view;
    }

    @OnClick(R.id.ryt_profile) void gotoProfile(){

        Constants.PAGE_TO = Constants.SETTING_PAGE;
        Intent intent = new Intent(activity, UserProfileActivity.class);
        intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
        activity.startActivity(intent);
        activity.finish();

    }

    @OnClick(R.id.ryt_delete) void gotoDeleteProfile(){

        deleteProfileDialog();

    }

    @OnClick(R.id.ryt_pay) void chooseCurrency(){

        /*if (Commons.g_user.user_paid < System.currentTimeMillis()/1000){

            dialog1 = new Dialog(activity);
            dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog1.setContentView(R.layout.alert_currency);
            dialog1.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            dialog1.setCancelable(true);
            dialog1.setCanceledOnTouchOutside(true);

            Button btn_confirm = (Button)dialog1.findViewById(R.id.btn_confirm);

            ListView lstCurrency = (ListView)dialog1.findViewById(R.id.lst_currency);
            adapter = new CurrencyListViewAdapter(this);
            lstCurrency.setAdapter(adapter);


            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (amount == 0){
                        activity.showAlertDialog(activity.getString(R.string.select_currency));
                    }else {

                        gotoPay6Months(currency,amount);
                        dialog1.dismiss();
                    }
                }
            });

            dialog1.show();
        }*/

        CountryModel currency = Commons.g_user.getCurrency();
        gotoPay6Months(currency.currency, currency.price);

        Log.d("currency==>", currency.currency);

    }

 /*   public void updateadapter(int position){

        for(int i =0; i<PriceModel.allPrices.size(); i++){
            if(i == position){
                PriceModel.allPrices.get(i).setChecked(true);
                amount = PriceModel.allPrices.get(i).price;
                currency = PriceModel.allPrices.get(i).currency;
            }

            else{
                PriceModel.allPrices.get(i).setChecked(false);
            }

            adapter.notifyDataSetChanged();
        }
    }*/

    public void gotoPay6Months(final String sp_currency, final int sp_amount){

        if (Commons.g_user.user_paid < System.currentTimeMillis() / 1000){

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.alert_payment);
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);

            final CardInputWidget mCardInputWidget = (CardInputWidget) dialog.findViewById(R.id.card_input_widget);

            TextView txvTitle = (TextView)dialog.findViewById(R.id.txv_title);
            TextView txv_confirm = (TextView)dialog.findViewById(R.id.txv_confirm);
            TextView txv_cancel = (TextView)dialog.findViewById(R.id.txv_cancel);

            txvTitle.setText(activity.getString(R.string.membership) + " - " + sp_amount +  " " + sp_currency);
            txv_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Card card = mCardInputWidget.getCard();
                    if (card == null) {
                        // Do not continue token creation.
                        Toast.makeText(activity,activity.getString(R.string.no_existCard), Toast.LENGTH_SHORT).show();

                    }else {

                        activity.showKProgress();

                        Stripe stripe = new Stripe(activity, "pk_live_99F9rEolqUk26NsKTZ4iwZHI");
                        stripe.createToken(
                                card,
                                new TokenCallback() {
                                    public void onSuccess(Token token) {

                                        final Callback callback = new Callback() {
                                            @Override
                                            public void callback(Object object) {

                                            }

                                            @Override
                                            public void callback(String result, final String message, JSONObject response) {

                                                new Thread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        activity.closeKProgress();
                                                    }
                                                });

                                                if (result.equals(ReqConst.RES_SUCCESS)){
                                                    final String paid = DateUtils.getDate(Commons.g_user.user_paid
                                                            , "dd.MM.yyy");

                                                    activity.closeKProgress();
                                                    txv_pay.setText(activity.getString(R.string.available_until) + " " + paid);
                                                    activity.showToast(message);

                                                    new Thread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            activity.closeKProgress();
                                                            txv_pay.setText(activity.getString(R.string.available_until) + " " + paid);
                                                            activity.showToast(message);
                                                        }
                                                    });
                                                }
                                            }
                                        };
                                        (new ApiHelper()).payment6Months(sp_amount,sp_currency, token.getId(), callback);
                                        dialog.dismiss();

                                    }
                                    public void onError(Exception error) {
                                        // Show localized error message
                                        activity.closeKProgress();
                                        Toast.makeText(getContext(),
                                                "Error",
                                                Toast.LENGTH_LONG
                                        ).show();
                                    }
                                }
                        );
                    }

                }
            });

            txv_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }

    }


    @OnClick(R.id.ryt_about_us) void gotoAboutUs(){

        Intent intent = new Intent(activity, TermsPolicyActivity.class);
        activity.startActivity(intent);
    }

    @OnClick(R.id.ryt_share) void gotoShare(){

        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setAction(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        /*share.putExtra(Intent.EXTRA_SUBJECT,"");
        share.putExtra(Intent.EXTRA_TEXT,"" );*/

        //startActivity(Intent.createChooser(share, ""));
        startActivity(share);
    }

    @OnClick(R.id.ryt_logout) void logOut(){

        logoutDialog();
    }

    private void logoutDialog(){

        new SweetAlertDialog(activity,SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getString(R.string.are_sure))
                .setContentText(getString(R.string.will_logout))
                .setConfirmText(getString(R.string.log_out))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                (new ApiHelper()).logOut(activity);

                                sweetAlertDialog.cancel();
                                sweetAlertDialog.dismiss();

                            }
                        }, 1000);

                    }
                })
                .setCancelText(getString(R.string.cancel))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                }).show();
    }

    public void deleteProfileDialog(){

        SweetAlertDialog alertDialog = new SweetAlertDialog(activity,SweetAlertDialog.WARNING_TYPE);
        alertDialog
                .setTitleText(getString(R.string.delete_profile))
                .setContentText(getString(R.string.confirm_delete))
                .setConfirmText(getString(R.string.delete_string))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog
                                .setTitleText(getString(R.string.deleted))
                                .setContentText(getString(R.string.profile_deleted))
                                .setConfirmText(getString(R.string.ok))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        deleteUser();
                                        sweetAlertDialog.cancel();
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .showCancelButton(false)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                    }
                })
                .setCancelText(getString(R.string.cancel))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                })
                .show();
    }


    private void deleteUser(){

        activity.showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseDeleteUser(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                activity.closeKProgress();
                activity.showAlertDialog(activity.getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_DELETE_USER);

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.SESSION, EasyPreference.with(activity).getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseDeleteUser(String json){

        activity.closeKProgress();
        try {

            JSONObject response = new JSONObject(json);
            String  result = response.getString(ReqConst.RESULT);
            String msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                //delete profile
                ChatManager.notifyDeleteProfile();

                Constants.SIGNUP_STATUS = 0; //sign up status

                EasyPreference.with(activity).clearAll().save();
                activity.startActivity(new Intent(activity, SignInActivity.class));
                activity.finish();

                Constants.SIGNUP_STATUS = 0;
                Constants.PROFILE_STATUS = 0;

                ChatManager.setUserOffline();
                ChatManager.removeAllListener();
                Commons.g_user = new UserModel();
                Commons.chatRooms = new HashMap<>();
                Commons.all_users = new ArrayList<>();
                Commons.favorite_users = new ArrayList<>();
                Commons.myNotifications = new ArrayList<>();
                Commons.messages = new HashMap<>();
                Commons.g_isAppRunning = false;

            } else {

                activity.closeKProgress();
                activity.showAlertDialog(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

}
