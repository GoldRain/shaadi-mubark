package itwhiz4u.shaadimubark.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.iamhabib.easy_preference.EasyPreference;
import com.nex3z.flowlayout.FlowLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.adapter.SpinnerAdapter;
import itwhiz4u.shaadimubark.adapter.UserGridViewAdapter;
import itwhiz4u.shaadimubark.circleRefresh.SwipeRefreshLayout;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;


@SuppressLint("ValidFragment")
public class SearchFragment extends Fragment implements View.OnClickListener{

    MainActivity activity;
    View view;
    @BindView(R.id.gdv_search) GridView gdv_search;
    UserGridViewAdapter _adapter;


    SwipeRefreshLayout ui_refreshLayout;
    int _pageIndex = 0;

    @BindView(R.id.edt_search) EditText edt_search;
    @BindView(R.id.imv_cancel) ImageView imv_cancel;
    ImageView imv_filter;


    //Filter dialog part
    TextView txv_min_year,txv_max_year,txv_min_height,txv_max_height,txvCaste, txvCountry;
    Spinner spn_religion, spn_min_education;
    SpinnerAdapter adapter_spn;

    //UserModel user ;
    String searchString = "";
    boolean isFilter = false;

    EditText edt_professions;
    ArrayList<CasteModel> strCaste = new ArrayList<>();
    ArrayList<CountryModel> strCountry = new ArrayList<>();

    RadioGroup radioGroupOwnApart, radioMustRelocate;
    RadioButton radioOwnApartYes,radioOwnApartNo, radioMustReYes, radioMustReNo;
    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);

        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }


    private void loadLayout() {

        imv_filter = (ImageView)activity.findViewById(R.id.imv_mark);
        imv_filter.setOnClickListener(this);

        _adapter = new UserGridViewAdapter(activity, Commons.all_users);
        gdv_search.setAdapter(_adapter);
        _adapter.notifyDataSetChanged();

        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH){
                    searchString = edt_search.getText().toString();
                    Commons.all_users.clear();
                    searchUsers(searchString);
                }
                return false;
            }
        });


        ui_refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh) ;

        ui_refreshLayout.setRefreshDrawableStyle(SwipeRefreshLayout.CIRCLE);
        ui_refreshLayout.setPullPosition(Gravity.BOTTOM);
        ui_refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        ui_refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchUsers(searchString);
            }
        });
    }


    private void searchUsers(final String keyword){

        ui_refreshLayout.setRefreshing(true);

        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseAllUser(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ui_refreshLayout.setRefreshing(false);
                activity.showAlertDialog(activity.getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();


                params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                params.put(ReqConst.FUNCTION, ReqConst.FUNC_SEARCH_USER);
                params.put(ReqConst.SESSION, EasyPreference.with(activity).getString(PrefConst.PREFKEY_SESSION, ""));

                params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                if(Commons.all_users.size() > 0) {
                    params.put(ReqConst.PARAM_USER_CREATEDAT, String.valueOf(Commons.all_users.get(Commons.all_users.size() - 1).user_createdat));
                }
                params.put(ReqConst.PARAM_SEARCH_STRING, keyword);
                //filter module

                if (Commons.filter_user != null && isFilter){

                    if (edt_professions.getText().toString().length() != 0)
                        Commons.filter_user.filter_professions = edt_professions.getText().toString();

                    params.put(ReqConst.PARAM_FILTER_AGEFROM, String.valueOf(Commons.filter_user.filter_agefrom));
                    params.put(ReqConst.PARAM_FILTER_AGETO, String.valueOf(Commons.filter_user.filter_ageto));
                    params.put(ReqConst.PARAM_FILTER_HEIGHTFROM, String.valueOf(Commons.filter_user.filter_heightfrom));
                    params.put(ReqConst.PARAM_USER_FILTER_HEIGHTTO, String.valueOf(Commons.filter_user.filter_heightto));

                    params.put(ReqConst.PARAM_FILTER_RELIGION, String.valueOf(Commons.filter_user.filter_religion.rawValue));
                    params.put(ReqConst.PARAM_FILTER_MINEDUCATION, String.valueOf(Commons.filter_user.filter_minimumedu.rawValue));
                    params.put(ReqConst.PARAM_USER_FILTER_PROFESSIONS, String.valueOf(Commons.filter_user.filter_professions));
                    params.put(ReqConst.PARAM_FILTER_CASTE, String.valueOf(Commons.filter_user.filter_caste));
                    params.put(ReqConst.PARAM_FILTER_COUNTRY, Commons.filter_user.filter_country);
                    params.put(ReqConst.PARAM_FILTER_OWNAPARTMENT, String.valueOf(Commons.filter_user.filter_ownapartment));
                    params.put(ReqConst.PARAM_FILTER_RELOCATABLE, String.valueOf(Commons.filter_user.filter_relocatable));

                }

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseAllUser(String json){

        ui_refreshLayout.setRefreshing(false);

        try {
            JSONObject response = new JSONObject(json);
            String result = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                isFilter = false;
                //Commons.all_users = new ArrayList<>();

                JSONArray jsonUsers = response.getJSONArray(ReqConst.RES_USERS);
                for (int i = 0; i < jsonUsers.length(); i++){

                    Commons.all_users.add(new UserModel(jsonUsers.getJSONObject(i)));
                }


                _adapter.setDatas(Commons.all_users);
                _adapter.notifyDataSetChanged();

                //get caste
                JSONArray castes = response.getJSONArray(ReqConst.RES_CASTE);
                CasteModel.allCastes = new ArrayList<>();
                for (int i = 0; i < castes.length(); i ++){
                    CasteModel.allCastes.add(new CasteModel(castes.getJSONObject(i)));
                }

            } else {

                if (result_msg.equals(activity.getString(R.string.session_tampered))){
                    ApiHelper manager = new ApiHelper();
                    manager.logOut(activity);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.imv_cancel) void clearSearch(){
        edt_search.setText("");
    }

    //Advance Filter part
    private void advancedFilter(){

        if (Commons.filter_user == null)
            Commons.filter_user = new UserModel();

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dialog_filter);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        CrystalRangeSeekbar range_year = (CrystalRangeSeekbar)dialog.findViewById(R.id.range_year);
        CrystalRangeSeekbar range_height = (CrystalRangeSeekbar)dialog.findViewById(R.id.range_height);


        txv_min_year = (TextView)dialog.findViewById(R.id.txv_min_year);
        txv_max_year = (TextView)dialog.findViewById(R.id.txv_max_year);

        txv_min_height = (TextView)dialog.findViewById(R.id.txv_min_height);
        txv_max_height = (TextView)dialog.findViewById(R.id.txv_max_height);


        spn_religion = (Spinner)dialog.findViewById(R.id.spn_religion);
        spn_min_education = (Spinner)dialog.findViewById(R.id.spn_min_education);

        txvCaste = (TextView)dialog.findViewById(R.id.txvCaste) ;
        txvCountry = (TextView)dialog.findViewById(R.id.txvCountry) ;
        edt_professions = (EditText)dialog.findViewById(R.id.edt_professions) ;


        radioGroupOwnApart = (RadioGroup) dialog.findViewById(R.id.radioGroupOwnApart);
        radioMustRelocate = (RadioGroup) dialog.findViewById(R.id.radioMustRelocate);

        radioOwnApartYes = (RadioButton) dialog.findViewById(R.id.radioOwnApartYes);
        radioOwnApartNo = (RadioButton) dialog.findViewById(R.id.radioOwnApartNo);
        radioMustReYes = (RadioButton) dialog.findViewById(R.id.radioMustReYes);
        radioMustReNo = (RadioButton) dialog.findViewById(R.id.radioMustReNo);

        LinearLayout lyt_apart_relocate = (LinearLayout)dialog.findViewById(R.id.lyt_apart_relocate);

        List<String> lstReligion = new ArrayList<>();
        List<String> lstMinEdu = new ArrayList<>();

        //Age from ~ to

        if (Commons.filter_user.filter_ageto != 0 && Commons.filter_user.filter_agefrom != 0)
            range_year.setMinStartValue(Commons.filter_user.filter_agefrom).setMaxStartValue(Commons.filter_user.filter_ageto).apply();

        range_year.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                txv_min_year.setText(String.valueOf(minValue));
                txv_max_year.setText(String.valueOf(maxValue));

                Commons.filter_user.filter_agefrom = (int)minValue.intValue();
                Commons.filter_user.filter_ageto = (int) maxValue.intValue();
            }
        });



        //Height from ~ to

        if (Commons.filter_user.filter_heightfrom != 0 && Commons.filter_user.filter_heightto != 0)
            range_height.setMinStartValue(Commons.filter_user.filter_heightfrom).setMaxStartValue(Commons.filter_user.filter_heightto).apply();

        range_height.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                txv_min_height.setText(String.valueOf(minValue));
                txv_max_height.setText(String.valueOf(maxValue));

                Commons.filter_user.filter_heightfrom = (int)minValue.intValue();
                Commons.filter_user.filter_heightto = (int)maxValue.intValue();
            }
        });


        //Religion

        for (UserModel.Religion religions : UserModel.Religion.possibleValues){
            lstReligion.add(religions.getString());
        }

        adapter_spn = new SpinnerAdapter(activity, R.layout.spinner_item, android.R.id.text1, lstReligion);
        spn_religion.setAdapter(adapter_spn);
        //spn_religion.setSelection(_adapter.getCount());
        for (int i = 0; i < UserModel.Religion.possibleValues.length ; i++){
            if (Commons.filter_user.filter_religion == UserModel.Religion.possibleValues[i]){
                spn_religion.setSelection(i);
            }
        }

        spn_religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 3){

                    Commons.filter_user.filter_religion = UserModel.Religion.possibleValues[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Minimum Education Level
        for (UserModel.EducationLevel minEducation : UserModel.EducationLevel.possibleValues){
            lstMinEdu.add(minEducation.getString());
        }

        adapter_spn = new SpinnerAdapter(activity, R.layout.spinner_item, android.R.id.text1, lstMinEdu);
        spn_min_education.setAdapter(adapter_spn);

        for (int i = 0; i < UserModel.EducationLevel.possibleValues.length; i++){
            if (Commons.filter_user.filter_minimumedu == UserModel.EducationLevel.possibleValues[i]){
                spn_min_education.setSelection(i);
            }
        }
        spn_min_education.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 5){
                    Commons.filter_user.filter_minimumedu = UserModel.EducationLevel.possibleValues[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        txvCaste.setText(Commons.filter_user.filter_casteString());
        txvCaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCaste();
            }
        });

        txvCountry.setText(Commons.filter_user.filter_countryString());
        txvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCountries();
            }
        });


        if (!Commons.filter_user.filter_professions.equals(""))
            edt_professions.setText(Commons.filter_user.filter_professions);



        //in case only women
        if (Commons.g_user.user_gender == UserModel.Gender.female){

            lyt_apart_relocate.setVisibility(View.VISIBLE);

            //own apartment
            switch (Commons.filter_user.filter_ownapartment){

                case 0:
                    radioOwnApartNo.setChecked(true);
                    break;
                case 1:
                    radioOwnApartYes.setChecked(true);
                    break;
                default:
                    radioOwnApartNo.setChecked(true);
                    break;
            }

            radioGroupOwnApart.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioOwnApartYes.isChecked()) {
                        Commons.filter_user.filter_ownapartment = 1;
                    }
                    else if (radioOwnApartNo.isChecked()){
                        Commons.filter_user.filter_ownapartment = 0;
                    }
                }
            });


            //must relocate
            switch (Commons.filter_user.filter_relocatable){

                case 0:
                    radioMustReNo.setChecked(true);
                    break;
                case 1:
                    radioMustReYes.setChecked(true);
                    break;
                default:
                    radioMustReNo.setChecked(true);
                    break;
            }

            radioMustRelocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioMustReYes.isChecked()) {
                        Commons.filter_user.filter_relocatable = 1;
                    }
                    else if (radioMustReNo.isChecked()){
                        Commons.filter_user.filter_relocatable = 0;
                    }
                }
            });


        } else  lyt_apart_relocate.setVisibility(View.GONE);


        Button btn_search = (Button)dialog.findViewById(R.id.btn_filter);
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Commons.all_users.clear();
                searchUsers(searchString);
                isFilter = true;

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void chooseCaste(){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_choose_caste);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView txv_alertTitle = (TextView)dialog.findViewById(R.id.txv_alertTitle);
        txv_alertTitle.setText(getString(R.string.choose_castes));

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        lyt_content.removeAllViews();
        Button button = (Button)dialog.findViewById(R.id.btn_add);
        strCaste = CasteModel.allCastes;

        for(int i = 0 ; i<strCaste.size() ; i++){

            final TextView textView = new TextView(activity);
            textView.setId(i);
            textView.setText(strCaste.get(i).caste_string());
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            if(Commons.filter_user.casteIsInFilter(strCaste.get(i).caste_id)) {
                textView.setBackgroundResource(R.drawable.item_select_fillrect);
                textView.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                textView.setTextColor(getResources().getColor(R.color.black));
            }

            textView.setId(i);
            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String filter = Commons.filter_user.filter_caste;
                    CasteModel caste = strCaste.get(view.getId());
                    if(Commons.filter_user.casteIsInFilter(caste.caste_id)) {
                        filter = "";
                        for (String casteString : Commons.filter_user.filter_caste.split(",")) {
                            if (Integer.parseInt(casteString) == caste.caste_id) {
                            }
                            else {
                                if (filter.length() > 0) {
                                    filter += "," + casteString;
                                }
                                else {
                                    filter = casteString;
                                }
                            }
                        }
                        Commons.filter_user.filter_caste = filter;
                    }
                    else {
                        if (filter.length() > 0) {
                            Commons.filter_user.filter_caste = filter + ("," + String.valueOf(caste.caste_id));
                        }
                        else {
                            Commons.filter_user.filter_caste = String.valueOf(caste.caste_id);
                        }
                    }
                    if(Commons.filter_user.casteIsInFilter(caste.caste_id)) {
                        textView.setBackgroundResource(R.drawable.item_select_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    }
                    else {
                        textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }

                }
            });
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txvCaste.setText(Commons.filter_user.filter_casteString());
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void chooseCountries(){

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_choose_caste);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView txv_alertTitle = (TextView)dialog.findViewById(R.id.txv_alertTitle);
        txv_alertTitle.setText(getString(R.string.choose_countries));

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        lyt_content.removeAllViews();
        Button button = (Button)dialog.findViewById(R.id.btn_add);
        strCountry = CountryModel.allCountry;

        for(int i = 0 ; i<strCountry.size() ; i++){

            final TextView textView = new TextView(activity);
            textView.setId(i);
            textView.setText(strCountry.get(i).country_string());
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            if(Commons.filter_user.countryIsInFilter(strCountry.get(i).code)) {
                textView.setBackgroundResource(R.drawable.item_select_fillrect);
                textView.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                textView.setTextColor(getResources().getColor(R.color.black));
            }

            textView.setId(i);
            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String filter = Commons.filter_user.filter_country;
                    CountryModel country = strCountry.get(view.getId());
                    if(Commons.filter_user.countryIsInFilter(country.code)) {
                        filter = "";
                        for (String countryString : Commons.filter_user.filter_country.split(",")) {
                            if (countryString.equals(country.code)) {
                            }
                            else {
                                if (filter.length() > 0) {
                                    filter += "," + countryString;
                                }
                                else {
                                    filter = countryString;
                                }
                            }
                        }
                        Commons.filter_user.filter_country = filter;
                    }
                    else {
                        if (filter.length() > 0) {
                            Commons.filter_user.filter_country = filter + ("," + country.code);
                        }
                        else {
                            Commons.filter_user.filter_country = country.code;
                        }
                    }
                    if(Commons.filter_user.countryIsInFilter(country.code)) {
                        textView.setBackgroundResource(R.drawable.item_select_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    }
                    else {
                        textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }

                }
            });
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txvCountry.setText(Commons.filter_user.filter_countryString());
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.imv_mark:
                advancedFilter();
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity)context;

    }
}
