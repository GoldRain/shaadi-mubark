package itwhiz4u.shaadimubark.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.adapter.NotificationListViewAdapter;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.Message;
import itwhiz4u.shaadimubark.model.NotificationModel;
import me.leolin.shortcutbadger.ShortcutBadger;

public class NotificationFragment extends Fragment implements View.OnClickListener{

    MainActivity activity;
    View view;
    ImageView imv_deleteAll;

    ArrayList<NotificationModel> _notis = new ArrayList<>();

    @BindView(R.id.lst_notification) ListView lst_notification;
    NotificationListViewAdapter adapter;

    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        _notis.clear();

        ShortcutBadger.removeCount(activity);

        imv_deleteAll = (ImageView)activity.findViewById(R.id.imv_mark);
        imv_deleteAll.setOnClickListener(this);

        adapter = new  NotificationListViewAdapter(activity, Commons.myNotifications);
        lst_notification.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    private void setNotificationRead(){

        String url = ReqConst.SERVER_URL;
        final StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject respond = new JSONObject(response);
                    String result_msg = respond.getString(ReqConst.RESULT);
                    if (result_msg.equals(ReqConst.RES_SUCCESS)){

                        Commons.notiCount = 0;

                        ArrayList<NotificationModel> readNotification  =  new ArrayList<>();
                        for (NotificationModel noti : Commons.myNotifications){

                            noti.notification_read = 1;
                            readNotification.add(noti);
                        }

                        Commons.myNotifications.clear();
                        Commons.myNotifications.addAll(readNotification);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_NOTIFICATIONS);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_SET_NOTIFICATION_READ);
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void clearAll(){

        if (Commons.myNotifications.size() > 0) {

            activity.showKProgress();
            String url = ReqConst.SERVER_URL;
            final StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    activity.closeKProgress();
                    try {
                        JSONObject respond = new JSONObject(response);
                        String result_msg = respond.getString(ReqConst.RESULT);
                        if (result_msg.equals(ReqConst.RES_SUCCESS)) {

                            Commons.notiCount = 0;
                            Commons.myNotifications.clear();
                            imv_deleteAll.setVisibility(View.GONE);
                            loadLayout();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<>();

                    try {
                        params.put(ReqConst.MODULE, ReqConst.MODULE_NOTIFICATIONS);
                        params.put(ReqConst.FUNCTION, ReqConst.FUNC_CLEAR_ALL);
                        params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));

                        params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                        params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                    } catch (Exception e) {
                    }
                    return params;
                }
            };

            request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.imv_mark:
                clearAll();
                break;
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        for (NotificationModel noti : Commons.myNotifications){

            if (noti.notification_read == 0){
                setNotificationRead();
                break;
            }

        }

    }

}
