package itwhiz4u.shaadimubark.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.adapter.ChatListViewAdapter;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.ChatModelComprator;
import itwhiz4u.shaadimubark.model.UserModel;


public class ChatFragment extends Fragment {

    MainActivity activity;
    View view;

    @BindView(R.id.lst_chat) ListView lst_chat;
    ChatListViewAdapter adapter;
    ArrayList<ChatModel> chatUsers = new ArrayList<>();

    public ChatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        ArrayList<ChatModel> chats = new ArrayList<>();
        for (ChatModel chat : Commons.chatRooms.values()){
            chats.add(chat);
        }
        Collections.sort(chats, new ChatModelComprator());
        Log.d("chat count", String.valueOf(chats.size()));
        chatUsers = chats;

        adapter = new ChatListViewAdapter(activity, chatUsers);
        lst_chat.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

}
