package itwhiz4u.shaadimubark.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.adapter.UserGridViewAdapter;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.model.UserModel;

public class FavoriteFragment extends Fragment {

    MainActivity activity;
    View view;

    UserGridViewAdapter _adapter;
    @BindView(R.id.gdv_favorite) GridView gdv_favorite;

    ArrayList<UserModel> _favoriteUsers = new ArrayList<>();

    public FavoriteFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_favorite, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        _adapter = new UserGridViewAdapter(activity);
        gdv_favorite.setAdapter(_adapter);

        _adapter.setDatas(Commons.favorite_users);
        _adapter.notifyDataSetChanged();

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (MainActivity)context;
    }

    @Override
    public void onResume() {
        super.onResume();

        _adapter.setDatas(Commons.favorite_users);
        _adapter.notifyDataSetChanged();
    }
}
