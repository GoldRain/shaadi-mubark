package itwhiz4u.shaadimubark.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.util.ArrayList;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.utils.TouchImageView;


public class ImagePreviewAdapter extends PagerAdapter {

    Context _context;
    LayoutInflater _layoutInflater;
    ArrayList<String> _imageUrls = new ArrayList<>();

    public ImagePreviewAdapter(Context context){

        _context = context ;
        _layoutInflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setDatas(ArrayList<String> datas){

        _imageUrls = datas;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return _imageUrls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = _layoutInflater.inflate(R.layout.item_image_preview, container, false);

        final TouchImageView imageView = (TouchImageView) itemView.findViewById(R.id.imv_preview);
        Glide.with(_context).load(_imageUrls.get(position)).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                imageView.setImageBitmap(resource);
            }
        });

        container.addView(itemView);
        return itemView ;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
