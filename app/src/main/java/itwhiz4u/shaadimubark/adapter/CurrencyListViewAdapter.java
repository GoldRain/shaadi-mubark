package itwhiz4u.shaadimubark.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.ArrayList;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.fragment.SettingsFragment;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.PriceModel;

public class CurrencyListViewAdapter extends BaseAdapter {

    SettingsFragment _activity;
    ArrayList<PriceModel> _currencies =  new ArrayList<>();

    public CurrencyListViewAdapter(SettingsFragment fragment){
        _activity = fragment;
        _currencies = PriceModel.allPrices;
    }

    @Override
    public int getCount() {
        return _currencies.size();
    }

    @Override
    public Object getItem(int i) {
        return _currencies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {

        final CurrencyHolder holder;
        if (view == null){
            holder = new CurrencyHolder();

            LayoutInflater inflater = (LayoutInflater) ShaadiMubarkApplication.getInstance().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_currency, viewGroup, false);

            holder.txvAmount = (TextView) view.findViewById(R.id.txv_amount);
            holder.txvCurrency = (TextView)view.findViewById(R.id.txv_currency);
            holder.checkBox = (CheckBox)view.findViewById(R.id.chk_box);
            /*holder.btn_confirm = (Button)view.findViewById(R.id.btn_confirm);*/

            view.setTag(holder);

        } else {

            holder = (CurrencyHolder)view.getTag();
        }

        final PriceModel priceModel = _currencies.get(position);

        holder.txvAmount.setText(String.valueOf(priceModel.price));
        holder.txvCurrency.setText(priceModel.currency);
        if(priceModel.getChecked())
            holder.checkBox.setChecked(true);
        else
            holder.checkBox.setChecked(false);

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //_activity.updateadapter(position);
            }
        });

        return view;
    }

    public class CurrencyHolder {

        TextView txvAmount, txvCurrency;
        CheckBox checkBox;
    }
}
