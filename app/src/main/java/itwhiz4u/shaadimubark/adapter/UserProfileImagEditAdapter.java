package itwhiz4u.shaadimubark.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.activity.ImagePreviewActivity;
import itwhiz4u.shaadimubark.activity.UserProfileActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;

import static itwhiz4u.shaadimubark.base.BaseActivity.min;

/**
 * Created by ITWhiz4U on 2/11/2018.
 */

public class UserProfileImagEditAdapter extends RecyclerView.Adapter<UserProfileImagEditAdapter.ImageHolder> {

    private ArrayList<String> _imageUrls = new ArrayList<>();
    int _id = 0;
    private UserProfileActivity _context;

    int add = 1;

    public UserProfileImagEditAdapter(UserProfileActivity context) {

        this._context = context;
    }

    @Override
    public UserProfileImagEditAdapter.ImageHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_takephoto_image_edit, viewGroup, false);

        ImageHolder viewHolder = new ImageHolder(view);

       /* viewHolder.imvPhoto.requestLayout();

        viewHolder.imvPhoto.getLayoutParams().width = 270;
        viewHolder.imvPhoto.getLayoutParams().height = *//*((Constants.WIDTH - 240)/3)*//*270;
        viewHolder.imvPhoto.setScaleType(ImageView.ScaleType.FIT_XY);

        _context.showToast(String.valueOf((Constants.WIDTH/3 - 60 - 20)/3));*/
        return viewHolder;
    }

    public void setDatas(ArrayList<String> images) {

        _imageUrls = images;
        notifyDataSetChanged();
    }

    public void setData(ArrayList<String> images, int idx){

        _imageUrls = images;
        _id = idx;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final ImageHolder viewHolder, int i) {

        //String imageUrl = _imageUrls.get(i);

        try {

            if (_imageUrls.size() > 0){
                if (i < _imageUrls.size()) {
                    String imageUrl = _imageUrls.get(i);
                    if(imageUrl.substring(0, 3).equals("htt")){

                        Picasso.with(_context).load(imageUrl).placeholder(R.drawable.img_user).into(viewHolder.imvPhoto);

                    }else {

                        Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
                        viewHolder.imvPhoto.setImageBitmap(bitmap);
                    }
                }
                else if (Commons.g_user.user_id == _id){
                    viewHolder.imvPhoto.setImageResource(R.drawable.ic_add_);
                }

            } else if (Commons.g_user.user_id == _id){
                viewHolder.imvPhoto.setImageResource(R.drawable.ic_add_);
            }

        } catch (Exception e){}

        //Bitmap bitmap = BitmapFactory.decodeFile(imageUrl);
//        Bitmap scaledBitmap = BitmapUtils.getSizeLimitedBitmap(bitmap);
//        bitmap.recycle();
        //viewHolder.imvPhoto.setImageBitmap(bitmap);

        final int position = i;

        viewHolder.imvDelete.setVisibility(View.GONE);

        viewHolder.imvPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                viewHolder.imvDelete.setVisibility(View.GONE);

                if (position >= _imageUrls.size()) _context.multiTakePhoto();

                else {

                    Intent intent = new Intent(_context, ImagePreviewActivity.class);
                    intent.putExtra(Constants.KEY_IMAGEPATH, _imageUrls);
                    intent.putExtra(Constants.KEY_POSITION, position);
                    _context.startActivity(intent);
                }
            }
        });

        viewHolder.imvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //_context.removeImage(_imageUrls.get(position));
                _context.deleteImage(_imageUrls.get(position));
                notifyDataSetChanged();

            }
        });

        if (Commons.g_user.user_id == _id){

            viewHolder.imvPhoto.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (position < _imageUrls.size()){

                        viewHolder.imvDelete.setVisibility(View.VISIBLE);
                        viewHolder.imvDelete.setColorFilter(_context.getResources().getColor(R.color.white));
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (Commons.g_user.user_id == _id){
            return min(6 , _imageUrls.size() + 1);
        } else return _imageUrls.size();
    }

    public class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imvPhoto;
        ImageView imvDelete;

        public ImageHolder(View view) {

            super(view);
            imvPhoto = (RoundedImageView) view.findViewById(R.id.imv_image);
            imvDelete = (ImageView) view.findViewById(R.id.imv_delete);
        }
    }
}
