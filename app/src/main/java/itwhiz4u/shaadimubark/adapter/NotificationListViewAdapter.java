package itwhiz4u.shaadimubark.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.NotificationModel;
import itwhiz4u.shaadimubark.utils.DateUtils;

/**
 * Created by ITWhiz4U on 2/6/2018.
 */

public class NotificationListViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<NotificationModel> _allNoti = new ArrayList<>();

    public NotificationListViewAdapter(MainActivity activity, ArrayList<NotificationModel> notis){

        this._activity = activity;
        this._allNoti = notis;
    }


    @Override
    public int getCount() {
        return _allNoti.size();
    }

    @Override
    public Object getItem(int position) {
        return _allNoti.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        NotificationHolder holder;

        if (convertView == null){

            holder = new NotificationHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_notification_list, parent, false);

            holder.imv_photo = (CircularImageView)convertView.findViewById(R.id.imv_photo_noti);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_time = (TextView)convertView.findViewById(R.id.txv_time);
            holder.txv_content = (TextView)convertView.findViewById(R.id.txv_content);
            holder.imv_badge = (ImageView)convertView.findViewById(R.id.imv_badge);
            holder.lyt_delete = (LinearLayout)convertView.findViewById(R.id.lyt_delete);
            holder.ryt_added = (RelativeLayout)convertView.findViewById(R.id.rlt_added);
            holder.txv_accept = (TextView)convertView.findViewById(R.id.txv_accept);
            holder.txv_block = (TextView)convertView.findViewById(R.id.txv_block);

            convertView.setTag(holder);

        } else {
            holder = (NotificationHolder) convertView.getTag();
        }

        final NotificationModel noti = _allNoti.get(position);

        if (noti.notification_sender.user_photo.length() > 0)
            Picasso.with(_activity).load(noti.notification_sender.user_photo).placeholder(R.drawable.img_user).into(holder.imv_photo);

        holder.txv_name.setText(noti.notification_sender.getName());
        holder.txv_time.setText(String.valueOf(DateUtils.getTimeString(noti.notification_timestamp)));
        if (noti.notification_message.length() > 0)
            holder.txv_content.setText(noti.notification_message);

        if (noti.notification_read == 0){
            holder.imv_badge.setVisibility(View.VISIBLE);
            }
        else if (noti.notification_read == 1)
            holder.imv_badge.setVisibility(View.GONE);

        if (noti.notification_type == NotificationModel.NOTIFICATION_TYPE_CHAT_REQUESTED){

            holder.ryt_added.setVisibility(View.VISIBLE);
        }
        else if (noti.notification_type == NotificationModel.NOTIFICATION_TYPE_CHAT_BLOCKED){


        }
        else if (noti.notification_type == NotificationModel.NOTIFICATION_TYPE_CHAT_ACCEPTED) {

            holder.ryt_added.setVisibility(View.VISIBLE);
            holder.txv_accept.setVisibility(View.VISIBLE);
            holder.txv_accept.setText(_activity.getString(R.string.start_chat));
            holder.txv_block.setVisibility(View.GONE);
        }
        else if (noti.notification_type == NotificationModel.NOTIFICATION_TYPE_CHAT_MESSAGE){
            holder.ryt_added.setVisibility(View.GONE);
        }

        holder.lyt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteNotification(noti);
            }
        });

        holder.txv_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (noti.notification_type == NotificationModel.NOTIFICATION_TYPE_CHAT_ACCEPTED){
                    if (Commons.chatRooms.containsKey(noti.notification_sender.user_id)){
                        _activity.gotoChatRoom(noti.notification_sender.user_id);
                        Commons.myNotifications.remove(noti);
                        notifyDataSetChanged();
                    }

                } else{

                    acceptRequest(noti);
                    Commons.myNotifications.remove(noti);
                    notifyDataSetChanged();
                }

            }
        });

        holder.txv_block.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                blockRequest(noti);
            }
        });
        return convertView;
    }


    private void deleteNotification(final NotificationModel notification){

        _activity.showKProgress();
        String url = ReqConst.SERVER_URL;
        final StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                _activity.closeKProgress();

                try {
                    JSONObject respond = new JSONObject(response);
                    String result_msg = respond.getString(ReqConst.RESULT);
                    if (result_msg.equals(ReqConst.RES_SUCCESS)){
                        Commons.myNotifications.remove(notification);
                        notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_NOTIFICATIONS);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_DELET_NOTIFICATION);
                    params.put(ReqConst.SESSION, EasyPreference.with(_activity).getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                    params.put(ReqConst.PARAM_NOTIFICATION_ID, String.valueOf(notification.notification_id));

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void acceptRequest(final NotificationModel accept){

        _activity.showKProgress();
        String url = ReqConst.SERVER_URL;
        final StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                _activity.closeKProgress();
                /*parseDeleteNotification(response);*/
                try {

                    JSONObject respond = new JSONObject(response);
                    String result_msg = respond.getString(ReqConst.RESULT);
                    if (result_msg.equals(ReqConst.RES_SUCCESS)){
                        //_activity.gotoChatRoom(accept.notification_sender.user_id);

                        int index = 0;
                        for (NotificationModel noti: Commons.myNotifications) {
                            if (noti.notification_id == accept.notification_id) {

                                int chatId = respond.optInt("chat_id", 0);
                                ChatModel chat = new  ChatModel();
                                chat.chat_id = chatId;
                                chat.chat_user = noti.notification_sender;
                                if (chat.chat_user.user_id > Commons.g_user.user_id) {
                                    chat.chat_room = String.valueOf(Commons.g_user.user_id ) + "_" + String.valueOf(chat.chat_user.user_id);
                                }
                                else {
                                    chat.chat_room = String.valueOf(chat.chat_user.user_id) + "_" + String.valueOf(Commons.g_user.user_id );
                                }

                                chat.chat_createdat =  (int) (new Date()).getTime()/1000;
                                chat.setChatListener();
                                Commons.chatRooms.put(chat.chat_user.user_id, chat);
                                Commons.myNotifications.remove(index);

                                _activity.gotoChatRoom(chat.chat_user.user_id);
                                return;
                            }
                            index += 1;
                        }

                        Commons.myNotifications.remove(accept);
                        notifyDataSetChanged();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_CHATS);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_ACCEPT_CHART_REQUEST);
                    params.put(ReqConst.SESSION, EasyPreference.with(_activity).getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_REQUEST_ID, String.valueOf(accept.notification_item));
                    params.put(ReqConst.PARAM_NOTIFICATION_ID, String.valueOf(accept.notification_id));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void blockRequest(final NotificationModel block){

        _activity.showKProgress();
        String url = ReqConst.SERVER_URL;
        final StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                _activity.closeKProgress();
                try {
                    JSONObject respond = new JSONObject(response);
                    String result_msg = respond.getString(ReqConst.RESULT);
                    if (result_msg.equals(ReqConst.RES_SUCCESS)){

                        Commons.myNotifications.remove(block);
                        notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_CHATS);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_BLOCK_CHART_REQUEST);
                    params.put(ReqConst.SESSION, EasyPreference.with(_activity).getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_REQUEST_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_NOTIFICATION_ID, String.valueOf(block.notification_id));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    static class NotificationHolder{

        CircularImageView imv_photo;
        TextView txv_name;
        TextView txv_time;
        TextView txv_content;
        ImageView imv_badge;
        LinearLayout lyt_delete;
        RelativeLayout ryt_added;
        TextView txv_accept,
                txv_block;
    }

}
