package itwhiz4u.shaadimubark.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.activity.ChatActivity;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.activity.UserProfileActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.Message;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.DateUtils;


/**
 * Created by ITWhiz4U on 3/6/2018.
 */

public class ChatListViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<ChatModel> _chatUsers =  new ArrayList<>();

    public ChatListViewAdapter(MainActivity activity, ArrayList<ChatModel> chatUsers){

        this._activity = activity;
        this._chatUsers = chatUsers;
    }

    @Override
    public int getCount() {
        return _chatUsers.size();

    }

    @Override
    public Object getItem(int position) {
        return _chatUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ChatListHolder holder;
        if (convertView == null){

            holder = new ChatListHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_chat_list, parent, false);

            holder.imv_photo = (CircularImageView)convertView.findViewById(R.id.imv_photo_chat);
            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_last_msg = (TextView)convertView.findViewById(R.id.txv_last_msg);
            holder.txv_time = (TextView)convertView.findViewById(R.id.txv_time);

            convertView.setTag(holder);
        } else {
            holder = (ChatListHolder)convertView.getTag();
        }

        final ChatModel chat = _chatUsers.get(position);

        if (chat.chat_user.user_photo.length() > 0)
            Picasso.with(_activity).load(chat.chat_user.user_photo).placeholder(R.drawable.img_user).into(holder.imv_photo);
        holder.txv_name.setText(chat.chat_user.getName());

        Message lastMessage = chat.getLastMessage();

        if (lastMessage != null){
            if (lastMessage.getMessagetype().equals("text")){

                holder.txv_last_msg.setText(chat.getLastMessage().getText());

            } else if (lastMessage.getMessagetype().equals("photo")){

               if (lastMessage.getUser().getId().equals(String.valueOf(Commons.g_user.user_id))){

                   holder.txv_last_msg.setText(_activity.getString(R.string.you_shared_photo));

               } else {
                   holder.txv_last_msg.setText(lastMessage.getUser().getName()  + _activity.getString(R.string.shared_photo));
               }
            }
        }


        holder.txv_time.setText(String.valueOf(DateUtils.getTimeString(chat.getLastMessageTime())));

       convertView.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               if (Commons.g_user.user_paid > DateUtils.getTimeStamp()){

                   Constants.PAGE_TO = Constants.CHATTING_PAGE;
                   Intent intent = new Intent(_activity, ChatActivity.class);
                   intent.putExtra("userId", chat.chat_user.user_id);
                   _activity.startActivity(intent);
                   _activity.finish();
               }
               else {

                   _activity.showSweetAlert(3,_activity.getString(R.string.premium), _activity.getString(R.string.membership_req));
                   //_activity.gotoSettingsFragment();
               }
           }
       });

       holder.imv_photo.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Constants.PAGE_TO = Constants.CHATTING_PAGE;
               Intent intent = new Intent(_activity, UserProfileActivity.class);
               intent.putExtra(Constants.KEY_USER, chat.chat_user);
               _activity.startActivity(intent);
               _activity.finish();

           }
       });

        return convertView;
    }

    public class ChatListHolder {

        CircularImageView imv_photo;
        TextView txv_name, txv_last_msg, txv_time;

    }

}
