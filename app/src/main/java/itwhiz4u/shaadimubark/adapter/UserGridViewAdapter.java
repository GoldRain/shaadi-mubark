package itwhiz4u.shaadimubark.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.activity.UserProfileActivity;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.MyJSONObject;

/**
 * Created by ITWhiz4U on 2/5/2018.
 */

public class UserGridViewAdapter extends BaseAdapter {

    MainActivity _activity;
    ArrayList<UserModel> _user = new ArrayList<>();
    ArrayList<UserModel> _allUsers = new ArrayList<>();
    public ArrayList<String> _contents = new ArrayList<>();

    public UserGridViewAdapter(MainActivity activity){
        super();
        this._activity = activity;
    }
    public UserGridViewAdapter(MainActivity activity, ArrayList<UserModel> users){

        this._activity = activity;
        _allUsers = users;

    }

    @Override
    public int getCount() {
        return _allUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return _allUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UserHolder holder;
        if (convertView ==  null){
            holder = new UserHolder();
            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_user_gridview, parent, false);

            holder.cardFavorite = (CardView)convertView.findViewById(R.id.cardFavorite);
            holder.imv_favorite = (ImageView)convertView.findViewById(R.id.imv_favorite);
            holder.imv_photo = (ImageView)convertView.findViewById(R.id.imv_photo);

            holder.txv_name = (TextView)convertView.findViewById(R.id.txv_name);
            holder.txv_year = (TextView)convertView.findViewById(R.id.txv_year);
            holder.txv_height = (TextView)convertView.findViewById(R.id.txv_height);

            convertView.setTag(holder);
        } else {

            holder = (UserHolder)convertView.getTag();
        }

        holder.cardFavorite.setPreventCornerOverlap(false);
        final UserModel user = _allUsers.get(position);

        if (user.user_photo.length() > 0)
            Picasso.with(_activity).load(user.user_photo).placeholder(R.drawable.img_user).into(holder.imv_photo);

        if (user.isFavorite == 1){
            holder.imv_favorite.setVisibility(View.VISIBLE);
        } else holder.imv_favorite.setVisibility(View.GONE);

        holder.txv_name.setText(user.getName());
        holder.txv_height.setText(String.valueOf(user.user_height));
        holder.txv_year.setText(String.valueOf(user.getAge()));


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(_activity, UserProfileActivity.class);
                intent.putExtra(Constants.KEY_USER, user);
                _activity.startActivity(intent);
                _activity.finish();
            }
        });

        return convertView;
    }

    public void setDatas(ArrayList<UserModel> data) {
        _allUsers = data;
        notifyDataSetChanged();
    }


    public class UserHolder{

        CardView cardFavorite;
        ImageView imv_photo;
        public ImageView imv_favorite;
        TextView txv_name, txv_year, txv_height;

    }
}
