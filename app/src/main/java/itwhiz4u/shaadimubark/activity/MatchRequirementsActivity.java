package itwhiz4u.shaadimubark.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.nex3z.flowlayout.FlowLayout;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.adapter.SpinnerAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.PriceModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;

public class MatchRequirementsActivity extends CommonActivity {

    @BindView(R.id.range_year) CrystalRangeSeekbar range_year;
    @BindView(R.id.range_height) CrystalRangeSeekbar range_height;

    @BindView(R.id.txv_min_year) TextView txv_min_year;
    @BindView(R.id.txv_max_year) TextView txv_max_year;
    @BindView(R.id.txv_min_height) TextView txv_min_height;
    @BindView(R.id.txv_max_height) TextView txv_max_height;

    @BindView(R.id.spn_religion) Spinner spn_religion;
    @BindView(R.id.spn_min_education) Spinner spn_min_education;
    SpinnerAdapter _adapter;

    List<String> lstReligion = new ArrayList<>();
    List<String> lstMinEdu = new ArrayList<>();

    @BindView(R.id.edt_professions) EditText edt_professions;
    ArrayList<CasteModel> strCaste = new ArrayList<>();
    ArrayList<UserModel.MaritalStatus> strMarital = new ArrayList<>();
    ArrayList<CountryModel> strCountry = new ArrayList<>();
    int value = 0;

    //radio group
    @BindView(R.id.radioChildren) RadioGroup radioChildren;
    @BindView(R.id.radioChildrenYes) RadioButton radioChildrenYes;
    @BindView(R.id.radioChildrenNo) RadioButton radioChildrenNo;

    @BindView(R.id.radioGroupOwnApart) RadioGroup radioGroupOwnApart;
    @BindView(R.id.radioOwnApartYes) RadioButton radioOwnApartYes;
    @BindView(R.id.radioOwnApartNo) RadioButton radioOwnApartNo;

    @BindView(R.id.radioMustRelocate) RadioGroup radioMustRelocate;
    @BindView(R.id.radioMustReYes) RadioButton radioMustReYes;
    @BindView(R.id.radioMustReNo) RadioButton radioMustReNo;
    @BindView(R.id.btn_next_mr) Button btn_next;

    @BindView(R.id.lyt_havechildren) LinearLayout lyt_havechildren;
    @BindView(R.id.lyt_apart_relocate) LinearLayout lyt_apart_relocate;

    ///
    @BindView(R.id.txvMaritalStatus) TextView txvMaritalStatus;
    @BindView(R.id.txvCaste) TextView txvCaste;
    @BindView(R.id.txvCountry) TextView txvCountry;
    ///

    UserModel user;

    //ArrayList<String> arrayCaste = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_requirements);

        ButterKnife.bind(this);

        user = (UserModel)getIntent().getSerializableExtra(Constants.KEY_USER);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_match_req);
        setSupportActionBar(toolbar);

        TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(getString(R.string.match_req));


        loadLayout();
    }


    private void loadLayout() {

        //Age from ~ to

        if (user.filter_ageto != 0 && user.filter_agefrom != 0)
            range_year.setMinStartValue(user.filter_agefrom).setMaxStartValue(user.filter_ageto).apply();

        range_year.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                txv_min_year.setText(String.valueOf(minValue));
                txv_max_year.setText(String.valueOf(maxValue));

                user.filter_agefrom = (int)minValue.intValue();
                user.filter_ageto = (int) maxValue.intValue();
            }
        });


        //Height from ~ to
        if (user.filter_heightfrom != 0 && user.filter_heightto != 0)
            range_height.setMinStartValue(user.filter_heightfrom).setMaxStartValue(user.filter_heightto).apply();

        range_height.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                txv_min_height.setText(String.valueOf(minValue));
                txv_max_height.setText(String.valueOf(maxValue));

                user.filter_heightfrom = (int)minValue.intValue();
                user.filter_heightto = (int)maxValue.intValue();
            }
        });

        //Marital Status


        //Religion
        lstReligion.clear();
        for (UserModel.Religion religions : UserModel.Religion.possibleValues){
            lstReligion.add(religions.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstReligion);
        spn_religion.setAdapter(_adapter);
        //spn_religion.setSelection(_adapter.getCount());
        for (int i = 0; i < UserModel.Religion.possibleValues.length ; i++){
            if (user.filter_religion == UserModel.Religion.possibleValues[i]){
                spn_religion.setSelection(i);
            }
        }

        spn_religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                user.filter_religion = UserModel.Religion.possibleValues[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Minimum Education Level
        lstMinEdu.clear();
        for (UserModel.EducationLevel minEducation : UserModel.EducationLevel.possibleValues){
            lstMinEdu.add(minEducation.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstMinEdu);
        spn_min_education.setAdapter(_adapter);

        for (int i = 0; i < UserModel.EducationLevel.possibleValues.length; i++){
            if (user.filter_minimumedu == UserModel.EducationLevel.possibleValues[i]){
                spn_min_education.setSelection(i);
            }
        }
        spn_min_education.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 5){
                    user.filter_minimumedu = UserModel.EducationLevel.possibleValues[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (user.filter_professions.equals("null") || user.filter_professions.length() == 0)
            edt_professions.setText("");
        else
            edt_professions.setText(user.filter_professions);


        //in case only women
        if (user.user_gender == UserModel.Gender.female){

            lyt_apart_relocate.setVisibility(View.VISIBLE);

            //own apartment
            switch (user.filter_ownapartment){

                case 0:
                    radioOwnApartNo.setChecked(true);
                    break;
                case 1:
                    radioOwnApartYes.setChecked(true);
                    break;
                default:
                    radioOwnApartNo.setChecked(true);
                    break;
            }

            radioGroupOwnApart.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioOwnApartYes.isChecked()) {
                        user.filter_ownapartment = 1;
                    }
                    else if (radioOwnApartNo.isChecked()){
                        user.filter_ownapartment = 0;
                    }
                }
            });


            //must relocate
            switch (user.filter_relocatable){

                case 0:
                    radioMustReNo.setChecked(true);
                    break;
                case 1:
                    radioMustReYes.setChecked(true);
                    break;
                default:
                    radioMustReNo.setChecked(true);
                    break;
            }

            radioMustRelocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioMustReYes.isChecked()) {
                        user.filter_relocatable = 1;
                    }
                    else if (radioMustReNo.isChecked()){
                        user.filter_relocatable = 0;
                    }
                }
            });

        } else  lyt_apart_relocate.setVisibility(View.GONE);


        if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5) btn_next.setText(getString(R.string.next));
        else if (Constants.SIGNUP_STATUS == 10) btn_next.setText(getString(R.string.update));


        txvCaste.setText(user.filter_casteString());
        txvCaste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCaste();
            }
        });

        txvCountry.setText(user.filter_countryString());
        txvCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCountries();
            }
        });

        //Marital status
        if (user.filter_maritalstatus > 1){
            lyt_havechildren.setVisibility(View.VISIBLE);

            switch (user.filter_havechildren){

                case 0:
                    radioChildrenNo.setChecked(true);
                    break;
                case 1:
                    radioChildrenYes.setChecked(true);
                    break;
                default:
                    radioChildrenNo.setChecked(true);
                    break;
            }

            radioChildren.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioChildrenYes.isChecked()) {
                        user.filter_havechildren = 1;
                    }
                    else if (radioChildrenNo.isChecked()){
                        user.filter_havechildren = 0;
                    }
                }
            });

        } else
            lyt_havechildren.setVisibility(View.GONE);

        txvMaritalStatus.setText(user.filter_maritalstatusString());
        txvMaritalStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMaritalStatus();
            }
        });
    }

    private void chooseMaritalStatus(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_choose_caste);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        Button button = (Button)dialog.findViewById(R.id.btn_add);

        TextView txv_alertTitle = (TextView)dialog.findViewById(R.id.txv_alertTitle);
        txv_alertTitle.setText(getString(R.string.choose_marital_status));

         strMarital = new ArrayList<>(Arrays.asList(UserModel.MaritalStatus.possibleValues));

        strMarital.remove(0);
        for(int i = 0 ; i<strMarital.size() ; i++){

            final TextView textView = new TextView(this);
            textView.setId(i);
            textView.setText(strMarital.get(i).getString());
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            if((user.filter_maritalstatus & strMarital.get(i).rawValue) > 0) {
                textView.setBackgroundResource(R.drawable.item_select_fillrect);
                textView.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                textView.setTextColor(getResources().getColor(R.color.black));
            }

            textView.setId(i);
            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String filter = user.filter_caste;
                    UserModel.MaritalStatus status = strMarital.get(view.getId());
                    if((status.rawValue & user.filter_maritalstatus) == 0) {
                        user.filter_maritalstatus = status.rawValue | user.filter_maritalstatus;
                        textView.setBackgroundResource(R.drawable.item_select_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    }
                    else {
                        user.filter_maritalstatus = user.filter_maritalstatus - status.rawValue;
                        textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }

                }
            });
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txvMaritalStatus.setText(user.filter_maritalstatusString());

                if (user.filter_maritalstatus > 1){
                    lyt_havechildren.setVisibility(View.VISIBLE);

                    switch (user.filter_havechildren){

                        case 0:
                            radioChildrenNo.setChecked(true);
                            break;
                        case 1:
                            radioChildrenYes.setChecked(true);
                            break;
                        default:
                            radioChildrenNo.setChecked(true);
                            break;
                    }

                } else
                    lyt_havechildren.setVisibility(View.GONE);

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void chooseCaste(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_choose_caste);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        Button button = (Button)dialog.findViewById(R.id.btn_add);
        strCaste = CasteModel.allCastes;

        for(int i = 0 ; i<strCaste.size() ; i++){

            final TextView textView = new TextView(this);
            textView.setId(i);
            textView.setText(strCaste.get(i).caste_string());
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            if(user.casteIsInFilter(strCaste.get(i).caste_id)) {
                textView.setBackgroundResource(R.drawable.item_select_fillrect);
                textView.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                textView.setTextColor(getResources().getColor(R.color.black));
            }

            textView.setId(i);
            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String filter = user.filter_caste;
                    CasteModel caste = strCaste.get(view.getId());
                    if(user.casteIsInFilter(caste.caste_id)) {
                        filter = "";
                        for (String casteString : user.filter_caste.split(",")) {
                            if (Integer.parseInt(casteString) == caste.caste_id) {
                            }
                            else {
                                if (filter.length() > 0) {
                                    filter += "," + casteString;
                                }
                                else {
                                    filter = casteString;
                                }
                            }
                        }
                        user.filter_caste = filter;
                    }
                    else {
                        if (filter.length() > 0) {
                            user.filter_caste = filter + ("," + String.valueOf(caste.caste_id));
                        }
                        else {
                            user.filter_caste = String.valueOf(caste.caste_id);
                        }
                    }
                    if(user.casteIsInFilter(caste.caste_id)) {
                        textView.setBackgroundResource(R.drawable.item_select_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    }
                    else {
                        textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }

                }
            });
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txvCaste.setText(user.filter_casteString());
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void chooseCountries(){

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_choose_caste);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.transparent)));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        TextView txv_alertTitle = (TextView)dialog.findViewById(R.id.txv_alertTitle);
        txv_alertTitle.setText(getString(R.string.choose_countries));

        FlowLayout lyt_content = (FlowLayout)dialog.findViewById(R.id.lyt_content);
        lyt_content.removeAllViews();
        Button button = (Button)dialog.findViewById(R.id.btn_add);
        strCountry = CountryModel.allCountry;

        for(int i = 0 ; i<strCountry.size() ; i++){

            final TextView textView = new TextView(this);
            textView.setId(i);
            textView.setText(strCountry.get(i).country_string());
            textView.setPadding(10,20,10,20);
            textView.setLeft(10);
            textView.setRight(10);

            if(user.countryIsInFilter(strCountry.get(i).code)) {
                textView.setBackgroundResource(R.drawable.item_select_fillrect);
                textView.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                textView.setTextColor(getResources().getColor(R.color.black));
            }

            textView.setId(i);
            lyt_content.addView(textView);

            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String filter = user.filter_country;
                    CountryModel country = strCountry.get(view.getId());
                    if(user.countryIsInFilter(country.code)) {
                        filter = "";
                        for (String countryString : user.filter_country.split(",")) {
                            if (countryString.equals(country.code)) {
                            }
                            else {
                                if (filter.length() > 0) {
                                    filter += "," + countryString;
                                }
                                else {
                                    filter = countryString;
                                }
                            }
                        }
                        user.filter_country = filter;
                    }
                    else {
                        if (filter.length() > 0) {
                            user.filter_country = filter + ("," + country.code);
                        }
                        else {
                            user.filter_country = country.code;
                        }
                    }
                    if(user.countryIsInFilter(country.code)) {
                        textView.setBackgroundResource(R.drawable.item_select_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.white));
                    }
                    else {
                        textView.setBackgroundResource(R.drawable.item_unselct_fillrect);
                        textView.setTextColor(getResources().getColor(R.color.black));
                    }
                }
            });
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txvCountry.setText(user.filter_countryString());
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @OnClick(R.id.btn_next_mr) void updateMatchReq(){

        showKProgress();

        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdateMatchReq(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(user.user_id));

                    params.put(ReqConst.PARAM_FILTER_AGEFROM, String.valueOf(user.filter_agefrom));
                    params.put(ReqConst.PARAM_FILTER_AGETO, String.valueOf(user.filter_ageto));
                    params.put(ReqConst.PARAM_FILTER_HEIGHTFROM, String.valueOf(user.filter_heightfrom));
                    params.put(ReqConst.PARAM_USER_FILTER_HEIGHTTO, String.valueOf(user.filter_heightto));
                    params.put(ReqConst.PARAM_USER_FILTER_MARITAL, String.valueOf(user.filter_maritalstatus));
                    params.put(ReqConst.PARAM_FILTER_RELIGION, String.valueOf(user.filter_religion.rawValue));
                    params.put(ReqConst.PARAM_FILTER_MINEDUCATION, String.valueOf(user.filter_minimumedu.rawValue));
                    params.put(ReqConst.PARAM_USER_FILTER_PROFESSIONS, String.valueOf(edt_professions.getText().toString()));
                    params.put(ReqConst.PARAM_FILTER_CASTE, String.valueOf(user.filter_caste));
                    params.put(ReqConst.PARAM_FILTER_COUNTRY, String.valueOf(user.filter_country));
                    params.put(ReqConst.PARAM_FILTER_HAVECHILDREN, String.valueOf(user.filter_havechildren));
                    params.put(ReqConst.PARAM_FILTER_OWNAPARTMENT, String.valueOf(user.filter_ownapartment));
                    params.put(ReqConst.PARAM_FILTER_RELOCATABLE, String.valueOf(user.filter_relocatable));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpdateMatchReq(String json){

        Log.d("parseMatch==>", json);
        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                UserModel user;

                showToast(result_msg);

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);

                Commons.g_user = user;

                Commons.all_users = new ArrayList<>();
                Commons.match_users = new ArrayList<>();
                Commons.favorite_users = new ArrayList<>();

                //get all users
                JSONArray jsonAllUsers = response.getJSONArray(ReqConst.RES_USERS);
                for (int i = 0; i < jsonAllUsers.length(); i++){

                    Commons.all_users.add(new UserModel(jsonAllUsers.getJSONObject(i)));
                }

                //get favorite users
                JSONArray jsonFavUsers = response.getJSONArray(ReqConst.RES_FAVORITE_USERS);
                for (int i = 0; i < jsonFavUsers.length(); i++){

                    Commons.favorite_users.add(new UserModel(jsonFavUsers.getJSONObject(i)));
                }

                //get match users
                JSONArray jsonMatchUsers = response.getJSONArray(ReqConst.RES_MATCH_USERS);
                for (int i = 0; i < jsonMatchUsers.length(); i++){

                    Commons.match_users.add(new UserModel(jsonMatchUsers.getJSONObject(i)));
                }

                //get caste
                JSONArray castes = response.getJSONArray(ReqConst.RES_CASTE);
                CasteModel.allCastes = new ArrayList<>();
                for (int i = 0; i < castes.length(); i ++){
                    CasteModel.allCastes.add(new CasteModel(castes.getJSONObject(i)));
                }

              /*  //get prices
                JSONArray prices = response.getJSONArray(ReqConst.RES_PRICE);
                PriceModel.allPrices = new ArrayList<>();
                for (int i = 0; i < prices.length(); i++){
                    PriceModel.allPrices.add(new PriceModel(prices.getJSONObject(i)));
                }*/

                if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5){

                    Intent intent = new Intent(this, MatchActivity.class);
                    Constants.SIGNUP_STATUS = 10;
                    startActivity(intent);
                    finish();

                  /*  Constants.SIGNUP_STATUS = 10;
                   Intent intent = new Intent(this, UserProfileActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();*/

                }

            } else {

                if (result_msg.equals(getString(R.string.session_tampered))){

                    ApiHelper manager = new ApiHelper();
                    manager.logOut(this);
                }

                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void gotoFinish(){

        new SweetAlertDialog(this,SweetAlertDialog.WARNING_TYPE)

                .setContentText(" Thank you for the registration , we will update you when the final version is ready for download")
                .setConfirmText("Okay")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                finish();

                                sweetAlertDialog.cancel();
                                sweetAlertDialog.dismiss();

                            }
                        }, 1000);

                    }
                })
                .setCancelText(getString(R.string.cancel))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                }).show();
    }


    @OnClick(R.id.imv_back) void backAction(){

        if (Constants.SIGNUP_STATUS ==10){
            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
            startActivity(intent);

        } else if (Constants.SIGNUP_STATUS == 0){

            Intent intent = new Intent(this, ProfessionalInfoActivity.class);
            intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
            startActivity(intent);

        }

        finish();
    }

    @Override
    public void onBackPressed() {
        backAction();
    }
}
