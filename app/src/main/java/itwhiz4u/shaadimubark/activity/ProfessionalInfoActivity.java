package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.adapter.SpinnerAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.User;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;

import static itwhiz4u.shaadimubark.model.UserModel.Gender.female;

public class ProfessionalInfoActivity extends CommonActivity {

    @BindView(R.id.spn_education_level) Spinner spn_education_level;
    @BindView(R.id.spn_jobstatus) Spinner spn_jobstatus;
    SpinnerAdapter _adapter;
    List<String> lstEducation = new ArrayList<>();
    List<String> lstJobStatus = new ArrayList<>();

    @BindView(R.id.edt_profession) EditText edt_profession;
    @BindView(R.id.edt_salary) EditText edt_salary;

    @BindView(R.id.radioRelocate) RadioGroup radioRelocate;
    @BindView(R.id.radioReYes) RadioButton radioReYes;
    @BindView(R.id.radioReNo) RadioButton radioReNo;

    @BindView(R.id.radioOwnApart) RadioGroup radioOwnApart;
    @BindView(R.id.radioYesApart) RadioButton radioYesApart;
    @BindView(R.id.radioNoApart) RadioButton radioNoApart;
    @BindView(R.id.btn_next) Button btn_next;

    @BindView(R.id.lyt_ownApart) LinearLayout lyt_ownApart;
    @BindView(R.id.lyt_relocate) LinearLayout lyt_relocate;

    UserModel user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professional_info);

        ButterKnife.bind(this);

        user = (UserModel) getIntent().getSerializableExtra(Constants.KEY_USER);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_professional);
        setSupportActionBar(toolbar);
        TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(getString(R.string.professional_info));

        loadLayout();

    }


    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_salary.getWindowToken(), 0);
                return false;
            }
        });


        if (user.user_profession.length() != 0 && !user.user_profession.equals("null")) edt_profession.setText(user.user_profession);

        if (user.user_salary != 0) edt_salary.setText(String.valueOf(user.user_salary));

        //Education
        lstEducation.clear();
        for (UserModel.EducationLevel education_levels : UserModel.EducationLevel.possibleValues){
            lstEducation.add(education_levels.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstEducation);
        spn_education_level.setAdapter(_adapter);

        for (int i = 0; i < UserModel.EducationLevel.possibleValues.length; i++){
            if (user.user_educationlevel == UserModel.EducationLevel.possibleValues[i]){
                spn_education_level.setSelection(i);
            }
        }

        spn_education_level.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_educationlevel = UserModel.EducationLevel.possibleValues[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        //Job Status
        lstJobStatus.clear();
        for (UserModel.JobStatus jobStatus : UserModel.JobStatus.possibleValues){
            lstJobStatus.add(jobStatus.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstJobStatus);
        spn_jobstatus.setAdapter(_adapter);
        for (int i = 0; i < UserModel.JobStatus.possibleValues.length; i++){
            if (user.user_jobstatus == UserModel.JobStatus.possibleValues[i]){
                spn_jobstatus.setSelection(i);
            }
        }

        spn_jobstatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_jobstatus = UserModel.JobStatus.getJobStatus(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //relocate
       if (user.user_gender == UserModel.Gender.male){

           switch (user.user_relocatable){

               case 0:
                   radioReNo.setChecked(true);
                   break;
               case 1:
                   radioReYes.setChecked(true);
                   break;
               default:
                   radioReNo.setChecked(true);
                   break;
           }

           radioRelocate.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(RadioGroup radioGroup, int i) {
                   if (radioReYes.isChecked()) {
                       user.user_relocatable = 1;
                   }
                   else if (radioReNo.isChecked()){
                       user.user_relocatable = 0;
                   }
               }
           });

       } else lyt_relocate.setVisibility(View.GONE);


        //own apartment.
        if (user.user_gender == UserModel.Gender.male){

            switch (user.user_ownapartment){

                case 0:
                    radioNoApart.setChecked(true);
                    break;
                case 1:
                    radioYesApart.setChecked(true);
                    break;
                default:
                    radioReNo.setChecked(true);
                    break;
            }

            radioOwnApart.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (radioYesApart.isChecked()) {
                        user.user_ownapartment = 1;
                    }
                    else if (radioNoApart.isChecked()){
                        user.user_ownapartment = 0;
                    }
                }
            });

        } else  lyt_ownApart.setVisibility(View.GONE);


        if (Constants.SIGNUP_STATUS == 10) btn_next.setText(getString(R.string.update));
        else btn_next.setText(getString(R.string.next));
    }

    @OnClick(R.id.btn_next) void gotoMatchReq(){

        if (checkValid())
            updateProfessionalInfo();
    }

    private void updateProfessionalInfo(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdate(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(user.user_id));
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_PROFESSIONAL, String.valueOf(edt_profession.getText().toString()));
                    params.put(ReqConst.PARAM_USER_EDUCATIONLEVEL, String.valueOf(user.user_educationlevel.rawValue));
                    params.put(ReqConst.PARAM_USER_JOBSTATUS, String.valueOf(user.user_jobstatus.rawValue));
                    params.put(ReqConst.PARAM_USER_SALARY, String.valueOf(edt_salary.getText().toString()));
                    params.put(ReqConst.PARAM_USER_RELOCATABLE, String.valueOf(user.user_relocatable));
                    params.put(ReqConst.PARAM_USER_OWNAPARTMENT, String.valueOf(user.user_ownapartment));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }


    private void parseUpdate(String json){

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                UserModel user;

                showToast(result_msg);

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);

                Commons.g_user = user;

                if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5){

                    Intent intent = new Intent(this, MatchRequirementsActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    Constants.SIGNUP_STATUS = 0;
                    startActivity(intent);
                    finish();
                }

            } else {

                closeKProgress();
                if (result_msg.equals("Session token has been tampered with")) {
                    ApiHelper manager = new ApiHelper();
                    manager.logOut(this);
                }

                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private boolean checkValid(){

        if (edt_profession.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.input_profession));
            return false;

        } else if (edt_salary.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.input_salary));
            return false;
        }

        return true;
    }



    @OnClick(R.id.imv_back) void backAction(){

        if (Constants.SIGNUP_STATUS == 0){

            Intent intent = new Intent(this, ReligionCasteActivity.class);
            intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
            startActivity(intent);

        }else if (Constants.SIGNUP_STATUS == 10){

            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
            startActivity(intent);
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        backAction();
    }
}
