package itwhiz4u.shaadimubark.activity;

import android.content.Intent;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;
import com.sjl.foreground.Foreground;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.fragment.ChatFragment;
import itwhiz4u.shaadimubark.fragment.FavoriteFragment;
import itwhiz4u.shaadimubark.fragment.NotificationFragment;
import itwhiz4u.shaadimubark.fragment.SearchFragment;
import itwhiz4u.shaadimubark.fragment.SettingsFragment;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.ChatManager;

public class MainActivity extends CommonActivity {

    private static String currentFragment = "";

    @BindView(R.id.bottomBar) BottomBar bottomBar;
    @BindView(R.id.toolbar_title) TextView txv_title;
    @BindView(R.id.imv_mark) ImageView imv_mark;
    public @BindView(R.id.imv_back) ImageView imv_back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

       loadLayout();

    }

    private void loadLayout() {

        ChatManager.setConnectedRef();
        ChatManager.setDeleteUserListener();

        if (Constants.PAGE_TO.equals(Constants.SETTING_PAGE)) {
            bottomBar.selectTabAtPosition(4);
        } else if (Constants.PAGE_TO.equals(Constants.CHATTING_PAGE)){
            bottomBar.selectTabAtPosition(3);
        }

        Constants.PAGE_TO = "";

        final BottomBarTab notification = bottomBar.getTabWithId(R.id.tab_notification);
        if (Commons.notiCount > 0){
            notification.setBadgeCount(Commons.notiCount);
        } else notification.removeBadge();

        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {

                if (tabId == R.id.tab_search) {

                    gotoSearchFragment();

                } else if (tabId == R.id.tab_favorite){

                    gotoFavoriteFragment();

                } else if (tabId ==  R.id.tab_notification){

                    notification.removeBadge();
                    gotoNotificationFragment();

                } else if(tabId == R.id.tab_chat){

                    gotoChatFragment();

                } else if (tabId == R.id.tab_setting){

                    gotoSettingsFragment();
                }

                Constants.PAGE_TO = "";
            }
        });
    }

    public void gotoSearchFragment() {

        currentFragment = Constants.SEARCH_FRAGMENT;

        txv_title.setText(R.string.search);

        imv_back.setVisibility(View.GONE);
        imv_mark.setVisibility(View.VISIBLE);
        imv_mark.setImageResource(R.mipmap.ic_filter);
        imv_mark.setColorFilter(getResources().getColor(R.color.white));
        SearchFragment fragment = new SearchFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoFavoriteFragment() {

        currentFragment = Constants.FAVORITE_FRAGMENT;

        txv_title.setText(R.string.favorite);
        imv_back.setVisibility(View.GONE);
        imv_mark.setVisibility(View.GONE);
        FavoriteFragment fragment = new FavoriteFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoNotificationFragment() {

        currentFragment = Constants.NOTIFICATION_FRAGMENT;

        txv_title.setText(R.string.notification);

        if (Commons.myNotifications.size() > 0){
            imv_mark.setVisibility(View.VISIBLE);
            imv_mark.setImageResource(R.drawable.ic_delete);
        } else {
            imv_mark.setVisibility(View.GONE);
        }

        imv_back.setVisibility(View.GONE);
        NotificationFragment fragment = new NotificationFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoChatFragment() {

        currentFragment = Constants.CHAT_FRAGMENT;

        txv_title.setText(R.string.chat);

        imv_mark.setVisibility(View.GONE);
        imv_back.setVisibility(View.GONE);
        ChatFragment fragment = new ChatFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoSettingsFragment() {

        currentFragment = Constants.SETTING_FRAGMENT;

        txv_title.setText(R.string.settings);

        imv_mark.setVisibility(View.GONE);
        imv_back.setVisibility(View.GONE);
        SettingsFragment fragment = new SettingsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoChatRoom(int userId){

        Intent intent  = new Intent(this, ChatActivity.class);
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        onExit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentActivity(this, Constants.MAIN_ACTIVITY);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearActivityReferences();
    }

    public void refreshFragment(){
        Log.d("aaaaa===", "RefreshFragment");
        switch (currentFragment){
            case Constants.CHAT_FRAGMENT:
                gotoChatFragment();
                break;
            case Constants.FAVORITE_FRAGMENT:
                gotoFavoriteFragment();
                break;
            case Constants.NOTIFICATION_FRAGMENT:
                gotoNotificationFragment();
                break;
            case Constants.SEARCH_FRAGMENT:
                gotoSearchFragment();
                break;
            case Constants.SETTING_FRAGMENT:
                gotoSettingsFragment();
                break;
        }
    }
}
