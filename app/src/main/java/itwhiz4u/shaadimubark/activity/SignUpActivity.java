package itwhiz4u.shaadimubark.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.BitmapUtils;
import itwhiz4u.shaadimubark.utils.MultiPartRequest;

public class SignUpActivity extends CommonActivity {

    @BindView(R.id.imv_photo) ImageView imv_photo;

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_first_name) EditText edt_first_name;
    @BindView(R.id.edt_last_name) EditText edt_last_name;
    @BindView(R.id.edt_user_name) EditText edt_user_name;
    @BindView(R.id.edt_phone_number) EditText edt_phone_number;
    @BindView(R.id.txv_date_birth) TextView txv_date_birth;
    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.edt_confirm_pass) EditText edt_confirm_pass;
    @BindView(R.id.btn_signup) Button btn_signup;

    @BindView(R.id.checkbox_terms_con) CheckBox checkbox_terms_con;
    @BindView(R.id.rlt_password) RelativeLayout rlt_password;
    @BindView(R.id.rlt_confirm_pass) RelativeLayout rlt_confirm_pass;
    @BindView(R.id.lyt_terms) LinearLayout lyt_terms;

    String _email = "", _first_name = "", _last_name = "", _user_name = "", _phone_number = "", _date_birth = "", _password = "", _confirm_pass = "";

    Calendar myCalendar;

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    public static int MY_PEQUEST_CODE = 123;

    Uri _imageCaptureUri;
    String _photoPath = "";
    DatePickerDialog datePicker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        if (Constants.SIGNUP_STATUS == 10){ // update user
            btn_signup.setText(getString(R.string.update));
            lyt_terms.setVisibility(View.GONE);
            rlt_password.setVisibility(View.GONE);
            rlt_confirm_pass.setVisibility(View.GONE);

            LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
            lytContainer.setOnTouchListener(new View.OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                    return false;
                }
            });

            setUserDate();
        }

        checkbox_terms_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkbox_terms_con.isChecked()){
                    gotoTermsPolicy();
                }
            }
        });

        myCalendar = Calendar.getInstance();
    }

    private void setUserDate(){

        UserModel user = Commons.g_user;
        if (user.user_photo.length() > 0) Picasso.with(this).load(user.user_photo).placeholder(R.drawable.img_user).into(imv_photo);
        edt_email.setText(user.user_email);
        edt_email.setEnabled(false);
        edt_first_name.setText(user.user_firstname);
        edt_last_name.setText(user.user_lastname);
        edt_user_name.setText(user.getName());
        edt_user_name.setEnabled(false);
        edt_phone_number.setText(String.valueOf(user.user_phonenumber));
        txv_date_birth.setText(String.valueOf(user.user_birthday));

        if (Constants.SIGNUP_STATUS == 10){

            edt_password.setText("000000");
            edt_confirm_pass.setText("000000");
            checkbox_terms_con.setChecked(true);
        }
    }

    private void updateProfile(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdateProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                    params.put(ReqConst.SESSION, EasyPreference.with(getApplication()).getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_FCMTOKEN, EasyPreference.with(_context).getString(Constants.TOKEN, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.copyUser().user_id));
                    params.put(ReqConst.PARAM_FIRST_NAME, _first_name);
                    params.put(ReqConst.PARAM_LAST_NAME, _last_name);
                    params.put(ReqConst.PARAM_USER_PHONE_NUMBER, _phone_number);
                    params.put(ReqConst.PARAM_USER_BIRTHDAY, _date_birth);
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }


    private void parseUpdateProfile(String json){

        Log.d("update==>", json);

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String  result = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                showToast(result_msg);

                UserModel user;
                //EasyPreference.with(this).addString(PrefConst.PREFKEY_SESSION, response.getString(ReqConst.SESSION)).save();

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);
                Commons.g_user = user;


            } else {
                closeKProgress();
                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void updatePhoto(){

        try {

            showKProgress();
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
            params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE_IMAGE);

            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.copyUser().user_id));
            params.put(ReqConst.SESSION, EasyPreference.with(this).getString(PrefConst.PREFKEY_SESSION, ""));
            params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
            String url = ReqConst.SERVER_URL;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (isFinishing()) {
                        closeKProgress();
                        showAlertDialog(getString(R.string.network_error));
                    }
                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    parseUpdatePhoto(json);

                }

            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ShaadiMubarkApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e){

            e.printStackTrace();
            closeKProgress();
            showAlertDialog(getString(R.string.network_error));
        }
    }


    private void parseUpdatePhoto(String json){

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String  result = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                showToast(result_msg);
                Commons.g_user.copyUser().user_photo = response.getString(ReqConst.RES_USER_PHOTO);
                Commons.g_user.user_photo = response.getString(ReqConst.RES_USER_PHOTO);

            } else {
                closeKProgress();
                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //sign up without photo
    private void progressSignUp(){ //without photo

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSignUp(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_SIGNUP);

                    params.put(ReqConst.PARAM_USER_EMAIL, _email);
                    params.put(ReqConst.PARAM_FIRST_NAME, _first_name);
                    params.put(ReqConst.PARAM_LAST_NAME, _last_name);
                    params.put(ReqConst.PARAM_USER_NAME, _user_name);
                    params.put(ReqConst.PARAM_USER_PHONE_NUMBER, _phone_number);
                    params.put(ReqConst.PARAM_USER_BIRTHDAY, _date_birth);
                    params.put(ReqConst.PARAM_USER_PASSWORD, _password);
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }


    private void progressSignUpPhoto(){ //with photo

        try {

            showKProgress();
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
            params.put(ReqConst.FUNCTION, ReqConst.FUNC_SIGNUP);
            params.put(ReqConst.PARAM_FCMTOKEN, EasyPreference.with(_context).getString(Constants.TOKEN, ""));

            params.put(ReqConst.PARAM_USER_EMAIL, _email);
            params.put(ReqConst.PARAM_FIRST_NAME, _first_name);
            params.put(ReqConst.PARAM_LAST_NAME, _last_name);
            params.put(ReqConst.PARAM_USER_NAME, _user_name);
            params.put(ReqConst.PARAM_USER_PHONE_NUMBER, _phone_number);
            params.put(ReqConst.PARAM_USER_BIRTHDAY, _date_birth);
            params.put(ReqConst.PARAM_USER_PASSWORD, _password);
            params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
            String url = ReqConst.SERVER_URL;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (isFinishing()) {
                        closeKProgress();
                        showAlertDialog(getString(R.string.network_error));
                    }
                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    parseSignUp(json);

                }

            }, file, ReqConst.PARAM_FILE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ShaadiMubarkApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e){

            e.printStackTrace();
            closeKProgress();
            showAlertDialog(getString(R.string.network_error));
        }
    }


    private void parseSignUp(String json){

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String  result = response.getString(ReqConst.RESULT);
            String msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                showToast(msg);

                EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, _password).save();
                EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL, _email).save();

                Intent intent = new Intent(this, VerifyActivity.class);
                Constants.SIGNUP_STATUS = 0; //sign up status
                startActivity(intent);
                finish();

            } else {
                closeKProgress();
                showAlertDialog(msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.imv_photo) void takePhoto(){

        if (!hasPermissions(this, PERMISSIONS)){

            ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
        }else {

            setPhoto();
        }
    }

    @OnClick({R.id.txv_terms,R.id.txv_policy}) void gotoTermsPolicy(){

        startActivity(new Intent(this,TermsPolicyActivity.class));
        checkbox_terms_con.setChecked(true);

    }


    @OnClick(R.id.btn_signup) void gotoPersonalInfo(){

        _email = edt_email.getText().toString();
        _first_name = edt_first_name.getText().toString();
        _last_name = edt_last_name.getText().toString();
        _user_name = edt_user_name.getText().toString();
        _phone_number = edt_phone_number.getText().toString();
        _date_birth = txv_date_birth.getText().toString();
        _password = edt_password.getText().toString();
        _confirm_pass = edt_confirm_pass.getText().toString();

        if (checkValid()) {

            if (Constants.SIGNUP_STATUS == 10){// update profile

                updateProfile();

            } else {
                if (_photoPath.length() > 0) //case take photo
                    progressSignUpPhoto();
                else progressSignUp();
            }
        }
    }


    @OnClick(R.id.txv_date_birth) void gotoDatePickerDialog(){
        if(datePicker == null) {
            datePicker = new DatePickerDialog(this, dateArrival, myCalendar.get(Calendar.YEAR) - 18, myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
            Calendar cal = Calendar.getInstance();
            cal.set(myCalendar.get(Calendar.YEAR) - 18, myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

            datePicker.getDatePicker().setMaxDate(cal.getTimeInMillis());
        }
        datePicker.show();

    }

    DatePickerDialog.OnDateSetListener dateArrival = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            setDateOfBirth();
        }

    };


    private void setDateOfBirth() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat);
        txv_date_birth.setText(sdf.format(myCalendar.getTime()));
    }


    public void setPhoto(){

        final String[] items = {getString(R.string.take_photo), getString(R.string.choose_gallery) ,getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

            setPhoto();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK) {

                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*if (Constants.SIGNUP_STATUS == 10)
                        updatePhoto();*/
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK) {
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA: {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    /*intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);*/
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }
        }
    }

    private boolean checkValid(){

        if (_photoPath.length() == 0){
            showAlertDialog(getString(R.string.take_profile_photo));
            return false;
        }
        else if (_email.length() == 0) {
            showAlertDialog(getString(R.string.input_your_email));
            return false;

        }else if (!Patterns.EMAIL_ADDRESS.matcher(_email).matches()){
            showToast(getString(R.string.valid_email));
            return false;

        } else if (_first_name.length() == 0){
            showAlertDialog(getString(R.string.input_first_name));
            return false;
        } else if (_last_name.length() == 0){
            showAlertDialog(getString(R.string.input_last_name));
            return false;
        } else if (_user_name.length() == 0){
            showAlertDialog(getString(R.string.input_user_name));
            return false;
        } else if (_phone_number.length() == 0){
            showAlertDialog(getString(R.string.input_phone_number));
            return false;
        } else if (_date_birth.length() == 0){
            showAlertDialog(getString(R.string.input_birthday));
            return false;
        } else if (_password.length() == 0){
            showAlertDialog(getString(R.string.input_password));
            return false;
        } else if (!_password.equals(_confirm_pass)){
            showAlertDialog(getString(R.string.check_confirm_password));
            return false;
        } else if (!checkbox_terms_con.isChecked()){
            showAlertDialog(getString(R.string.check_term_condition));
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {

        if (Constants.SIGNUP_STATUS == 0){

            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
