package itwhiz4u.shaadimubark.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Commons.g_isAppRunning = true;
        gotoMain();
    }

    private void gotoMain() {


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                /*startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                finish();*/

                startActivity(new Intent(SplashActivity.this, SignInActivity.class));
                finish();
            }
        }, Constants.SPLASH_TIME);
    }
}
