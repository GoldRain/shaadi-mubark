package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.UserModel;

public class ForgotActivity extends CommonActivity {

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.btn_reset_password) Button btn_reset_password;
    String _email = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(), 0);
                return false;
            }
        });

        _email = edt_email.getText().toString();
    }

    private void processVerify(){


        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseVerify(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_RESEND_CODE);
                    //param
                    params.put(ReqConst.PARAM_USER_EMAIL, _email);
                    EasyPreference.with(_context).addString(PrefConst.PREFKEY_USEREMAIL, _email).save();
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseVerify(String json){

        closeKProgress();
        try {

            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                showToast(result_msg);

                Intent intent = new Intent(this, VerifyActivity.class);
                Constants.SIGNUP_STATUS = 100; //special forgot
                startActivity(intent);
                finish();

            } else {

                closeKProgress();
                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btn_reset_password) void gotoVerify(){

        _email = edt_email.getText().toString();

        if (checkValid()){

            processVerify();
        }

    }

    @OnClick(R.id.imv_back) void gotoLogin(){
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }

    private boolean checkValid(){
        if (_email.length() == 0){
            showAlertDialog(getString(R.string.input_your_email));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
