package itwhiz4u.shaadimubark.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.adapter.SpinnerAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;

public class ReligionCasteActivity extends CommonActivity {

    SpinnerAdapter _adapter;
    @BindView(R.id.spn_religion) Spinner spn_religion;
    List<String> _lstReligion = new ArrayList<>();

    //@BindView(R.id.edt_caste) EditText edt_caste;
    @BindView(R.id.edt_sub_caste) EditText edt_sub_caste;
    @BindView(R.id.btn_next) Button btn_next;
    @BindView(R.id.txv_caste) AutoCompleteTextView txv_caste;

    UserModel user;

    ArrayList<String> arrayCaste = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_religion_caste);

        user = (UserModel) getIntent().getSerializableExtra(Constants.KEY_USER);
        ButterKnife.bind(this);
        loadLayout();

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_religion);
        setSupportActionBar(toolbar);
        TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(getString(R.string.religion_caste));
    }

    private void loadLayout() {

        _lstReligion.clear();
        for (UserModel.Religion religions : UserModel.Religion.possibleValues){
            _lstReligion.add(religions.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, _lstReligion);
        spn_religion.setAdapter(_adapter);

        for (int i = 0; i < UserModel.Religion.possibleValues.length ; i++){
            if (user.user_religion == UserModel.Religion.possibleValues[i]){
                spn_religion.setSelection(i);
            }
        }
        spn_religion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_religion = UserModel.Religion.possibleValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        String caste = user.user_castestring;
        if(user.user_caste == null) {

            caste = "";
        }
        else {
            caste = user.user_caste.caste_string();
        }

        txv_caste.setText(caste);

        ArrayList<CasteModel> strCaste = new ArrayList<>();
        strCaste = CasteModel.allCastes;

        for (int i = 0; i < strCaste.size(); i++){
            arrayCaste.add(strCaste.get(i).caste_string());
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, R.layout.spinner_item, arrayCaste);
        txv_caste.setThreshold(0);
        txv_caste.setAdapter(adapter);
        txv_caste.setTextColor(Color.BLACK);


        if (!user.user_subcaste.equals("null") && user.user_subcaste.length() != 0)
            edt_sub_caste.setText(user.user_subcaste);

        if (Constants.SIGNUP_STATUS == 10) btn_next.setText(getString(R.string.update));
        else if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5) btn_next.setText(getString(R.string.next));

    }

    @OnClick(R.id.btn_next) void updateProfile(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdate(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(user.user_id));
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_RELIGION, String.valueOf(user.user_religion.rawValue));
                    params.put(ReqConst.PARAM_USER_CASTESTRING, String.valueOf(txv_caste.getText().toString()));
                    CasteModel caste = getCaste(txv_caste.getText().toString());
                    if(caste == null) {
                        params.put(ReqConst.PARAM_USER_CASTE, String.valueOf(0));
                    }
                    else {
                        params.put(ReqConst.PARAM_USER_CASTE, String.valueOf(caste.caste_id));
                    }
                    params.put(ReqConst.PARAM_USER_SUBCASTE, edt_sub_caste.getText().toString());
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpdate(String json){

        Log.d("religion==>", json);

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                UserModel user;

                showToast(result_msg);

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);

                Commons.g_user = user;

                if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5){

                    Intent intent = new Intent(this, ProfessionalInfoActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    Constants.SIGNUP_STATUS = 0;
                    startActivity(intent);
                    finish();
                }

            } else {

                if (result_msg.equals(getString(R.string.session_tampered))){
                    ApiHelper manager = new ApiHelper();
                    manager.logOut(this);
                }

                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.imv_back) void backAction(){

        Intent intent = new Intent(this, PersonalInfoActivity.class);
        intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
        startActivity(intent);

        finish();
    }

    private CasteModel getCaste(String string) {
        for(CasteModel caste : CasteModel.allCastes) {
            if(caste.caste_string().toLowerCase().equals(string.toLowerCase())) {
                return caste;
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        backAction();
    }
}
