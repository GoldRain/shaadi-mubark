package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.Countrypicker.CountryPicker;
import itwhiz4u.shaadimubark.Countrypicker.OnCountryPickerListener;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.adapter.SpinnerAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.User;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;

public class PersonalInfoActivity extends CommonActivity implements OnCountryPickerListener {

    @BindView(R.id.imv_mark) ImageView imv_mark;
    @BindView(R.id.imv_back) ImageView imv_back;
    @BindView(R.id.toolbar_title) TextView title;

    @BindView(R.id.radioSex) RadioGroup radioSex;
    @BindView(R.id.radioMale) RadioButton radioMale;
    @BindView(R.id.radioFemale) RadioButton radioFemale;

    @BindView(R.id.edt_height) EditText edt_height;
    @BindView(R.id.edt_siblings) EditText edt_siblings;

    //country picker
    CountryPicker countryPicker;
    @BindView(R.id.txv_country) TextView txv_country;
    @BindView(R.id.edt_city) EditText edt_city;

    SpinnerAdapter _adapter;
    @BindView(R.id.spn_marital_status) Spinner spn_marital_status;
    @BindView(R.id.spn_complexion) Spinner spn_complexion;
    @BindView(R.id.spn_body_type) Spinner spn_body_type;
    @BindView(R.id.spn_appearance) Spinner spn_appearance;
    @BindView(R.id.spn_hair) Spinner spn_hair;
    @BindView(R.id.spn_facial_hair) Spinner spn_facial_hair;

    List<String> lstMaritalStatus = new ArrayList<>();
    List<String> lstComplexion = new ArrayList<>();
    List<String> lstBodyType = new ArrayList<>();
    List<String> lstAppearance = new ArrayList<>();
    List<String> lstHair = new ArrayList<>();
    List<String> lstFacialHair = new ArrayList<>();

    @BindView(R.id.lyt_children) LinearLayout lyt_children;
    @BindView(R.id.edt_children) EditText edt_children;
    @BindView(R.id.lyt_men) LinearLayout lyt_men;
    @BindView(R.id.btn_next) Button btn_next;
    String countryCode = "";

    UserModel user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info);

        user = (UserModel) getIntent().getSerializableExtra(Constants.KEY_USER);
        ButterKnife.bind(this);
        setListener();
        loadLayout();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_personal);
        setSupportActionBar(toolbar);
        title.setText(getString(R.string.personal_info));

        if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5)
            imv_back.setVisibility(View.GONE);
        else imv_back.setVisibility(View.VISIBLE);

        imv_mark.setVisibility(View.GONE);

    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_height.getWindowToken(), 0);
                return false;
            }
        });

        //height
        if (user.user_height == 0.0){
            edt_height.setText("");
        } else edt_height.setText(String.valueOf(user.user_height));

        if (user.user_totalsibling == 0){
            edt_siblings.setText("");
        } else edt_siblings.setText(String.valueOf(user.user_totalsibling));

        if (user.user_country.equals("null") || user.user_country.length() == 0){
            txv_country.setText("");
        }
        else
            txv_country.setText(new Locale("", user.user_country).getDisplayCountry());

        if (user.user_city.equals("null") || user.user_city.length() == 0)
            edt_city.setText("");
        else edt_city.setText(user.user_city);

        //user gender
        switch (user.user_gender) {
            case male:
                radioMale.setChecked(true);
                lyt_men.setVisibility(View.VISIBLE);
                break;
            case female:
                radioFemale.setChecked(true);
                lyt_men.setVisibility(View.GONE);
                break;

                default:
                    break;
        }

        countryPicker =
                new CountryPicker.Builder().with(this)
                        .listener(this)
                        .build();

        for (int i = 0; i < CountryModel.allCountry.size(); i++){

        }

        if (Constants.SIGNUP_STATUS == 10){

            radioSex.setEnabled(false);
            radioFemale.setEnabled(false);
            radioMale.setEnabled(false);

        }

        radioSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                if (radioMale.isChecked()){
                    lyt_men.setVisibility(View.VISIBLE);
                    user.user_gender = UserModel.Gender.male;

                } else if (radioFemale.isChecked()) {
                    lyt_men.setVisibility(View.GONE);
                    user.user_gender = UserModel.Gender.female;
                }
            }
        });


        //Marital Status
        lstMaritalStatus.clear();
        for (UserModel.MaritalStatus maritalStatus : UserModel.MaritalStatus.possibleValues){
            lstMaritalStatus.add(maritalStatus.getString());
        }
        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstMaritalStatus);
        spn_marital_status.setAdapter(_adapter);

        for (int i = 0; i < UserModel.MaritalStatus.possibleValues.length; i++){
            if (user.user_maritalstatus == UserModel.MaritalStatus.possibleValues[i]){

                spn_marital_status.setSelection(i);
                if (user.user_maritalstatus.rawValue > 1) {
                    lyt_children.setVisibility(View.VISIBLE);
                    edt_children.setText(String.valueOf(user.user_children));
                } else lyt_children.setVisibility(View.GONE);
                break;
            }
        }

        spn_marital_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                user.user_maritalstatus = UserModel.MaritalStatus.possibleValues[position];

                if (user.user_maritalstatus.rawValue > 1) {
                    lyt_children.setVisibility(View.VISIBLE);
                    edt_children.setText(String.valueOf(user.user_children));
                } else lyt_children.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //complexion

        lstComplexion.clear();

        for (UserModel.Complexion complexion : UserModel.Complexion.possibleValues){
            lstComplexion.add(complexion.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstComplexion);
        spn_complexion.setAdapter(_adapter);

        for (int i = 0;i < UserModel.Complexion.possibleValues.length; i++){
            if(user.user_complexion == UserModel.Complexion.possibleValues[i]) {
                spn_complexion.setSelection(i);
                break;
            }
        }

        spn_complexion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                user.user_complexion = UserModel.Complexion.possibleValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //body type

        lstBodyType.clear();
        for (UserModel.BodyType body_types : UserModel.BodyType.possibleValues){

            lstBodyType.add(body_types.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstBodyType);
        spn_body_type.setAdapter(_adapter);

        for (int i = 0; i < UserModel.BodyType.possibleValues.length; i++){
            if (user.user_bodytype == UserModel.BodyType.possibleValues[i]){
                spn_body_type.setSelection(i);
            }
        }


        spn_body_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_bodytype = UserModel.BodyType.possibleValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //appearance

        lstAppearance.clear();
        for (UserModel.Appearance appearances : UserModel.Appearance.possibleValues){
            lstAppearance.add(appearances.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstAppearance);
        spn_appearance.setAdapter(_adapter);

        for (int i = 0; i < UserModel.Appearance.possibleValues.length; i++){
            if (user.user_appearance == UserModel.Appearance.possibleValues[i]){
                spn_appearance.setSelection(i);
            }
        }

        spn_appearance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_appearance = UserModel.Appearance.possibleValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //hair

        lstHair.clear();
        for (UserModel.Hair hair : UserModel.Hair.possibleValues){
            lstHair.add(hair.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstHair);
        spn_hair.setAdapter(_adapter);

        for (int i = 0; i < UserModel.Hair.possibleValues.length; i++){
            if (user.user_hair ==  UserModel.Hair.possibleValues[i]){
                spn_hair.setSelection(i);
            }
        }

        spn_hair.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_hair = UserModel.Hair.possibleValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //facial Fair
        lstFacialHair.clear();
        for (UserModel.FacialHair facial_fair : UserModel.FacialHair.possibleValues){
            lstFacialHair.add(facial_fair.getString());
        }

        _adapter = new SpinnerAdapter(this, R.layout.spinner_item, android.R.id.text1, lstFacialHair);
        spn_facial_hair.setAdapter(_adapter);

        for (int i = 0; i < UserModel.FacialHair.possibleValues.length; i++){
            if (user.user_facialhair ==  UserModel.FacialHair.possibleValues[i]){
                spn_facial_hair.setSelection(i);
            }
        }

        spn_facial_hair.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                user.user_facialhair = UserModel.FacialHair.possibleValues[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (Constants.SIGNUP_STATUS == 10) btn_next.setText(getString(R.string.update));
        else btn_next.setText(getString(R.string.next));

    }

    //country picker
    private void setListener() {
        txv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countryPicker.showDialog(getSupportFragmentManager());
            }
        });
    }


    @Override
    public void onSelectCountry(CountryModel country) {
        txv_country.setText(country.country_string());
        countryCode = country.code;
        user.user_country = country.code;
    }


    @OnClick(R.id.btn_next) void nextAction(){ // update on server
        if (checkValid())
            updateProfile();
    }

    private void updateProfile(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdate(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(user.user_id));

                    params.put(ReqConst.PARAM_USER_GENDER, String.valueOf(user.user_gender.rawValue));
                    params.put(ReqConst.PARAM_USER_HEIGHT, String.valueOf(edt_height.getText().toString()));
                    params.put(ReqConst.PARAM_USER_TOTALSIBLING, String.valueOf(edt_siblings.getText().toString()));
                    params.put(ReqConst.PARAM_USER_COUNTRY, user.user_country);
                    params.put(ReqConst.PARAM_USER_CITY, edt_city.getText().toString());
                    params.put(ReqConst.PARAM_USER_MARITAL_STATUS, String.valueOf(user.user_maritalstatus.rawValue));

                    //if (Integer.parseInt(edt_children.getText().toString()) > 0)
                    params.put(ReqConst.PARAM_USER_CHILDREN, String.valueOf(edt_children.getText().toString()));

                    params.put(ReqConst.PARAM_USER_COMPLEXION, String.valueOf(user.user_complexion.rawValue));
                    params.put(ReqConst.PARAM_USER_BODYTYPE, String.valueOf(user.user_bodytype.rawValue));
                    params.put(ReqConst.PARAM_USER_APPEARANCE, String.valueOf(user.user_appearance.rawValue));

                    params.put(ReqConst.PARAM_USER_HAIR, String.valueOf(user.user_hair.rawValue));
                    params.put(ReqConst.PARAM_USER_FACIAL, String.valueOf(user.user_facialhair.rawValue));
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpdate(String json){

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                UserModel user;

                showToast(result_msg);

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);

                Commons.g_user = user;

                if (Constants.SIGNUP_STATUS == 0 || Constants.SIGNUP_STATUS == 5){

                    Intent intent = new Intent(this, ReligionCasteActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    Constants.SIGNUP_STATUS = 0;
                    startActivity(intent);
                    finish();

                }

            } else {

                if (result_msg.equals(getString(R.string.session_tampered))){

                    ApiHelper manager = new ApiHelper();
                    manager.logOut(this);
                }

                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private boolean checkValid(){

        if (user.user_gender.rawValue == 0){
            showAlertDialog(getString(R.string.choose_gender));
            return false;
        }
        else if (edt_height.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.input_height));
            return false;
        } else if (txv_country.getText().toString().length() == 0){

            showAlertDialog(getString(R.string.pick_country));
            return false;
        } else if (edt_city.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.input_your_city));
            return false;
        }

        return true;
    }

    @OnClick(R.id.imv_back) void backAction(){

        if (Constants.SIGNUP_STATUS == 0){
            onExit();

        } else if (Constants.SIGNUP_STATUS == 5){

            onExit();

        }else if (Constants.SIGNUP_STATUS == 10){

            Intent intent = new Intent(this, UserProfileActivity.class);
            intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        backAction();
    }

}
