 package itwhiz4u.shaadimubark.activity;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.Space;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telecom.Call;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.adapter.UserProfileImagEditAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.AppBarStateChangeListener;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;
import itwhiz4u.shaadimubark.utils.BitmapUtils;
import itwhiz4u.shaadimubark.utils.Callback.Callback;
import itwhiz4u.shaadimubark.utils.DateUtils;
import itwhiz4u.shaadimubark.utils.MultiPartRequest;
import itwhiz4u.shaadimubark.utils.Multipartyrequest;
import itwhiz4u.shaadimubark.utils.Utils;

import static itwhiz4u.shaadimubark.activity.SignUpActivity.MY_PEQUEST_CODE;

 public class UserProfileActivity extends CommonActivity {

     private final static float EXPAND_AVATAR_SIZE_DP = 110f;
     private final static float COLLAPSED_AVATAR_SIZE_DP = 36f;

     @BindView(R.id.app_bar) AppBarLayout mAppBarLayout;
     @BindView(R.id.toolbar_profile) Toolbar toolbar_profile;
     @BindView(R.id.collapsingToolbar) CollapsingToolbarLayout collapsingToolbar;
     @BindView(R.id.toolbar_title) TextView mToolbarTextView;
     @BindView(R.id.txv_name_profile_) TextView mTitleTextView;
     @BindView(R.id.space) Space mSpace;


     //Avatar animation on AppBar
     AppBarStateChangeListener mAppBarStateChangeListener;
     private int[] mAvatarPoint = new int[2], mSpacePoint = new int[2], mToolbarTextPoint =
             new int[2], mTitleTextViewPoint = new int[2];
     private float mTitleTextSize;
     int _dp = 0;

     //scroll part
     @BindView(R.id.recycler_photos) RecyclerView recyclerPhotos;
     UserProfileImagEditAdapter _imageAdapter;
     ArrayList<String> _imagePaths = new ArrayList<>();

     Uri _imageCaptureUri;
     Uri _imageCaptureUri_profile;
     String _photoPath = "";
     String _photoPath_profile = "";
     int MAX_IMAGES = 6;

     int takePhotoStatus = 0; //5 profile , 10 : multi photo;

     //profile details
     @BindView(R.id.imv_chat_profile) ImageView imv_chat_profile;
     @BindView(R.id.imv_favorite_profile) ImageView imv_favorite_profile;

     //@BindView(R.id.txv_year) TextView txv_year;
     //@BindView(R.id.txv_height) TextView txv_height;
     @BindView(R.id.imv_photo_profile) CircularImageView imv_photo;

     /*contact info*/
    /* @BindView(R.id.txv_user_name) TextView txv_user_name;
     @BindView(R.id.txv_email) TextView txv_email;
     @BindView(R.id.txv_phone_number) TextView txv_phone_number;
     @BindView(R.id.txv_date_birth) TextView txv_date_birth;*/

     /*personal info*/
     @BindView(R.id.txv_age) TextView txv_age;
     @BindView(R.id.txv_height) TextView txv_height;
     @BindView(R.id.txv_marital_status) TextView txv_marital_status;
     @BindView(R.id.lyt_children) LinearLayout lyt_children;
     @BindView(R.id.txv_children) TextView txv_children;
     @BindView(R.id.txv_complexion) TextView txv_complexion;
     @BindView(R.id.txv_body_type) TextView txv_body_type;
     @BindView(R.id.txv_appearance) TextView txv_appearance;
     @BindView(R.id.txv_hair) TextView txv_hair;
     @BindView(R.id.txv_facial_hair) TextView txv_facial_hair;
     @BindView(R.id.txv_siblings) TextView txv_siblings;
     @BindView(R.id.txv_country_profile) TextView txv_country_profile;
     @BindView(R.id.txv_city_profile) TextView txv_city_profile;

     /*religion & caste*/
     @BindView(R.id.txv_religion) TextView txv_religion;
     @BindView(R.id.txv_caste) TextView txv_caste;
     @BindView(R.id.txv_sub_caste) TextView txv_sub_caste;
     //@BindView(R.id.imv_religion_edit) ImageView imv_religion_edit;

     /*professional info*/
     @BindView(R.id.txv_edu_level) TextView txv_edu_level;
     @BindView(R.id.txv_profession) TextView txv_profession;
     @BindView(R.id.txv_job_status) TextView txv_job_status;
     @BindView(R.id.txv_salary) TextView txv_salary;
     @BindView(R.id.txv_relocate_other) TextView txv_relocate_other;
     @BindView(R.id.txv_own_apart) TextView txv_own_apart;
     @BindView(R.id.lyt_relocate) LinearLayout lyt_relocate;

     /*match requirements*/
     @BindView(R.id.txv_fromAge) TextView txv_fromAge;
     @BindView(R.id.txv_toAge) TextView txv_toAge;
     @BindView(R.id.txv_fromHeight) TextView txv_fromHeight;
     @BindView(R.id.txv_toHeight) TextView txv_toHeight;
     @BindView(R.id.txv_filter_marital_status) TextView txv_filter_marital_status;
     @BindView(R.id.lyt_havechildren) LinearLayout lyt_havechildren;
     @BindView(R.id.txv_match_religion) TextView txv_match_religion;
     @BindView(R.id.txv_match_edu) TextView txv_match_edu;
     @BindView(R.id.txv_match_profession) TextView txv_match_profession;
     @BindView(R.id.txv_match_caste) TextView txv_match_caste;
     @BindView(R.id.txv_countries) TextView txv_countries;
     @BindView(R.id.txv_match_havechildren) TextView txv_match_havechildren;
     @BindView(R.id.txv_match_own_apart) TextView txv_match_own_apart;
     @BindView(R.id.txv_match_relocate) TextView txv_match_relocate;


     //@BindView(R.id.imv_contact_edit) ImageView imv_contact_edit;
     @BindView(R.id.imv_personal_edit) ImageView imv_personal_edit;
     //@BindView(R.id.imv_religion_edit) ImageView imv_religion_edit;
     @BindView(R.id.imv_prof_edit) ImageView imv_prof_edit;
     @BindView(R.id.imv_match_edit) ImageView imv_match_edit;
     @BindView(R.id.card_match) CardView card_match;
     @BindView(R.id.btn_done) Button btn_done;

     String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
     public static int MY_REQUEST_CODE = 123;

     UserModel user;
     ArrayList<String> _user_images = new ArrayList<>();
     @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

         Display display = getWindowManager().getDefaultDisplay();
         Constants.WIDTH = display.getWidth();

         user = (UserModel)getIntent().getSerializableExtra(Constants.KEY_USER);

        loadLayout();

        Constants.SIGNUP_STATUS = 10; //completed user profile

    }

     private void loadLayout() {

    /*     //beta version
         imv_chat_profile.setVisibility(View.GONE);
         imv_favorite_profile.setVisibility(View.GONE);

         //imv_contact_edit.setVisibility(View.GONE);
         imv_personal_edit.setVisibility(View.VISIBLE);
         imv_religion_edit.setVisibility(View.VISIBLE);
         imv_prof_edit.setVisibility(View.VISIBLE);
         imv_match_edit.setVisibility(View.VISIBLE);
         card_match.setVisibility(View.VISIBLE);*/


         if (Commons.g_user.user_id != user.user_id){

             //imv_contact_edit.setVisibility(View.GONE);
             imv_personal_edit.setVisibility(View.GONE);
             //imv_religion_edit.setVisibility(View.GONE);
             imv_prof_edit.setVisibility(View.GONE);
             imv_match_edit.setVisibility(View.GONE);
             card_match.setVisibility(View.GONE);
             imv_photo.setEnabled(false);

         }
         else {

             imv_chat_profile.setVisibility(View.GONE);
             imv_favorite_profile.setVisibility(View.GONE);
             btn_done.setVisibility(View.GONE);
             imv_photo.setEnabled(true);

         }

        _dp = getScreenWidthInDPs(this);

        collapsingToolbar.setTitle(getString(R.string.profile));

         mTitleTextSize = mTitleTextView.getTextSize();

         setUpToolbar();
         setUpAmazingAvatar();

         GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);

         //gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

         recyclerPhotos.setLayoutManager(gridLayoutManager);

         _imageAdapter = new UserProfileImagEditAdapter(this);
         recyclerPhotos.setAdapter(_imageAdapter);

         setUserProfile();
     }

     private void setUserProfile(){

         if (user.user_photo.length() > 0){
             Picasso.with(this).load(user.user_photo).placeholder(R.drawable.img_user).into(imv_photo);
         }

         mTitleTextView.setText(String.valueOf(user.user_username));
         /*txv_year.setText(String.valueOf(user.getAge()));
         txv_height.setText(String.valueOf(user.user_height));*/

         String[] imageArray = user.user_images.split(",");
         for (int i = 0; i <imageArray.length ; i++){
             if (imageArray[i].length() > 0)
                 _user_images.add(imageArray[i]);
         }

         //if (_user_images.size() > 0)
             addImages(_user_images);

         if (user.user_id != Commons.g_user.user_id && _user_images.size() == 0)
             recyclerPhotos.setVisibility(View.GONE);

         if (user.isFavorite == 1)
             imv_favorite_profile.setSelected(true);
         else imv_favorite_profile.setSelected(false);

         //contact info
         /*txv_user_name.setText(user.user_username);
         txv_email.setText(user.user_email);
         txv_phone_number.setText(user.user_phonenumber);
         txv_date_birth.setText(user.user_birthday);*/

         //personal info

         txv_age.setText(String.valueOf(user.getAge()));
         if (user.user_height == 0){
             txv_height.setText("");
         } else txv_height.setText(String.valueOf(user.user_height) + " " +"Cm");
         txv_marital_status.setText(user.user_maritalstatus.getString());
         if (user.user_children > 0){
             txv_children.setText(String.valueOf(user.user_children));
         }else {

         }
         txv_complexion.setText(user.user_complexion.getString());
         txv_body_type.setText(user.user_bodytype.getString());
         txv_appearance.setText(user.user_appearance.getString());
         txv_siblings.setText(String.valueOf(user.user_totalsibling));

         if (user.user_country.length()== 0 || user.user_country.equals("null"))
             txv_country_profile.setText("");
         else txv_country_profile.setText(new Locale("", user.user_country).getDisplayCountry());


         if (user.user_city.length() == 0 ||user.user_city.equals("null"))
             txv_city_profile.setText("");
         else txv_city_profile.setText(user.user_city);


         txv_marital_status.setText(user.user_maritalstatus.getString());
         if (user.user_maritalstatus.rawValue > 1){
             lyt_children.setVisibility(View.VISIBLE);
             txv_children.setText(String.valueOf(user.user_children));
         }
         else lyt_children.setVisibility(View.GONE);

         /*religion & caste*/
         txv_religion.setText(user.user_religion.getString());

         if (user.user_subcaste.equals("null") || user.user_subcaste.length() == 0)
             txv_sub_caste.setText("");
         else txv_sub_caste.setText(user.user_subcaste);

         String caste = user.user_castestring;
         if(user.user_caste == null) {
         }
         else {
             caste = user.user_caste.caste_string();
         }
         txv_caste.setText(caste);


         /*professional info*/
         txv_edu_level.setText(user.user_educationlevel.getString());

         if (user.user_profession.length() == 0 ||  user.user_profession.equals("null"))
             txv_profession.setText("");
         else txv_profession.setText(user.user_profession);

         txv_job_status.setText(user.user_jobstatus.getString());
         txv_salary.setText(String.valueOf(user.user_salary));



         if (user.user_gender == UserModel.Gender.male){

             //professional info
             if (user.user_relocatable == 0){
                 txv_relocate_other.setText(getString(R.string.no)); //true
             } else  txv_relocate_other.setText(getString(R.string.yes)); //true

             //personal info

             txv_hair.setText(user.user_hair.getString());
             txv_facial_hair.setText(user.user_facialhair.getString());

             if (user.user_ownapartment == 0){
                 txv_own_apart.setText(getString(R.string.no));     // 0: false, 1: true
             } else txv_own_apart.setText(getString(R.string.yes));

         }else {

             //professional info
             lyt_relocate.setVisibility(View.GONE);

             //personal info
             TextView hair_title = (TextView)findViewById(R.id.hair_title);
             hair_title.setVisibility(View.GONE);
             txv_hair.setVisibility(View.GONE);

             TextView facial_title = (TextView)findViewById(R.id.facial_title);
             facial_title.setVisibility(View.GONE);
             txv_facial_hair.setVisibility(View.GONE);

             TextView txv_own_aprt_title = (TextView)findViewById(R.id.txv_own_apart_title);
             txv_own_aprt_title.setVisibility(View.GONE);
             txv_own_apart.setVisibility(View.GONE);
         }

         //match requirement
         txv_fromAge.setText(String.valueOf(user.filter_agefrom));
         txv_toAge.setText(String.valueOf(user.filter_ageto));
         txv_fromHeight.setText(String.valueOf(user.filter_heightfrom));
         txv_toHeight.setText(String.valueOf(user.filter_heightto));
         txv_filter_marital_status.setText(user.filter_maritalstatusString());
         txv_match_religion.setText(user.filter_religion.getString());
         txv_match_edu.setText(user.filter_minimumedu.getString());
         txv_match_profession.setText(user.filter_professions);
         txv_match_caste.setText(user.filter_casteString());
         txv_countries.setText(user.filter_countryString());


         if (user.filter_maritalstatus > 1){

             lyt_havechildren.setVisibility(View.VISIBLE);

             if (user.filter_havechildren == 1){
                 txv_match_havechildren.setText(getString(R.string.yes));
             } else if (user.filter_havechildren == 0){
                 txv_match_havechildren.setText(getString(R.string.no));
             }

         }
         else lyt_havechildren.setVisibility(View.GONE);

         // Case only Female
         TextView txv_match_own_apart_title = (TextView)findViewById(R.id.txv_match_own_apart_title);
         TextView txv_match_relocate_title = (TextView)findViewById(R.id.txv_match_relocate_title);

         if (user.user_gender == UserModel.Gender.female){

             if (user.filter_ownapartment ==1){
                 txv_match_own_apart.setText(getString(R.string.yes));
             } else if (user.filter_ownapartment == 0)
                 txv_match_own_apart.setText(getString(R.string.no));

             if (user.filter_relocatable ==1){
                 txv_match_relocate.setText(getString(R.string.yes));
             } else txv_match_relocate.setText(getString(R.string.no));

         }
         else {

             txv_match_own_apart_title.setVisibility(View.GONE);
             txv_match_own_apart.setVisibility(View.GONE);

             txv_match_relocate_title.setVisibility(View.GONE);
             txv_match_relocate.setVisibility(View.GONE);
         }

     }

     public void addImage(String path) {

         _imagePaths.add(path);
         _imageAdapter.setDatas(_imagePaths);
         _imageAdapter.notifyDataSetChanged();

     }

     public void addImages(ArrayList<String> paths) {

         _imagePaths.clear();
         _imagePaths.addAll(paths);
         //_imageAdapter.setDatas(_imagePaths);
         _imageAdapter.setData(_imagePaths, user.user_id);


         if (Commons.g_user.user_id == user.user_id)
            recyclerPhotos.getLayoutParams().height = Commons.GetPixelValueFromDp(_context, 102/*(Constants.WIDTH/9 - 60 - 20 )*/) * min(((_imagePaths.size()) / 3 + 1), 2);

         else
             recyclerPhotos.getLayoutParams().height = Commons.GetPixelValueFromDp(_context, 102/*(Constants.WIDTH/9 - 60 - 20 )*/) * min(((_imagePaths.size() - 1) / 3 + 1), 2);

     }

     public void removeImage(String path) {

         _imagePaths.remove(path);
         _imageAdapter.setDatas(_imagePaths);
         recyclerPhotos.getLayoutParams().height = Commons.GetPixelValueFromDp(_context, 103) * ((_imagePaths.size()) / 3 + 1);
     }


     @OnClick(R.id.imv_photo_profile) void takeProfilePhoto(){

         takePhotoStatus = 5;

         if (!hasPermissions(this, PERMISSIONS)){

             ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
         }else {

             onSelectPhoto();
         }
     }

     public void multiTakePhoto(){

         takePhotoStatus = 10;

         if (!hasPermissions(this, PERMISSIONS)){

             ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
         }else {

             onSelectPhoto();
         }
     }

     public static boolean hasPermissions(Context context, String... permissions) {
         if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
             for (String permission : permissions) {
                 if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                     return false;
                 }
             }
         }
         return true;
     }

     @Override
     public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
         if (requestCode == MY_PEQUEST_CODE
                 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
             //  gps functionality

             onSelectPhoto();
         }
     }

     public void onSelectPhoto(){

        if (takePhotoStatus == 10){

            if (_imagePaths.size() >= MAX_IMAGES) {

                showAlertDialog("You can only select up to 6 photos.");
                return ;
            }
        }

         final String[] items = {getString(R.string.take_photo), getString(R.string.choose_gallery) ,getString(R.string.cancel)};
         final AlertDialog.Builder builder = new AlertDialog.Builder(this);


         builder.setItems(items, new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int item) {

                 if (item == 0){
                     doTakePhoto();

                 }else if (item == 1){

                     if (takePhotoStatus == 5){

                         doTakeGalleryProfile();
                     }
                     else if (takePhotoStatus == 10){

                         doTakeGalleryImages();
                     }

                 }else return;
             }

         });

         AlertDialog alertDialog = builder.create();
         alertDialog.show();
     }

     public void doTakePhoto(){

         Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         if (intent.resolveActivity(getPackageManager()) != null) {
             ContentValues values = new ContentValues(1);
             values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");

             if (takePhotoStatus == 5){

                 _imageCaptureUri_profile = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                 intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri_profile);
             }
             else if (takePhotoStatus == 10){
                 _imageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                 intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
             }

             intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
             startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
         } else {
             Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
         }
     }

     private void doTakeGalleryImages(){

         Intent intent = new Intent(this, SelectImageActivity.class);
         intent.putExtra(Constants.KEY_COUNT, _imagePaths.size());
         startActivityForResult(intent, Constants.PICK_FROM_IMAGES);
     }

     private void doTakeGalleryProfile(){

         Intent intent = new Intent(Intent.ACTION_PICK);
         intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
         startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
     }

     @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
         switch (requestCode) {

             case Constants.CROP_FROM_CAMERA: {

                 if (takePhotoStatus == 5){

                     if (resultCode == RESULT_OK) {

                         try {

                             File saveFile = BitmapUtils.getOutputMediaFile(this);

                             InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                             BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                             Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                             in.close();

                             //set The bitmap data to image View
                             imv_photo.setImageBitmap(bitmap);
                             _photoPath_profile = saveFile.getAbsolutePath();

                         } catch (Exception e) {
                             e.printStackTrace();
                         }

                    if (takePhotoStatus == 5)
                        updatePhoto();
                     }
                 } else if (takePhotoStatus == 10){

                     if (resultCode == RESULT_OK){
                         try {

                             //File outFile = new File(_photoPath);
                             File outFile = BitmapUtils.getOutputMediaFile(this);

                             InputStream in = getContentResolver().openInputStream(Uri.fromFile(outFile));
                             BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                             Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                             in.close();

                             ExifInterface ei = new ExifInterface(outFile.getAbsolutePath());
                             int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                     ExifInterface.ORIENTATION_NORMAL);

                             Bitmap returnedBitmap = bitmap;

                             switch (orientation) {

                                 case ExifInterface.ORIENTATION_ROTATE_90:
                                     returnedBitmap = BitmapUtils.rotateImage(bitmap, 90);
                                     // Free up the memory
                                     bitmap.recycle();
                                     bitmap = null;
                                     break;
                                 case ExifInterface.ORIENTATION_ROTATE_180:
                                     returnedBitmap = BitmapUtils.rotateImage(bitmap, 180);
                                     // Free up the memory
                                     bitmap.recycle();
                                     bitmap = null;
                                     break;
                                 case ExifInterface.ORIENTATION_ROTATE_270:
                                     returnedBitmap = BitmapUtils.rotateImage(bitmap, 270);
                                     // Free up the memory
                                     bitmap.recycle();
                                     bitmap = null;
                                     break;

                                 default:
                                     returnedBitmap = bitmap;
                             }

                             Bitmap w_bmpSizeLimited = Bitmap.createScaledBitmap(returnedBitmap, Constants.PROFILE_IMAGE_SIZE, Constants.PROFILE_IMAGE_SIZE, true);

                             BitmapUtils.saveOutput(outFile, w_bmpSizeLimited);

                             _photoPath = outFile.getAbsolutePath();

                             addImage(_photoPath);

                         } catch (Exception e) {
                             e.printStackTrace();
                         }
                     }
                 }
                 break;
             }

             case Constants.PICK_FROM_IMAGES:

                 if (resultCode == RESULT_OK){
                     final ArrayList<String> paths = data.getStringArrayListExtra(Constants.KEY_IMAGES);
                     //addImages(paths);
                     uploadPhotos(paths);

                 }
                 break;

             case Constants.PICK_FROM_ALBUM:

                 if (resultCode == RESULT_OK) {
                     _imageCaptureUri_profile = data.getData();
                 }

             case Constants.PICK_FROM_CAMERA: {


                 if (takePhotoStatus == 5){

                     try {

                         _photoPath_profile = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri_profile);

                         Intent intent = new Intent("com.android.camera.action.CROP");
                         intent.setDataAndType(_imageCaptureUri_profile, "image/*");

                         intent.putExtra("crop", true);
                         intent.putExtra("scale", true);
                         intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                         intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                        intent.putExtra("aspectX", 1);
                        intent.putExtra("aspectY", 1);
                         intent.putExtra("noFaceDetection", true);
                         //intent.putExtra("return-data", true);
                         intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                         startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 }

                 else if(takePhotoStatus == 10){

                     try {
                         _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                         //beginGrop(_imageCaptureUri ) ;

                         new AsyncTask<Void, Void, Void>() {
                             @Override
                             protected Void doInBackground(Void... params) {

                                 //We send the message here.
                                 //You should also check if the suername is valid here.
                                 try {

                                     selectPhoto();

                                 } catch (Exception e) {

                                 }
                                 return null;
                             }

                             @Override
                             protected void onPostExecute(Void aVoid) {
                                 super.onPostExecute(aVoid);

                                 //addImage(_photoPath);
                                 // add photo  :
                                 ArrayList<String> paths = new ArrayList<>();
                                 paths.add(_photoPath);

                                 uploadPhotos(paths);

                             }

                         }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 }

                 break;
             }
         }
     }


     private void selectPhoto() {

         try {
             Bitmap returnedBitmap = BitmapUtils.loadOrientationAdjustedBitmap(_photoPath);

             Log.d("=======", _photoPath) ;

             File outFile = getOutputMediaFile();

             BitmapUtils.saveOutput(outFile , returnedBitmap);
             returnedBitmap.recycle();
             returnedBitmap = null;

             _photoPath = outFile.getAbsolutePath();
         }    catch (Exception e) {

             e.printStackTrace();
         }
     }

     private void beginCrop(Uri source) {

         File outFile = getOutputMediaFile();
         Uri destination = Uri.fromFile(outFile);
         _photoPath = outFile.getAbsolutePath();
         //Crop.of(source, destination).asSquare().start(this);
     }

     public void deleteAllTempImages() {

         File mediaStorageDir = new File(
                 Environment.getExternalStorageDirectory() + "/android/data/"
                         + this.getPackageName() + "/myprofile");

         if (!mediaStorageDir.exists()) {
             if (!mediaStorageDir.mkdirs()) {
                 return;
             }
         } else {
             String[] children = mediaStorageDir.list();
             for (int i = 0; i < children.length; i++)
             {
                 new File(mediaStorageDir, children[i]).delete();
             }
         }
     }

     public File getOutputMediaFile() {

         File mediaStorageDir = new File(
                 Environment.getExternalStorageDirectory() + "/android/data/"
                         + this.getPackageName() + "/myprofile");

         if (!mediaStorageDir.exists()) {
             if (!mediaStorageDir.mkdirs()) {
                 return null;
             }
         }

         long random = new Date().getTime();

         File mediaFile = new File(mediaStorageDir.getPath() + File.separator
                 + "temp" + random + ".png");

         return mediaFile;
     }

     //update profile photo

     private void updatePhoto(){

         try {

             showKProgress();
             File file = new File(_photoPath_profile);

             Map<String, String> params = new HashMap<>();

             params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
             params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE_IMAGE);

             params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.copyUser().user_id));
             params.put(ReqConst.SESSION, EasyPreference.with(this).getString(PrefConst.PREFKEY_SESSION, ""));
             params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
             String url = ReqConst.SERVER_URL;

             MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                 @Override
                 public void onErrorResponse(VolleyError error) {

                     if (isFinishing()) {
                         closeKProgress();
                         showAlertDialog(getString(R.string.network_error));
                     }
                 }

             }, new Response.Listener<String>() {
                 @Override
                 public void onResponse(String json) {

                     parseUpdatePhoto(json);

                 }

             }, file, ReqConst.PARAM_FILE, params);

             reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
             ShaadiMubarkApplication.getInstance().addToRequestQueue(reqMultiPart, url);

         } catch (Exception e){

             e.printStackTrace();
             closeKProgress();
             showAlertDialog(getString(R.string.network_error));
         }
     }


     private void parseUpdatePhoto(String json){

         closeKProgress();
         try {
             JSONObject response = new JSONObject(json);
             String  result = response.getString(ReqConst.RESULT);
             String result_msg = response.getString(ReqConst.MESSAGE);

             if (result.equals(ReqConst.RES_SUCCESS)){

                 showToast(result_msg);
                 Commons.g_user.copyUser().user_photo = response.getString(ReqConst.RES_USER_PHOTO);
                 Commons.g_user.user_photo = response.getString(ReqConst.RES_USER_PHOTO);

             } else {
                 closeKProgress();
                 showAlertDialog(result_msg);
             }
         } catch (JSONException e) {
             e.printStackTrace();
         }
     }



     // upload photos

     private void uploadPhotos(ArrayList<String>_images){

         showKProgress();
         String url = ReqConst.SERVER_URL;


         // has image
         if (_imagePaths.size() > 0) {

             //File part
             Map<String, File> mFilePartData= new HashMap<>();

             for (int i = 0; i < _images.size(); i++) {

                 String path = _images.get(i);
                 if (path.length() > 0)
                     mFilePartData.put("file[" + i + "]", new File(path));
             }

             //String part
             Map<String, String> mStringPart= new HashMap<>();

             mStringPart.put(ReqConst.MODULE, ReqConst.MODULE_USER);
             mStringPart.put(ReqConst.FUNCTION, ReqConst.FUNC_ADD_IMAGES);
             mStringPart.put(ReqConst.SESSION, EasyPreference.with(getApplication()).getString(PrefConst.PREFKEY_SESSION, ""));

             mStringPart.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
             mStringPart.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

            // CustomMultipartRequest mCustomRequest = new CustomMultipartRequest(Request.Method.POST, this, url, new Response.Listener<JSONObject>() {
             Multipartyrequest mCustomRequest = new Multipartyrequest(Request.Method.POST, this, url, new Response.Listener<JSONObject>() {

                 @Override
                 public void onResponse(JSONObject jsonObject) {
                     parseUploadPhotos(jsonObject.toString());
                 }
             }, new Response.ErrorListener() {
                 @Override
                 public void onErrorResponse(VolleyError volleyError) {
                     showToast(getString(R.string.network_error));
                     closeKProgress();
                 }
             }, mFilePartData, mStringPart);   //, mHeaderPart

             mCustomRequest.setRetryPolicy(new DefaultRetryPolicy(600000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
             ShaadiMubarkApplication.getInstance().addToRequestQueue(mCustomRequest, url);

         }
     }


     private void parseUploadPhotos(String json){

         closeKProgress();

         _user_images.clear();

         try {
             JSONObject response = new JSONObject(json);
             String success = response.getString(ReqConst.RESULT);
             String msg = response.getString(ReqConst.MESSAGE);

             if (success.equals( ReqConst.RES_SUCCESS)){

                 showToast(msg);

                 String[] imageArray = response.getString(ReqConst.RES_USER_IMAGES).split(",");
                 for (int i = 0; i <imageArray.length ; i++){
                     if (imageArray[i].length() > 0)
                         _user_images.add(imageArray[i]);
                 }

                 addImages(_user_images);
                 Commons.g_user.user_images = response.getString(ReqConst.RES_USER_IMAGES);


             } else {

                 if (msg.equals(getString(R.string.session_tampered))){

                     ApiHelper manager = new ApiHelper();
                     manager.logOut(this);
                 }
                 closeKProgress();
             }
         } catch (JSONException e) {
             e.printStackTrace();
         }
     }


     //delete image

     public void deleteImage(final String image){

         showKProgress();

         String url = ReqConst.SERVER_URL;

         StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
             @Override
             public void onResponse(String response) {
                 parseDeleteImage(response);
             }
         }, new Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 closeKProgress();
                 showAlertDialog(getString(R.string.network_error));
             }
         }){

             @Override
             protected Map<String, String> getParams() throws AuthFailureError {

                 Map<String, String> params = new HashMap<>();

                 try {

                     String deleteImageString = Commons.g_user.user_images.replaceAll("," + image, "").replaceAll(image + ",", "").replaceAll(image,"");

                     params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                     params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                     params.put(ReqConst.SESSION, EasyPreference.with(getApplication()).getString(PrefConst.PREFKEY_SESSION, ""));

                     params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                     params.put(ReqConst.PARAM_USER_IMAGES, deleteImageString);
                     params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                 } catch (Exception e){}
                 return params;
             }
         };

         request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
         ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);

     }


     private void parseDeleteImage(String json){

         closeKProgress();
         _user_images.clear();
         try {
             JSONObject response = new JSONObject(json);
             String success = response.getString(ReqConst.RESULT);
             String result_msg = response.getString(ReqConst.MESSAGE);

             if (success.equals( ReqConst.RES_SUCCESS)){

                 UserModel user;

                 showToast(result_msg);

                 JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                 user = new UserModel(jsonUser);

                 Commons.g_user = user;
                 this.user = user.copyUser();

                 String[] imageArray = Commons.g_user.user_images.split(",");
                 for (int i = 0; i <imageArray.length ; i++){
                     if (imageArray[i].length() > 0)
                         _user_images.add(imageArray[i]);
                 }

                 addImages(_user_images);

             } else {

                 closeKProgress();
                 if (result_msg.equals(getString(R.string.session_tampered))) {
                     ApiHelper manager = new ApiHelper();
                     manager.logOut(this);
                 }

                 showAlertDialog(result_msg);
             }
         } catch (JSONException e) {
             e.printStackTrace();
         }
     }




     @OnClick(R.id.imv_chat_profile) void gotoChat(){


         if (Commons.g_user.user_paid < DateUtils.getTimeStamp()){


             new SweetAlertDialog(this,SweetAlertDialog.WARNING_TYPE)
                     .setTitleText(getString(R.string.premium))
                     .setContentText(getString(R.string.membership_req))
                     .setConfirmText(getString(R.string.ok))
                     .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                         @Override
                         public void onClick(final SweetAlertDialog sweetAlertDialog) {

                             sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                             new Handler().postDelayed(new Runnable() {
                                 @Override
                                 public void run() {

                                     Constants.PAGE_TO = Constants.SETTING_PAGE;
                                     startActivity(new Intent(UserProfileActivity.this, MainActivity.class));
                                     finish();

                                     sweetAlertDialog.cancel();
                                     sweetAlertDialog.dismiss();

                                 }
                             }, 1000);

                         }
                     })
                     .setCancelText(getString(R.string.cancel))
                     .showCancelButton(true)
                     .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                         @Override
                         public void onClick(SweetAlertDialog sweetAlertDialog) {
                             sweetAlertDialog.cancel();
                         }
                     }).show();
         }else {

             Constants.PAGE_TO = Constants.PROFILE_PAGE;

             if(Commons.chatRooms.containsKey(user.user_id)) {

                 Intent intent = new Intent(this, ChatActivity.class);
                 intent.putExtra("userId", user.user_id);
                 startActivity(intent);

             } else requestChat();
         }
     }

     private void requestChat(){

         showKProgress();
         String url = ReqConst.SERVER_URL;
         StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
             @Override
             public void onResponse(String response) {
                 parseRequestChat(response);
             }
         }, new Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 closeKProgress();
                 showAlertDialog(getString(R.string.network_error));
             }
         }){

             @Override
             protected Map<String, String> getParams() throws AuthFailureError {

                 Map<String, String> params = new HashMap<>();

                 try {
                     params.put(ReqConst.MODULE, ReqConst.MODULE_CHATS);
                     params.put(ReqConst.FUNCTION, ReqConst.FUNC_SEND_CHAT_REQUEST);
                     params.put(ReqConst.SESSION, EasyPreference.with(_context).getString(PrefConst.PREFKEY_SESSION, ""));

                     params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                     params.put(ReqConst.PARAM_REQUEST_USER, String.valueOf(Commons.g_user.user_id));
                     params.put(ReqConst.PARAM_RECEIVE_USER, String.valueOf(user.user_id));
                     params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                 } catch (Exception e){}
                 return params;
             }
         };

         request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
         ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);

     }

     private void parseRequestChat(String json){

         closeKProgress();
         try {
             JSONObject respond = new JSONObject(json);
             String result_msg = respond.getString(ReqConst.RESULT);
             if (result_msg.equals(ReqConst.RES_SUCCESS)){
                 showToast("A chat notification is sent to the user");
             }
         } catch (JSONException e) {
             e.printStackTrace();
         }
     }

     @OnClick(R.id.imv_favorite_profile) void favorite(){

         setUserFavorite();
     }

     private void setUserFavorite(){

         showKProgress();
         ApiHelper manager = new ApiHelper();
         int status = 0;
         if (user.isFavorite == 0){
             status = 1;
         }
         else {
             status = 0;
         }
         Callback callback = new Callback() {
             @Override
             public void callback(Object object) {

             }

             @Override
             public void callback(final String result, final String message, JSONObject response) {
                 runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         closeKProgress();
                         if(result.equals(ReqConst.RES_SUCCESS)) {
                             if (imv_favorite_profile.isSelected()){
                                 imv_favorite_profile.setSelected(false);

                             } else {
                                 imv_favorite_profile.setSelected(true);
                             }
                         }
                         else {
                             showToast(message);
                         }
                     }
                 });

             }
         };
         manager.setUserFavorite(user, status, callback);
     }


     @OnClick(R.id.imv_personal_edit) void gotoPersonal(){
         Intent intent = new Intent(this, PersonalInfoActivity.class);
         intent.putExtra(Constants.KEY_USER, user);
         startActivity(intent);
         finish();
     }

 /*    @OnClick(R.id.imv_religion_edit) void gotoReligion(){
         Intent intent = new Intent(this, ReligionCasteActivity.class);
         intent.putExtra(Constants.KEY_USER, user);
         startActivity(intent);
         finish();
     }*/

     @OnClick(R.id.imv_prof_edit) void gotoProfessional(){
         Intent intent = new Intent(this, ProfessionalInfoActivity.class);
         intent.putExtra(Constants.KEY_USER, user);
         startActivity(intent);
         finish();
     }
     @OnClick(R.id.imv_match_edit) void gotoMatchReq(){
         Intent intent = new Intent(this, MatchRequirementsActivity.class);
         intent.putExtra(Constants.KEY_USER, user);
         startActivity(intent);
         finish();
     }

     @OnClick(R.id.btn_done) void doneButton(){
         //gotoBack();
         reportUser();
     }


     private void reportUser(){

         Intent intent = new Intent(this, ReportUserActivity.class);
         intent.putExtra(Constants.REPORTED_USER_ID, user.user_id);
         startActivity(intent);
     }


     public int getScreenWidthInDPs(Context context){

         DisplayMetrics dm = new DisplayMetrics();
         WindowManager windowManager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
         windowManager.getDefaultDisplay().getMetrics(dm);
         int widthInDP = Math.round(dm.widthPixels / dm.density);
         return widthInDP;
     }



     @Override
     public void onBackPressed() {
         gotoBack();
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case android.R.id.home:
                 gotoBack();
                 return true;
             default:
                 return super.onOptionsItemSelected(item);
         }
     }

     private void gotoBack(){

         Intent intent = new Intent(this, MainActivity.class);
         startActivity(intent);
         finish();

         //onExit();

     }

     //animation area
     private void setUpToolbar() {
         setSupportActionBar(toolbar_profile);
         if (getSupportActionBar() != null) {
             getSupportActionBar().setDisplayShowTitleEnabled(false);
             getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         }
     }

     private void setUpAmazingAvatar() {

         mAppBarStateChangeListener = new AppBarStateChangeListener() {
             @Override
             public void onStateChanged(AppBarLayout appBarLayout,
                                        AppBarStateChangeListener.State state) {
             }

             @Override
             public void onOffsetChanged(AppBarStateChangeListener.State state, float offset) {
                 translationView(offset);
             }
         };
         mAppBarLayout.addOnOffsetChangedListener(mAppBarStateChangeListener);
     }

     private void translationView(float offset) {

         float xOffset = -(mAvatarPoint[0] - mSpacePoint[0]) * offset;
         float yOffset = -(mAvatarPoint[1] - mSpacePoint[1]) * offset;
         float xTitleOffset = -(mTitleTextViewPoint[0] - mToolbarTextPoint[0]) * offset;
         float yTitleOffset = -(mTitleTextViewPoint[1] - mToolbarTextPoint[1]) * offset;
         int newSize = Utils.convertDpToPixelSize(
                 EXPAND_AVATAR_SIZE_DP - (EXPAND_AVATAR_SIZE_DP - COLLAPSED_AVATAR_SIZE_DP) * offset,
                 this);
         float newTextSize =
                 mTitleTextSize - (mTitleTextSize - mToolbarTextView.getTextSize()) * offset;
         imv_photo.getLayoutParams().width = newSize;
         imv_photo.getLayoutParams().height = newSize;
         imv_photo.setTranslationX(xOffset);
         imv_photo.setTranslationY(yOffset);
         mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, newTextSize);
         mTitleTextView.setTranslationX(xTitleOffset);
         mTitleTextView.setTranslationY(yTitleOffset);
     }

     /**
      * Avatar from TinyFaces (https://github.com/maximedegreve/TinyFaces)
      */


     private void clearAnim() {
         imv_photo.setTranslationX(0);
         imv_photo.setTranslationY(0);
         mTitleTextView.setTextSize(TypedValue.COMPLEX_UNIT_PX, mTitleTextSize);
         mTitleTextView.setTranslationX(0);
         mTitleTextView.setTranslationY(0);
     }

     private void resetPoints() {

         clearAnim();

         //int avatarSize = Utils.convertDpToPixelSize(EXPAND_AVATAR_SIZE_DP, this);
         int avatarSize = Utils.convertDpToPixelSize(_dp, this);
         imv_photo.getLocationOnScreen(mAvatarPoint);
         //mAvatarPoint[0] -= (avatarSize + imv_photo.getWidth()) / 2 ;
         mAvatarPoint[0] -= (int) (avatarSize + imv_photo.getWidth())/2 ;
         mSpace.getLocationOnScreen(mSpacePoint);
         mToolbarTextView.getLocationOnScreen(mToolbarTextPoint);
         mToolbarTextPoint[0] += Utils.convertDpToPixelSize(0, this);
         mTitleTextView.post(new Runnable() {

             @Override
             public void run() {
                 mTitleTextView.getLocationOnScreen(mTitleTextViewPoint);
                 translationView(mAppBarStateChangeListener.getCurrentOffset());
             }
         });
     }

     @Override
     public void onWindowFocusChanged(boolean hasFocus) {
         super.onWindowFocusChanged(hasFocus);
         if (!hasFocus) {
             return;
         }
         resetPoints();
     }


     @Override
     protected void onResume() {
         super.onResume();

         //setUpAmazingAvatar();
     }
 }
