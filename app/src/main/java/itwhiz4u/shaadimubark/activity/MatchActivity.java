package itwhiz4u.shaadimubark.activity;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.listeners.ItemRemovedListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.model.Message;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.TinderCard;

public class MatchActivity extends CommonActivity {

    @BindView(R.id.toolbar_match) Toolbar toolbar_match;
    @BindView(R.id.swipeView)
    public SwipePlaceHolderView mSwipView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar_match);
        TextView txv_match_req = (TextView)findViewById(R.id.toolbar_title);
        txv_match_req.setText(getString(R.string.match_users));
        ImageView imv_back = (ImageView)findViewById(R.id.imv_back);
        imv_back.setVisibility(View.GONE);
        ImageView imv_mark = (ImageView)findViewById(R.id.imv_mark);
        imv_mark.setVisibility(View.GONE);

    }

    @Override
    public void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (Commons.match_users.size() == 0){

            Intent intent = new Intent(MatchActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        mSwipView.disableTouchSwipe();

        mSwipView.addItemRemoveListener(new ItemRemovedListener() {

            @Override
            public void onItemRemoved(int count) {
                if(count == 0){

                    Intent intent = new Intent(MatchActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        mSwipView.getBuilder()
                .setDisplayViewCount(10)
                .setIsUndoEnabled(true)
                .setWidthSwipeDistFactor(4)
                .setHeightSwipeDistFactor(6)
                .setSwipeDecor(new SwipeDecor()
                        .setMarginTop(60)
                        .setViewGravity(Gravity.TOP)
                        .setViewGravity(Gravity.CENTER_HORIZONTAL)
                        .setPaddingTop(15)
                        .setSwipeMaxChangeAngle(2f)
                        .setRelativeScale(0.01f)
                        .setSwipeInMsgLayoutId(R.layout.tinder_swipe_in_msg_view)
                        .setSwipeOutMsgLayoutId(R.layout.tinder_swipe_out_msg_view));

        for (int i = 0; i < Commons.match_users.size(); i++){
            mSwipView.addView(new TinderCard(this, Commons.match_users.get(i)));
        }

        new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    mSwipView.enableTouchSwipe();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @OnClick(R.id.rejectBtn)
    public void onRejectClick(){
        mSwipView.doSwipe(false);
    }

    @OnClick(R.id.acceptBtn)
    public void onAcceptClick(){
        mSwipView.doSwipe(true);
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
