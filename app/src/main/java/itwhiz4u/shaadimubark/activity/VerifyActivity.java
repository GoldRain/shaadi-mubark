package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.UserModel;

public class VerifyActivity extends CommonActivity {

    @BindView(R.id.edt_verify) EditText edt_verify;
    String _verify_code = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        ButterKnife.bind(this);

        loadLayout();
    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_verify.getWindowToken(), 0);
                return false;
            }
        });
    }

    @OnClick(R.id.imv_back) void gotoBack(){

        if (Constants.SIGNUP_STATUS == 5 && Constants.PROFILE_STATUS == 0){ //from sign in , not completed profile

            onExit();

        } else if (Constants.SIGNUP_STATUS == 0){

            onExit();

        }else if (Constants.SIGNUP_STATUS == 100){

            startActivity(new Intent(this , ForgotActivity.class));
            Constants.SIGNUP_STATUS = 0;
            finish();
        }
    }

    @OnClick(R.id.btn_verify) void gotoVerified(){

        _verify_code = edt_verify.getText().toString();


        if (checkValid()){

            processVerify();
        }
    }

    private void processVerify(){


        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseVerify(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_VERIFY);

                    params.put(ReqConst.PARAM_USER_EMAIL, EasyPreference.with(getApplicationContext()).getString(PrefConst.PREFKEY_USEREMAIL, ""));
                    params.put(ReqConst.PARAM_PIN, _verify_code);
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseVerify(String json){

        Log.d("Versify==>", json);

        closeKProgress();
        try {

            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                UserModel user;

                showToast(result_msg);

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);

                Commons.g_user = user;

                JSONArray castes = response.getJSONArray("caste");
                CasteModel.allCastes = new ArrayList<>();
                for (int i = 0; i < castes.length(); i ++){
                    CasteModel.allCastes.add(new CasteModel(castes.getJSONObject(i)));
                }

                //get countries
                JSONArray countries = response.getJSONArray(ReqConst.RES_COUNTRIES);
                CountryModel.allCountry = new ArrayList<>();
                for (int i = 0; i < countries.length(); i++){
                    CountryModel.allCountry.add(new CountryModel(countries.getJSONObject(i)));
                }

                Commons.price = response.getInt("price");

                EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL, user.user_email).save();
                EasyPreference.with(this).addString(PrefConst.PREFKEY_SESSION, response.getString(ReqConst.SESSION)).save();

                Log.d("Session==>", EasyPreference.with(this).getString(PrefConst.PREFKEY_SESSION, ""));

                if (Constants.SIGNUP_STATUS != 100){

                    Intent intent = new Intent(this, PersonalInfoActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    Constants.SIGNUP_STATUS = 0;
                    startActivity(intent);
                    finish();
                }
                 else {

                    startActivity(new Intent(this, ChangePasswordActivity.class));
                    finish();
                }

            } else {
                showAlertDialog(result_msg);
                edt_verify.setText("");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.txv_resend) void gotoResendVerify(){
        resendVerify();
    }

    private void resendVerify(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseResentVerify(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_RESEND_CODE);

                    params.put(ReqConst.PARAM_USER_EMAIL, EasyPreference.with(getApplicationContext()).getString(PrefConst.PREFKEY_USEREMAIL, ""));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                    edt_verify.setText("");

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseResentVerify(String json){

        closeKProgress();
        try {

            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){

                showToast(result_msg);
                edt_verify.setText("");


            } else {
                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkValid(){

        if (_verify_code.length() == 0){

            showAlertDialog(getString(R.string.input_verify_code));
            return  false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
