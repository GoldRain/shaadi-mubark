package itwhiz4u.shaadimubark.activity;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.adapter.ImagePreviewAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Constants;

public class ImagePreviewActivity extends CommonActivity implements View.OnClickListener {

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    TextView ui_txvImageNo;

    ArrayList<String> _imagePaths = new ArrayList<>();
    int _position = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        _imagePaths = getIntent().getStringArrayListExtra(Constants.KEY_IMAGEPATH);
        _position = getIntent().getIntExtra(Constants.KEY_POSITION,0);

        loadLayout();
    }

    private void loadLayout(){

        ui_txvImageNo = (TextView)findViewById(R.id.txv_timeline_no);


        ImageView imvBack = (ImageView)findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        ui_viewPager = (ViewPager)findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(this);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);
        ui_viewPager.setCurrentItem(_position);

        if (_imagePaths.size() != 1)
            ui_txvImageNo.setText((_position + 1) + " / " + _imagePaths.size());
        else ui_txvImageNo.setVisibility(View.GONE);

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.imv_back:
                finish();
                break;
        }
    }
}
