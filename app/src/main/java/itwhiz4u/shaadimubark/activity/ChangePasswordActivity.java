package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.Backend.ApiHelper;

public class ChangePasswordActivity extends CommonActivity {

    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.edt_confirm_pass) EditText edt_confirm_pass;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);
        loadLayout();
    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_password.getWindowToken(), 0);
                return false;
            }
        });
    }

    @OnClick(R.id.btn_reset_password) void changePassword(){

        if (!checkValid())
            return;

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdate(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPDATE_PROFILE);
                    params.put(ReqConst.SESSION, EasyPreference.with(_context).getString(PrefConst.PREFKEY_SESSION, ""));
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_USER_PASSWORD, String.valueOf(edt_password.getText().toString()));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpdate(String json){

        Log.d("Json-==>", json);

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String success = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (success.equals( ReqConst.RES_SUCCESS)){


                showToast(result_msg);
                EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, edt_password.getText().toString()).save();


                Intent intent = new Intent(this, SignInActivity.class);
                startActivity(intent);
                finish();


            } else {

                if (result_msg.equals(getString(R.string.session_tampered)))
                    (new ApiHelper()).logOut(this);

                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkValid(){

        if (edt_password.getText().toString().length() == 0){

            showAlertDialog(getString(R.string.input_password));
            return false;
        }
        else if (!edt_password.getText().toString().equals(edt_confirm_pass.getText().toString())){
            showAlertDialog(getString(R.string.check_confirm_password));
            return false;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
