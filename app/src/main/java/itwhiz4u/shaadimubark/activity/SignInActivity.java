package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.JsonObject;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.CasteModel;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.CountryModel;
import itwhiz4u.shaadimubark.model.NotificationModel;
import itwhiz4u.shaadimubark.model.PriceModel;
import itwhiz4u.shaadimubark.model.UserModel;

import static itwhiz4u.shaadimubark.commons.Constants.PROFILE_STATUS;

public class SignInActivity extends CommonActivity {

    @BindView(R.id.edt_user_name) EditText edt_user_name;
    @BindView(R.id.edt_password) EditText edt_password;
    String _user_name = "", _user_password = "";

    UserModel user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        ButterKnife.bind(this);
        loadLayout();

        Constants.PAGE_TO = "";
    }

    boolean _isFromLogout = false ;
    private void initValue() {

        Intent intent = getIntent();

        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);

        } catch (Exception e){
        }
    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_user_name.getWindowToken(), 0);
                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL,"").save();
            EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, "").save();

            edt_user_name.setText("");
            edt_password.setText("");

        } else {

            String _email = EasyPreference.with(this).getString( PrefConst.PREFKEY_USEREMAIL, "");
            String _password = EasyPreference.with(this).getString(PrefConst.PREFKEY_USERPWD, "");

            edt_user_name.setText(_email);
            edt_password.setText(_password);

            if ( _email.length()>0 && _password.length() > 0 ) {

                _user_name = edt_user_name.getText().toString();
                _user_password = edt_password.getText().toString();

                progressSignin();
            }
        }
    }

    private void progressSignin(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSignIn(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                params.put(ReqConst.MODULE, ReqConst.MODULE_USER);
                params.put(ReqConst.FUNCTION, ReqConst.FUNC_SIGNIN);

                params.put(ReqConst.PARAM_USER_EMAIL, _user_name);
                params.put(ReqConst.PARAM_USER_PASSWORD, _user_password);
                params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

                if (Prefs.getString(Constants.TOKEN, "").length() > 0)
                    params.put(ReqConst.PARAM_FCMTOKEN, Prefs.getString(Constants.TOKEN, ""));
                else
                    params.put(ReqConst.PARAM_FCMTOKEN, "123456");

                Log.d("FCM_token==>", Prefs.getString(Constants.TOKEN, ""));

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseSignIn(String json){

        Log.d("login_-", json);

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String result = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                //get caste
                JSONArray castes = response.getJSONArray(ReqConst.RES_CASTE);
                CasteModel.allCastes = new ArrayList<>();
                for (int i = 0; i < castes.length(); i ++){
                    CasteModel.allCastes.add(new CasteModel(castes.getJSONObject(i)));
                }

               /* //get prices
                JSONArray prices = response.getJSONArray(ReqConst.RES_PRICE);
                PriceModel.allPrices = new ArrayList<>();
                for (int i = 0; i < prices.length(); i++){
                    PriceModel.allPrices.add(new PriceModel(prices.getJSONObject(i)));
                }*/

                //get countries
                JSONArray countries = response.getJSONArray(ReqConst.RES_COUNTRIES);
                CountryModel.allCountry = new ArrayList<>();
                for (int i = 0; i < countries.length(); i++){
                    CountryModel.allCountry.add(new CountryModel(countries.getJSONObject(i)));
                }

                //get session
                EasyPreference.with(this).addString(PrefConst.PREFKEY_SESSION, response.getString(ReqConst.SESSION)).save();
                Prefs.putString(PrefConst.PREFKEY_SESSION, response.getString(ReqConst.SESSION));

                JSONObject jsonUser = response.getJSONObject(ReqConst.RES_USER);
                user = new UserModel(jsonUser);

                Commons.g_user = user;

                Commons.all_users = new ArrayList<>();
                Commons.match_users = new ArrayList<>();
                Commons.favorite_users = new ArrayList<>();

                if (Commons.g_user.user_status == 10){

                    //get all users
                   JSONArray jsonAllUsers = response.getJSONArray(ReqConst.RES_USERS);
                   Commons.all_users = new ArrayList<>();
                    for (int i = 0; i < jsonAllUsers.length(); i++){

                        Commons.all_users.add(new UserModel(jsonAllUsers.getJSONObject(i)));
                    }

                    //get favorite users
                    JSONArray jsonFavUsers = response.getJSONArray(ReqConst.RES_FAVORITE_USERS);
                    Commons.favorite_users = new ArrayList<>();
                    for (int i = 0; i < jsonFavUsers.length(); i++){

                        Commons.favorite_users.add(new UserModel(jsonFavUsers.getJSONObject(i)));
                    }

                    //get match users
                    JSONArray jsonMatchUsers = response.getJSONArray(ReqConst.RES_MATCH_USERS);
                    Commons.match_users = new ArrayList<>();
                    for (int i = 0; i < jsonMatchUsers.length(); i++){

                        Commons.match_users.add(new UserModel(jsonMatchUsers.getJSONObject(i)));
                    }

                    //get chats
                    JSONArray jsonChats = response.getJSONArray(ReqConst.RES_CHATS);
                    Commons.chatRooms = new HashMap<>();
                    for (int i = 0; i < jsonChats.length(); i++){
                        ChatModel chatRoom = new ChatModel(jsonChats.getJSONObject(i));
                        if (chatRoom.chat_user.user_id == Commons.g_user.user_id) continue;
                        Commons.chatRooms.put(chatRoom.chat_user.user_id, chatRoom);
                    }

                    JSONArray jsonNoti = response.getJSONArray(ReqConst.RES_NOTIFICATIONS);
                    Commons.myNotifications = new ArrayList<>();
                    Commons.notiCount = 0;
                    for (int i = 0; i < jsonNoti.length(); i++){

                        Commons.myNotifications.add(new NotificationModel(jsonNoti.getJSONObject(i)));

                        NotificationModel notification = new NotificationModel(jsonNoti.getJSONObject(i));
                        if (notification.notification_read != 1){
                            Commons.notiCount++;
                        }
                    }

                }

                EasyPreference.with(this).addString(PrefConst.PREFKEY_USEREMAIL,_user_name).save();
                EasyPreference.with(this).addString(PrefConst.PREFKEY_USERPWD, _user_password).save();

                PROFILE_STATUS = user.user_status;
                Constants.SIGNUP_STATUS = 5; // from sign in;

                if (PROFILE_STATUS == 0){

                    Intent intent = new Intent(this, VerifyActivity.class);
                    startActivity(intent);
                    finish();

                } else if (PROFILE_STATUS == 1){ //personal

                    Intent intent = new Intent(this, PersonalInfoActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();
                }
                else if (PROFILE_STATUS == 2){ // religion

                    Intent intent = new Intent(this, ReligionCasteActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();
                }
                else if (PROFILE_STATUS == 3){ //professional

                    Intent intent = new Intent(this, ProfessionalInfoActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();
                }
                else if (PROFILE_STATUS == 4){ //matchReq

                    Intent intent = new Intent(this, MatchRequirementsActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();
                }
                else if (PROFILE_STATUS == 10){

                    Intent intent = new Intent(this, MatchActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();

                    Commons.loginTime = (new Date()).getTime();

                    /*Intent intent = new Intent(this, UserProfileActivity.class);
                    intent.putExtra(Constants.KEY_USER, Commons.g_user.copyUser());
                    startActivity(intent);
                    finish();*/

                }
                else if (PROFILE_STATUS == 100){
                    showAlertDialog(getString(R.string.account_suspended));
                }

                else if (PROFILE_STATUS == 101) showAlertDialog(getString(R.string.account_deleted));


            } else {
                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @OnClick(R.id.btn_login) void gotoLogin(){

        _user_name = edt_user_name.getText().toString();
        _user_password = edt_password.getText().toString();

        if (checkValid())
            progressSignin();
    }

    @OnClick(R.id.txv_forgot) void gotoForgot(){
        startActivity(new Intent(this, ForgotActivity.class));
        finish();
    }

    @OnClick(R.id.txv_signup) void gotoSignUp(){

        startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }

    private boolean checkValid(){
        if (_user_name.length() == 0){
            showAlertDialog(getString(R.string.input_your_email));
            return false;
        } else if (_user_password.length() == 0){
            showAlertDialog(getString(R.string.input_password));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
