package itwhiz4u.shaadimubark.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.ReqConst;

public class TermsPolicyActivity extends CommonActivity{

    @BindView(R.id.toolbar_title) TextView txv_title;
    @BindView(R.id.imv_mark) ImageView imv_mark;

    @BindView(R.id.radioTermPolicy) RadioGroup radioTermPolicy;
    @BindView(R.id.radio_terms) RadioButton radio_terms;
    @BindView(R.id.radio_policy) RadioButton radio_policy;

    @BindView(R.id.web_content) WebView web_content;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_policy);

        ButterKnife.bind(this);
        loadLayout();

        Toolbar toolbar_forgot = (Toolbar)findViewById(R.id.toolbar_terms);
        setSupportActionBar(toolbar_forgot);

        txv_title.setText(getString(R.string.term_con_t));
        imv_mark.setVisibility(View.GONE);
    }

    private void loadLayout(){

        web_content.loadUrl(ReqConst.TERMS_URL);

        radioTermPolicy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (radio_terms.isChecked()){

                    web_content.loadUrl(ReqConst.TERMS_URL);

                } else if (radio_policy.isChecked()){

                    web_content.loadUrl(ReqConst.PRIVACY_URL);
                }
            }
        });

    }

    @OnClick(R.id.imv_back) void gotoSignUp(){

        //startActivity(new Intent(this, SignUpActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoSignUp();
    }
}
