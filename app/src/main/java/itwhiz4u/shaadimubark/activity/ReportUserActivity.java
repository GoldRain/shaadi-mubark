package itwhiz4u.shaadimubark.activity;

import android.content.Context;
import android.support.constraint.solver.widgets.ConstraintAnchor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.Message;

public class ReportUserActivity extends CommonActivity {

    @BindView(R.id.edt_report) EditText edt_report;

    int reportedUser = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_user);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_report);
        setSupportActionBar(toolbar);

        TextView title = (TextView)findViewById(R.id.toolbar_title);
        title.setText(getString(R.string.report_user));

        ((ImageView)findViewById(R.id.imv_mark)).setVisibility(View.GONE);
        //imv_mark.setVisibility(View.GONE);

        reportedUser = getIntent().getIntExtra(Constants.REPORTED_USER_ID, 0);
        loadLayout();
    }

    private void loadLayout() {

        LinearLayout lytContainer = (LinearLayout) findViewById(R.id.lyt_container);
        lytContainer.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_report.getWindowToken(), 0);
                return false;
            }
        });

    }

    @OnClick(R.id.btn_report) void gotoReportUser(){
        if (checkValid()){
            reportUser();
        }
    }

    private void reportUser(){

        showKProgress();
        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseReportUser(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeKProgress();
                showAlertDialog(getString(R.string.network_error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_REPORT);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_REPORT_USER);
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_REPORTED_USER, String.valueOf(reportedUser));
                    params.put(ReqConst.PARAM_REPORT_CONTENT, edt_report.getText().toString());
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseReportUser(String json){

        closeKProgress();
        try {
            JSONObject respond = new JSONObject(json);
            String result_msg = respond.getString(ReqConst.RESULT);
            if (result_msg.equals(ReqConst.RES_SUCCESS)){
                showToast(result_msg);
                finish();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.imv_back) void gotoBack(){
        finish();
    }

    private boolean checkValid(){

        if (edt_report.getText().toString().length() == 0){
            showAlertDialog(getString(R.string.input_report_reason));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }

}
