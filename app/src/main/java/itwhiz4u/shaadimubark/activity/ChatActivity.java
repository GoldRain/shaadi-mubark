package itwhiz4u.shaadimubark.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.database.DatabaseReference;
import com.iamhabib.easy_preference.EasyPreference;
import com.sjl.foreground.Foreground;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.utils.DateFormatter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.base.BaseActivity;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.Message;
import itwhiz4u.shaadimubark.model.MessagesenderModel;
import itwhiz4u.shaadimubark.model.NotificationModel;
import itwhiz4u.shaadimubark.utils.BitmapUtils;
import itwhiz4u.shaadimubark.utils.Callback.Callback;
import itwhiz4u.shaadimubark.utils.ChatManager;
import itwhiz4u.shaadimubark.utils.DateUtils;
import itwhiz4u.shaadimubark.utils.MultiPartRequest;

public class ChatActivity extends BaseActivity
        implements MessageInput.InputListener,
        MessageInput.AttachmentsListener,
        DateFormatter.Formatter, MessagesListAdapter.OnLoadMoreListener,
        Foreground.Listener  {

    @BindView(R.id.toolbar_chatting) Toolbar toolbar_chatting;
    @BindView(R.id.toolbar_title) TextView txv_title;
    @BindView(R.id.imv_mark) ImageView imv_mark;
    @BindView(R.id.imv_back) ImageView imv_back;

    protected ImageLoader mImageLoader;
    protected MessagesListAdapter<Message> messagesAdapter;

    Uri _imageCaptureUri;
    String _photoPath = "";
    ArrayList<String> enlargeUrl = new ArrayList<>();

    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE,  Manifest.permission.WRITE_EXTERNAL_STORAGE,  Manifest.permission.CAMERA};
    public static int MY_PEQUEST_CODE = 123;

    String text = "TEXT", image = "IMAGE";

    public static void open(Context context) {
        context.startActivity(new Intent(context, ChatActivity.class));
    }

    private MessagesList messagesList;
    private static DatabaseReference reference;
    private ChatModel chatRoom;
    private Foreground.Binding listenerBinding;


    private Callback callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar_chatting);

        int userId = (int)getIntent().getIntExtra("userId", 0);
        chatRoom = Commons.chatRooms.get(userId);

        txv_title.setText(chatRoom.chat_user.getName());
        imv_mark.setVisibility(View.GONE);

        messagesList = (MessagesList) findViewById(R.id.messagesList);
        //initAdapter();

        MessageInput input = (MessageInput) findViewById(R.id.input);
        input.setInputListener(this);
        input.setAttachmentsListener(this);

        listenerBinding = Foreground.get(getApplication()).addListener(this);

        loadLayout();

    }

    private void loadLayout(){

        mImageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                Picasso.with(_context).load(url).placeholder(R.drawable.placeholder).into(imageView);
            }
        };

        initAdapter();
    }

    private void initAdapter() {

        MessageHolders holdersConfig = new MessageHolders()
                .setIncomingImageLayout(R.layout.incoming_avarta);

        messagesAdapter = new MessagesListAdapter<>(String.valueOf(Commons.g_user.user_id),mImageLoader);
        //super.messagesAdapter.enableSelectionMode(this);
        messagesAdapter.setLoadMoreListener(this);
        messagesAdapter.setDateHeadersFormatter(this);

        ChatManager manager = new ChatManager();
        manager.setUserOnlineListener(chatRoom.chat_user.user_id, new Callback() {
            @Override
            public void callback(final Object object) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        chatRoom.chat_user.user_online = (Boolean) object;
                    }
                });
            }

            @Override
            public void callback(String result, String message, JSONObject response) {

            }
        });

        if(Commons.messages.containsKey(chatRoom.chat_user.user_id)) {

            List<Message> messages = Commons.messages.get(chatRoom.chat_user.user_id);

            //revers
            ArrayList<Message> reversedMessages = new ArrayList<Message>();
            for(Message message: messages) {
                reversedMessages.add(0,message);
            }
            messagesAdapter.addToEnd(reversedMessages, false);
        }

        messagesList.setAdapter(messagesAdapter);
        callback = new Callback() {
            @Override
            public void callback(Object object) {
                final Message message = (Message) object;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        messagesAdapter.addToStart(message, true);
                    }
                });
            }

            public void callback(String result, String message, JSONObject json) {

            }
        };
        chatRoom.delegate = callback;

        messagesAdapter.setOnMessageViewClickListener(new MessagesListAdapter.OnMessageViewClickListener<Message>() {
            @Override
            public void onMessageViewClick(View view, Message message) {

                if(message.getMessagetype().equals("photo")){

                    enlargeUrl = new ArrayList<>();

                    enlargeUrl.add(0, message.getImageUrl());

                    Intent intent = new Intent(ChatActivity.this, ImagePreviewActivity.class);
                    intent.putExtra(Constants.KEY_IMAGEPATH, enlargeUrl);
                    intent.putExtra(Constants.KEY_POSITION, 0);
                    startActivity(intent);
                }
            }
        });

    }

    public void setPhoto(){

        final String[] items = {getString(R.string.take_photo), getString(R.string.choose_gallery) ,getString(R.string.cancel)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }


    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }
    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == MY_PEQUEST_CODE
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //  gps functionality

            setPhoto();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK) {

                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        //imv_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    attachImage();
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK) {
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA: {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    //intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    //intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            }

        }

    }

    private void attachImage(){

        try {

            showKProgress();
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.MODULE, ReqConst.MODULE_IMAGE_UPLOADER);
            params.put(ReqConst.FUNCTION, ReqConst.FUNC_UPLOAD_IMAGE);
            params.put(ReqConst.SESSION, EasyPreference.with(_context).getString(PrefConst.PREFKEY_SESSION, ""));
            params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());

            String url = ReqConst.SERVER_URL;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    if (isFinishing()) {
                        closeKProgress();
                        showAlertDialog(getString(R.string.network_error));
                    }
                }

            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String json) {

                    parseUpdatePhoto(json);

                }

            }, file, ReqConst.PARAM_FILE_KEY, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            ShaadiMubarkApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e){

            e.printStackTrace();
            closeKProgress();
            showAlertDialog(getString(R.string.network_error));
        }
    }

    private void parseUpdatePhoto(String json){

        closeKProgress();
        try {
            JSONObject response = new JSONObject(json);
            String  result = response.getString(ReqConst.RESULT);
            String result_msg = response.getString(ReqConst.MESSAGE);

            if (result.equals(ReqConst.RES_SUCCESS)){

                String img_url = response.getString(ReqConst.RES_IMAGE_URL);

                MessagesenderModel message =  new MessagesenderModel(
                        DateUtils.getcurrentgmttime(),
                        0,
                        String.valueOf(Commons.g_user.user_id),
                        img_url,
                        "photo" );

                ChatManager.sendMessage(message, chatRoom.chat_room);

                if (!chatRoom.chat_user.user_online){
                    sendChatNotification(message);
                }

            } else {
                closeKProgress();
                showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAddAttachments() {
      //  messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);
        //messagesAdapter.addToStart(message, true);;

        if (!hasPermissions(this, PERMISSIONS)){

            ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PEQUEST_CODE);
        }else {
            setPhoto();
        }
    }


    @Override
    public boolean onSubmit(CharSequence input) {


        MessagesenderModel message = new MessagesenderModel(
                DateUtils.getcurrentgmttime(),
                0,
                String.valueOf(Commons.g_user.user_id),
                input.toString(),
        "text" );
        ChatManager.sendMessage(message, chatRoom.chat_room);
        if (!chatRoom.chat_user.user_online){
            sendChatNotification(message);
        }
        return true;
    }

    private void sendChatNotification(final MessagesenderModel message){

        String url = ReqConst.SERVER_URL;
        final StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSendChatNotification(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_NOTIFICATIONS);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_SEND_CHAT_NOTI);
                    params.put(ReqConst.SESSION, EasyPreference.with(_context).getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_NOTIFICATION_TYPE, String.valueOf(NotificationModel.NOTIFICATION_TYPE_CHAT_MESSAGE));
                    params.put(ReqConst.PARAM_NOTIFICATION_SENDER, String.valueOf(Commons.g_user.user_id));
                    params.put(ReqConst.PARAM_NOTIFICATION_RECEIVER, String.valueOf(chatRoom.chat_user.user_id));
                    params.put(ReqConst.PARAM_NOTIFICATION_READ, String.valueOf(0));
                    params.put(ReqConst.PARAM_NOTIFICATION_ITEM, String.valueOf(0));
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                    if(message.type.equals("text")) {
                        params.put(ReqConst.PARAM_NOTIFICATION_MESSAGE, message.text);
                    } else {
                        params.put(ReqConst.PARAM_NOTIFICATION_MESSAGE, Commons.g_user.getName() + getString(R.string.shared_photo));
                    }
                    params.put(ReqConst.PARAM_NOTIFICATION_TITLE, getString(R.string.message_from) + Commons.g_user.getName());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseSendChatNotification(String json){

        try {
            JSONObject respond = new JSONObject(json);
            String result_msg = respond.getString(ReqConst.RESULT);
            if (result_msg.equals(ReqConst.RES_SUCCESS)){

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String format(Date date) {
        if (DateFormatter.isToday(date)) {
            return "Today";
        } else if (DateFormatter.isYesterday(date)) {
            return "Yesterday";
        } else {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR);
        }
    }



    @Override
    public void onLoadMore(int page, int totalItemsCount) {}

    @OnClick(R.id.imv_back) void back(){

        if (Constants.PAGE_TO.equals(Constants.CHATTING_PAGE)){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        back();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        chatRoom.delegate = null;
        Commons.g_chatActivity = null;
    }

    @Override
    public void onBecameForeground() {

        chatRoom.delegate = callback;
        ChatManager.setUserOnline();
    }

    @Override
    public void onBecameBackground() {

        chatRoom.delegate = null;
        ChatManager.setUserOffline();
    }

}
