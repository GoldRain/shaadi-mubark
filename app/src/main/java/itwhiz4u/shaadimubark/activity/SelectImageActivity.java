package itwhiz4u.shaadimubark.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.adapter.ImageGalleryAdapter;
import itwhiz4u.shaadimubark.base.CommonActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.utils.BitmapUtils;

public class SelectImageActivity extends CommonActivity implements View.OnClickListener{

    private TextView ui_txvConfirm;
    private ImageView ui_imvBack;
    private GridView ui_gridView;
    private ImageGalleryAdapter _imageAdapter;
    private ArrayList<String> _imageUrls = new ArrayList<>();
    private ArrayList<String> _selectedImages = new ArrayList<>();
    public int _existCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_image);

        _existCount = getIntent().getIntExtra(Constants.KEY_COUNT, 0);

        loadLayout();
    }

    private void loadLayout(){

        ui_txvConfirm = (TextView)findViewById(R.id.txv_confirm);
        ui_txvConfirm.setOnClickListener(this);

        ui_imvBack = (ImageView)findViewById(R.id.imv_back);
        ui_imvBack.setOnClickListener(this);

        ui_gridView = (GridView) findViewById(R.id.grid_image);

        loadImages();
    }

    public void loadImages() {

        if (_imageUrls.size() == 0) {

            final String[] columns = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID};

            Cursor imagecursor = managedQuery(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null,
                    null, MediaStore.Images.Media.DATE_TAKEN + " DESC");

            for (int i = 0; i < imagecursor.getCount(); i++) {
                imagecursor.moveToPosition(i);
                int dataColumnIndex = imagecursor.getColumnIndex(MediaStore.Images.Media.DATA);
                _imageUrls.add(imagecursor.getString(dataColumnIndex));

            }
        }

        _imageAdapter = new ImageGalleryAdapter(this, _imageUrls);
        ui_gridView.setAdapter(_imageAdapter);

    }


    public void displayCount() {

        int count = _imageAdapter.getCheckedItems().size();

        if (count == 0)
            ui_txvConfirm.setText("Confirm");
        else
            ui_txvConfirm.setText("Confirm" + "(" + count + ")");

    }


    public void onConfirm() {

        _selectedImages.clear();

        if (_imageAdapter.getCheckedItems().size() == 0) return;

        for (String path : _imageAdapter.getCheckedItems()) {

            String filename = Commons.fileNameWithoutExtFromUrl(path) + ".png";

            Bitmap w_bmpGallery = BitmapUtils.loadOrientationAdjustedBitmap(path);

            String w_strLimitedImageFilePath = BitmapUtils.getUploadImageFilePath(w_bmpGallery, filename);

            if (w_strLimitedImageFilePath != null) {
                path = w_strLimitedImageFilePath;
            }

            _selectedImages.add(path);

        }

        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_IMAGES, _selectedImages);
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
            case R.id.txv_confirm:
                onConfirm();
                break;
        }
    }
}
