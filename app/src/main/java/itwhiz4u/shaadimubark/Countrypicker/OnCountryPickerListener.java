package itwhiz4u.shaadimubark.Countrypicker;


import itwhiz4u.shaadimubark.model.CountryModel;

public interface OnCountryPickerListener {
  void onSelectCountry(CountryModel country);
}
