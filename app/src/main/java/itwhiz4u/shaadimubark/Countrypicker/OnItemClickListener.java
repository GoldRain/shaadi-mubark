package itwhiz4u.shaadimubark.Countrypicker;

import itwhiz4u.shaadimubark.model.CountryModel;

public interface OnItemClickListener {
  void onItemClicked(CountryModel country);
}