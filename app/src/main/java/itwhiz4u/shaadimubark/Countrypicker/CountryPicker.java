package itwhiz4u.shaadimubark.Countrypicker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.telephony.TelephonyManager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.model.CountryModel;

public class CountryPicker
    implements CountryPickerDialog.CountryPickerDialogInteractionListener {

  // region Countries

  // endregion

  // region Variables
  public static final int SORT_BY_NONE = 0;
  public static final int SORT_BY_NAME = 1;
  public static final int SORT_BY_ISO = 2;
  public static final int SORT_BY_DIAL_CODE = 3;
  private static final String COUNTRY_TAG = "COUNTRY_PICKER";

  private Context context;
  private int sortBy = SORT_BY_NONE;
  private OnCountryPickerListener onCountryPickerListener;
  private boolean canSearch = true;

  private ArrayList<CountryModel> countries = new ArrayList<>();
  // endregion

  // region Constructors
  private CountryPicker() {
  }

  CountryPicker(Builder builder) {
    sortBy = builder.sortBy;
    if (builder.onCountryPickerListener != null) {
      onCountryPickerListener = builder.onCountryPickerListener;
    }
    context = builder.context;
    canSearch = builder.canSearch;
    countries = new ArrayList<>(CountryModel.allCountry);
    sortCountries(countries);
  }
  // endregion

  // region Listeners
  @Override public void sortCountries(@NonNull List<CountryModel> countries) {
    switch (sortBy) {
      case SORT_BY_NAME:
        Collections.sort(countries, new Comparator<CountryModel>() {
          @Override
          public int compare(CountryModel CountryModel1, CountryModel CountryModel2) {
            return CountryModel1.country_string().trim().compareToIgnoreCase(CountryModel2.country_string().trim());
          }
        });
      case SORT_BY_ISO:
        Collections.sort(countries, new Comparator<CountryModel>() {
          @Override
          public int compare(CountryModel CountryModel1, CountryModel CountryModel2) {
            return CountryModel1.code.trim().compareToIgnoreCase(CountryModel2.code.trim());
          }
        });
      case SORT_BY_DIAL_CODE:
        Collections.sort(countries, new Comparator<CountryModel>() {
          @Override
          public int compare(CountryModel CountryModel1, CountryModel CountryModel2) {
            return CountryModel1.getDialCode().trim().compareToIgnoreCase(CountryModel2.getDialCode().trim());
          }
        });
    }
  }

  @Override public List<CountryModel> getAllCountries() {
    return countries;
  }

  @Override public boolean canSearch() {
    return canSearch;
  }

  // endregion

  // region Utility Methods
  public void showDialog(@NonNull FragmentManager supportFragmentManager) {
    if (countries == null || countries.isEmpty()) {
      throw new IllegalArgumentException(context.getString(R.string.error_no_countries_found));
    } else {
      CountryPickerDialog countryPickerDialog = CountryPickerDialog.newInstance();
      if (onCountryPickerListener != null) {
        countryPickerDialog.setCountryPickerListener(onCountryPickerListener);
      }
      countryPickerDialog.setDialogInteractionListener(this);
      countryPickerDialog.show(supportFragmentManager, COUNTRY_TAG);
    }
  }
  public void setCountries(@NonNull List<CountryModel> countries) {
    this.countries.clear();
    this.countries.addAll(countries);
    sortCountries(this.countries);
  }

 /* public CountryModel getCountryFromSIM() {
    TelephonyManager telephonyManager =
        (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
    if (telephonyManager != null
        && telephonyManager.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
      return getCountryByISO(telephonyManager.getSimCountryIso());
    }
    return null;
  }

  public CountryModel getCountryByLocale(@NonNull Locale locale) {
    String countryIsoCode = locale.getISO3Country().substring(0, 2).toLowerCase();
    return getCountryByISO(countryIsoCode);
  }

  public CountryModel getCountryByName(@NonNull String countryName) {
    countryName = countryName.toUpperCase();
    CountryModel country = new CountryModel();
    country.country_string();
    int i = Arrays.binarySearch(CountryModel.allCountry.toArray(), country, new NameComparator());
    if (i < 0) {
      return null;
    } else {
      return CountryModel.allCountry.get(i);
    }
  }*/

/*  public CountryModel getCountryByISO(@NonNull String countryIsoCode) {
    countryIsoCode = countryIsoCode.toUpperCase();
    CountryModel country = new CountryModel();
    country.code = countryIsoCode;
    int i = Arrays.binarySearch(CountryModel.allCountry.toArray(), country, new ISOCodeComparator());
    if (i < 0) {
      return null;
    } else {
      return CountryModel.allCountry.get(i);
    }
  }*/
  // endregion

  // region Builder
  public static class Builder {
    private Context context;
    private int sortBy = SORT_BY_NONE;
    private boolean canSearch = true;
    private OnCountryPickerListener onCountryPickerListener;

    public Builder with(@NonNull Context context) {
      this.context = context;
      return this;
    }

    public Builder sortBy(@NonNull int sortBy) {
      this.sortBy = sortBy;
      return this;
    }

    public Builder listener(@NonNull OnCountryPickerListener onCountryPickerListener) {
      this.onCountryPickerListener = onCountryPickerListener;
      return this;
    }

    public Builder canSearch(@NonNull boolean canSearch) {
      this.canSearch = canSearch;
      return this;
    }

    public CountryPicker build() {
      return new CountryPicker(this);
    }
  }
  // endregion

  // region Comparators
  public static class ISOCodeComparator implements Comparator<CountryModel> {
    @Override
    public int compare(CountryModel country, CountryModel nextCountry) {
      return country.code.compareTo(nextCountry.code);
    }
  }


  public static class NameComparator implements Comparator<CountryModel> {
    @Override
    public int compare(CountryModel country, CountryModel nextCountry) {
      return country.country_string().compareTo(nextCountry.country_string());
    }
  }
  // endregion
}
