package itwhiz4u.shaadimubark.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.iamhabib.easy_preference.EasyPreference;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.sjl.foreground.Foreground;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.ChatActivity;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.activity.SignInActivity;
import itwhiz4u.shaadimubark.activity.UserProfileActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.model.UserModel;
import itwhiz4u.shaadimubark.utils.ChatManager;


public abstract class BaseActivity extends AppCompatActivity implements Handler.Callback{

    Activity mCurrentActivity = null;

    public ShaadiMubarkApplication application;
    public Context _context = null;

    public Handler _handler = null;

    private ProgressDialog _progressDlg;
    private Vibrator _vibrator;

    private KProgressHUD kProgressHUD;
    MyReceiver receiver;
    private LocalBroadcastManager lbm;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        application = (ShaadiMubarkApplication)getApplication();
        _context = this;

        _vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        _handler = new Handler(this);

        lbm = LocalBroadcastManager.getInstance(this);
        initReceiver();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onDestroy() {

        closeProgress();

        try {
            if (_vibrator != null)
                _vibrator.cancel();
        } catch (Exception e) {
        }
        _vibrator = null;


        super.onDestroy();
    }

    public boolean _isEndFlag = false;

    public static final int BACK_TWO_CLICK_DELAY_TIME = 3000; // ms

    public Runnable _exitRunner = new Runnable() {

        @Override
        public void run() {

            _isEndFlag = false;
        }
    };

    public void onExit() {

        if (_isEndFlag == false) {

            Toast.makeText(this, getString(R.string.str_back_one_more_end),
                    Toast.LENGTH_SHORT).show();
            _isEndFlag = true;

            _handler.postDelayed(_exitRunner, BACK_TWO_CLICK_DELAY_TIME);

        } else if (_isEndFlag == true) {

            ChatManager.setUserOffline();

            ChatManager.removeAllListener();
            Commons.g_user = new UserModel();
            Commons.chatRooms = new HashMap<>();
            Commons.all_users = new ArrayList<>();
            Commons.favorite_users = new ArrayList<>();
            Commons.myNotifications = new ArrayList<>();
            Commons.messages = new HashMap<>();
            Commons.g_isAppRunning = false;


            finish();
        }
    }

    public void showProgress(boolean cancelable) {

        closeProgress();

        _progressDlg = new ProgressDialog(_context, R.style.MyTheme);
        _progressDlg
                .setProgressStyle(android.R.style.Widget_ProgressBar_Large);
        _progressDlg.setCancelable(cancelable);
        _progressDlg.show();
    }

    public void showProgress() {
        showProgress(false);
    }

    public void closeProgress() {

        if(_progressDlg == null) {
            return;
        }

        _progressDlg.dismiss();
        _progressDlg = null;
    }


    public void showKprogress(boolean cancel){

        closeKProgress();
        kProgressHUD = new KProgressHUD(_context);
        kProgressHUD.setStyle(KProgressHUD.Style
                .SPIN_INDETERMINATE)
                .setCancellable(cancel)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public void showKProgress() {
        showKprogress(false);
    }

    public void closeKProgress() {

        if(kProgressHUD == null) {
            return;
        }

        kProgressHUD.dismiss();
        kProgressHUD = null;
    }

    public void showAlertDialog(String msg) {

        AlertDialog alertDialog = new AlertDialog.Builder(_context).create();

        alertDialog.setTitle(getString(R.string.app_name));
        alertDialog.setMessage(msg);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, _context.getString(R.string.ok),

                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        //alertDialog.setIcon(R.drawable.banner);
        alertDialog.show();

    }

    /**
     *  show toast
     * @param toast_string
     */
    public void showToast(String toast_string) {

        Toast.makeText(_context, toast_string, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean handleMessage(Message msg) {

        switch (msg.what) {

            default:
                break;
        }
        return false;
    }

    public void showSweetAlert(int alertType,String title, String content){

        new SweetAlertDialog(_context,alertType)
                .setTitleText(title)
                .setContentText(content)
                .setConfirmText(getString(R.string.ok))
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {

                    @Override
                    public void onClick(final SweetAlertDialog sweetAlertDialog) {

                        sweetAlertDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                Constants.PAGE_TO = Constants.SETTING_PAGE;
                                startActivity(new Intent(_context, MainActivity.class));
                                finish();

                                sweetAlertDialog.cancel();
                                sweetAlertDialog.dismiss();

                            }
                        }, 1000);

                    }

                })
                .setCancelText(getString(R.string.cancel))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                }).show();
    }

 /*   public void logOut(){

        Constants.SIGNUP_STATUS = 0;
        Constants.PROFILE_STATUS = 0;
        ChatManager.setUserOffline();
        ChatManager.removeAllListener();
        Commons.g_user = new UserModel();
        Commons.chatRooms = new HashMap<>();
        Commons.all_users = new ArrayList<>();
        Commons.favorite_users = new ArrayList<>();
        Commons.myNotifications = new ArrayList<>();
        Commons.messages = new HashMap<>();
        Commons.g_isAppRunning = false;

        startActivity(new Intent(_context, SignInActivity.class));
        EasyPreference.with(_context).clearAll().save();
        finish();
    }
*/

    public static Integer min(Integer... vals) {
        Integer ret = null;
        for (Integer val : vals) {
            if (ret == null || (val != null && val < ret)) {
                ret = val;
            }
        }
        return ret;
    }

    public static Integer max(Integer... vals) {
        Integer ret = null;
        for (Integer val : vals) {
            if (ret == null || (val != null && val > ret)) {
                ret = val;
            }
        }
        return ret;
    }

    private void initReceiver() {
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.KEY_BROADCAST);
        lbm.registerReceiver(receiver, filter);
    }

    class MyReceiver extends BroadcastReceiver {
        MyReceiver() {}

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d("aaaaa===", "received");
            if (action.equals(Constants.KEY_BROADCAST)){
                switch (Commons.mCurrentActivityName) {
                    case Constants.MAIN_ACTIVITY:
                        Log.d("aaaaa===", "MainActivity");
                        if(mCurrentActivity instanceof MainActivity){
                            ((MainActivity)mCurrentActivity).refreshFragment();
                        }
                        break;
                }
            }
        }
    }

    public void setCurrentActivity(Activity mCurrentActivity , String mCurrentActivityName){
        this.mCurrentActivity = mCurrentActivity;
        Commons.mCurrentActivityName = mCurrentActivityName;
    }

    public Activity getmCurrentActivity() {
        return mCurrentActivity;
    }

    public void clearActivityReferences(){
        this.mCurrentActivity = null;
        Commons.mCurrentActivityName = "";
    }
}
