package itwhiz4u.shaadimubark.base;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Bundle;


import java.util.List;
import java.util.Locale;

import itwhiz4u.shaadimubark.commons.Commons;

public abstract class CommonActivity extends BaseActivity{

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
    }


    private String getLauncherClassName() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setPackage(getPackageName());

        List<ResolveInfo> resolveInfoList = getPackageManager().queryIntentActivities(intent, 0);
        if(resolveInfoList != null && resolveInfoList.size() > 0) {
            return resolveInfoList.get(0).activityInfo.name;
        }

        return null;
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        Commons.g_isAppPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Commons.g_isAppPaused = false;
        Commons.g_isAppRunning = true;
    }
}
