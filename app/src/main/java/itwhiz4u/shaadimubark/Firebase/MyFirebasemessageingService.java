package itwhiz4u.shaadimubark.Firebase;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.iamhabib.easy_preference.EasyPreference;
import com.pixplicity.easyprefs.library.Prefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import itwhiz4u.shaadimubark.R;
import itwhiz4u.shaadimubark.RestartActivity;
import itwhiz4u.shaadimubark.ShaadiMubarkApplication;
import itwhiz4u.shaadimubark.activity.MainActivity;
import itwhiz4u.shaadimubark.commons.Commons;
import itwhiz4u.shaadimubark.commons.Constants;
import itwhiz4u.shaadimubark.commons.PrefConst;
import itwhiz4u.shaadimubark.commons.ReqConst;
import itwhiz4u.shaadimubark.model.ChatModel;
import itwhiz4u.shaadimubark.model.NotificationModel;
import itwhiz4u.shaadimubark.utils.StatePendingActivity;
import me.leolin.shortcutbadger.ShortcutBadger;

import static itwhiz4u.shaadimubark.ShaadiMubarkApplication.TAG;

public class MyFirebasemessageingService extends FirebaseMessagingService {

    String message;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
       /* if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (*//* Check if data needs to be processed by long running job *//* true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }

        }*/

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            message = remoteMessage.getNotification().getBody();
            getMyNotifications();
        }

    }

    private void getMyNotifications() {

        String url = ReqConst.SERVER_URL;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetMyNotification(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.MODULE, ReqConst.MODULE_NOTIFICATIONS);
                    params.put(ReqConst.FUNCTION, ReqConst.FUNC_GET_MY_NOTIFICATIONS);
                    params.put(ReqConst.SESSION, Prefs.getString(PrefConst.PREFKEY_SESSION, ""));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.user_id));
                    if (Commons.myNotifications.size() > 0)
                        params.put(ReqConst.PARAM_NOTICATION_TIMESTAMP, String.valueOf(Commons.myNotifications.get(0).notification_timestamp));
                    else {
                        params.put(ReqConst.PARAM_NOTICATION_TIMESTAMP, String.valueOf(0));
                    }
                    params.put(ReqConst.PARAM_LANGUAGE, Commons.getCurrentLanCode());
                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        ShaadiMubarkApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseGetMyNotification(String json){

        Log.d("notis+====>", json);

        try {
            JSONObject respond = new JSONObject(json);
            String result = respond.getString(ReqConst.RESULT);

            if (result.equals(ReqConst.RES_SUCCESS)){

                NotificationModel noti = new NotificationModel();
                JSONArray notiArray = respond.getJSONArray(ReqConst.RES_NOTIFICATIONS);
                for (int i = notiArray.length() - 1; i >= 0  ; i--){
                    noti = new NotificationModel(notiArray.getJSONObject(i));
                    Commons.myNotifications.add(0, noti);

                    if (noti.notification_read != 1){
                        Commons.notiCount++;
                    }
                }


                JSONArray chats = respond.getJSONArray(ReqConst.RES_CHATS);
                for (int i = chats.length() - 1; i >= 0  ; i--){
                    ChatModel chat = new ChatModel(chats.getJSONObject(i));
                    Commons.chatRooms.put(chat.chat_user.user_id, chat);
                }

                ShortcutBadger.applyCount(this, Commons.notiCount);
                sendNotification(message);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void sendNotification(String messageBody) {

        Log.d("MyFirebasemessage", messageBody);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        //String body = getString(R.string.new_message);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setFullScreenIntent(pendingIntent, true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
